import os

from PIL import Image

from .enums import *

class MapData:
    # ((bg_sprite_x, bg_sprite_y), (fg_sprite_x, fg_sprite_y), tile_kind, encounter_zone_id)
    # TODO write an image parser to convert a map PNG to this shit
    bg_atlas = None
    fg_atlas = None
    encounter_zones = []
    collision_zones = []
    map_size = (0, 0)

    objects = [
        {"type": MapObject.CYBER_CENTER, "x": 117, "y": 176},
        {"type": MapObject.SHOP, "x": 122, "y": 176},
        {"type": MapObject.SHOP, "x": 147, "y": 145},

        {"type": MapObject.NPC, "x": 2654 / 16, "y": 3317 / 16, "name": "Small girl", "sprite": "girl_1", "message": "Oh! Someone new!", "money": 10,
        "team": [(CybermonId.GOWATU, 1)]},

        {"type": MapObject.NPC, "x": 2882 / 16, "y": 3478 / 16, "name": "Small girl", "sprite": "girl_1", "message": "Hi! Look what cool bug I found!", "money": 20,
        "team": [(CybermonId.MUDDLEBEE, 2)]},

        {"type": MapObject.NPC, "x": 3009 / 16, "y": 3343 / 16, "name": "Small boy", "sprite": "boy_1", "message": "Grruarhh!", "money": 15,
        "team": [(CybermonId.FAINTRICK, 2)]},

        {"type": MapObject.NPC, "x": 2904 / 16, "y": 3302 / 16, "name": "Guy", "sprite": "guy_2", "message": "Look what cybermon I found!", "money": 200,
        "team": [(CybermonId.GUMBWAAL, 4)]},

        {"type": MapObject.NPC, "x": 2683 / 16, "y": 3491 / 16, "name": "Treehugger", "sprite": "woman_3", "message": "Must. Get. ALL THE BIRDS!", "money": 500,
        "team": [(CybermonId.GOWATU, 2), (CybermonId.GOWATU, 3), (CybermonId.DRIBBINO, 2)]},

        {"type": MapObject.NPC, "x": 2420 / 16, "y": 3206 / 16, "name": "Old man", "sprite": "old_man_1", "message": "Here's a nickel, kid... Get yourself some good cybermon.", "money": 800,
        "team": [(CybermonId.CROCOAL, 6), (CybermonId.POMPILLAR, 7)]},

        {"type": MapObject.NPC, "x": 2123 / 16, "y": 3191 / 16, "name": "Cool guy", "sprite": "guy_3", "message": "Hey!", "money": 400,
        "team": [(CybermonId.FAINTRICK, 4), (CybermonId.POMPILLAR, 7)]},

        {"type": MapObject.NPC, "x": 1810 / 16, "y": 3487 / 16, "name": "Wise hiker", "sprite": "man_1", "message": "Look at all these bugs I found!", "money": 600,
        "team": [(CybermonId.MUDDLEBEE, 5), (CybermonId.POMPILLAR, 3), (CybermonId.TINIMER, 2)]},

        {"type": MapObject.NPC, "x": 1790 / 16, "y": 3105 / 16, "name": "Some guy", "sprite": "guy_2", "message": "Bugs!", "money": 200,
        "team": [(CybermonId.TINIMER, 2), (CybermonId.TINIMER, 1)]},

        {"type": MapObject.NPC, "x": 1664 / 16, "y": 2617 / 16, "name": "Lady", "sprite": "woman_2", "message": "I found this while I was on a hike!", "money": 500,
        "team": [(CybermonId.ACAFIA, 8)]},

        {"type": MapObject.NPC, "x": 2119 / 16, "y": 2810 / 16, "name": "Small boy", "sprite": "boy_1", "message": "I have so many cybermon!", "money": 50,
        "team": [(CybermonId.MUDDLEBEE, 2), (CybermonId.MUDDLEBEE, 3), (CybermonId.POMPILLAR, 2), (CybermonId.FAINTRICK, 1)]},

        {"type": MapObject.NPC, "x": 1623 / 16, "y": 2437 / 16, "name": "Kevin", "sprite": "guy_1", "message": "I'm the best in town!", "money": 1500,
        "team": [(CybermonId.FAINTRICK, 5), (CybermonId.FAINTRICK, 8), (CybermonId.FAINTRICK, 12), (CybermonId.FAINTRICK, 16)]},

        {"type": MapObject.NPC, "x": 2310 / 16, "y": 2372 / 16, "name": "Lady", "sprite": "woman_1", "message": "My cybermon are so strong!", "money": 800,
        "team": [(CybermonId.FAINTRICK, 7), (CybermonId.ACAFIA, 9), (CybermonId.MELANYAN, 3)]},

        {"type": MapObject.NPC, "x": 1820 / 16, "y": 2213 / 16, "name": "Little girl", "sprite": "girl_1", "message": "Meow meow!!", "money": 50,
        "team": [(CybermonId.MELANYAN, 7)]},

        {"type": MapObject.NPC, "x": 2854 / 16, "y": 2744 / 16, "name": "Jack", "sprite": "guy_1", "message": "I WILL destroy you!", "money": 1200,
        "team": [(CybermonId.POMPILLAR, 12), (CybermonId.FAINTRICK, 14)]},

        {"type": MapObject.NPC, "x": 2708 / 16, "y": 2881 / 16, "name": "Hiker", "sprite": "man_1", "message": "Look what I found!", "money": 600,
        "team": [(CybermonId.TINIMER, 16)]},

        {"type": MapObject.NPC, "x": 2820 / 16, "y": 2338 / 16, "name": "Crazy woman", "sprite": "woman_3", "message": "Electrifying!", "money": 800,
        "team": [(CybermonId.FINNIAL, 15)]},

        {"type": MapObject.NPC, "x": 3169 / 16, "y": 2699 / 16, "name": "Old man", "sprite": "old_man_1", "message": "Ah! I love this island. It's so relaxing.", "money": 500,
        "team": [(CybermonId.LIZEROON, 4), (CybermonId.MUDDLEBEE, 6)]},

        {"type": MapObject.NPC, "x": 3506 / 16, "y": 2546 / 16, "name": "Cool guy", "sprite": "guy_1", "message": "I never lose!", "money": 2400,
        "team": [(CybermonId.FINNIAL, 24), (CybermonId.GOWATU, 12)]},

        {"type": MapObject.NPC, "x": 3417 / 16, "y": 1921 / 16, "name": "Little girl", "sprite": "girl_1", "message": "They told me the route above is closed!", "money": 60,
        "team": [(CybermonId.DRIBBINO, 14), (CybermonId.FAINTRICK, 6)]},
    ]

    @staticmethod
    def load_objects():
        spawn_locations = [(x,y) for x in range(154, 187) for y in range(200, 203)]
        spawn_locations += [(x,y) for x in range(154, 175) for y in range(214, 217)]

        for sl in spawn_locations:
            MapData.objects.append({"type": MapObject.SPAWN_LOCATION, "x": sl[0], "y": sl[1]})

        for i, o in enumerate(MapData.objects):
            o["x"] = int(round(o["x"]))
            o["y"] = int(round(o["y"]))
            o["id"] = i

    @staticmethod
    def get_all_objects_of_type(t):
        ret = []

        for obj in MapData.objects:
            if obj["type"] == t:
                ret.append(obj)

        return ret
        
    @staticmethod
    def load_map_data(folder_path):
        tile_size = (16, 16)
        MapData.bg_atlas = Image.open(os.path.join(folder_path, "tile_atlas_bg.png"))
        MapData.fg_atlas = Image.open(os.path.join(folder_path, "tile_atlas_fg.png"))

        target_size = (int(MapData.bg_atlas.size[0] / tile_size[0]), int(MapData.bg_atlas.size[1] / tile_size[1]))

        encounter_image = Image.open(os.path.join(folder_path, "encounter_map.png")).convert('RGB').resize((target_size[0], target_size[1]), Image.NEAREST)
        collision_image = Image.open(os.path.join(folder_path, "collision_map.png")).convert('RGB').resize((target_size[0], target_size[1]), Image.NEAREST)
        MapData.map_size = (MapData.bg_atlas.size[0] // tile_size[0], MapData.bg_atlas.size[1] // tile_size[1])

        
        MapData.encounter_zones = encounter_image.load()
        MapData.collision_zones = (collision_image.load(), collision_image.size)

    @staticmethod
    def get_encounter_zone(x, y):
        pixel = MapData.encounter_zones[x,y]

        if (pixel not in MapData.encounter_zone_data):
            print("WARNING!!! Invalid pixel in encounter_map.png: " + str(pixel))
            return {"chance": 0, "encounters": []}

        return MapData.encounter_zone_data[pixel]

    # "encounters" chances need increase until a cybermon has a chance of 1 (e.g. 0.2, 0.5, 1)
    # the difference between these increases is each cybermon's chance of appearing

    # TODO - Do encounter zone data, change from 100% chances, set to 6.5%
    encounter_zone_data = {
        (0, 0, 0): {"chance": 0, "encounters": []}, # Nothing

        # R1
        (125, 255, 114): {"chance": 0.15, "encounters": [
            {"chance": 0.3, "cybermon": CybermonId.GOWATU, "level_range": [1, 3]},
            {"chance": 0.7, "cybermon": CybermonId.FAINTRICK, "level_range": [1, 3]},
            {"chance": 0.9, "cybermon": CybermonId.STICKIBAT, "level_range": [2, 3]},
            {"chance": 0.98, "cybermon": CybermonId.GUMBWAAL, "level_range": [2, 3]},
            {"chance": 1, "cybermon": CybermonId.ACAFIA, "level_range": [2, 4]}
        ]},

        # City
        (32, 217, 126): {"chance": 0.1, "encounters": [
            {"chance": 0.2, "cybermon": CybermonId.MUDDLEBEE, "level_range": [3, 5]},
            {"chance": 0.4, "cybermon": CybermonId.POMPILLAR, "level_range": [3, 5]},
            {"chance": 0.7, "cybermon": CybermonId.WYRMAL, "level_range": [2, 6]},
            {"chance": 0.9, "cybermon": CybermonId.LARVILL, "level_range": [2, 6]},
            {"chance": 1, "cybermon": CybermonId.SHRYMPH, "level_range": [3, 5]}
        ]},

        # Forest
        (15, 166, 141): {"chance": 0.15, "encounters": [
            {"chance": 0.1, "cybermon": CybermonId.TINIMER, "level_range": [5, 10]},
            {"chance": 0.3, "cybermon": CybermonId.MUDDLEBEE, "level_range": [5, 7]},
            {"chance": 0.4, "cybermon": CybermonId.DRILLINDER, "level_range": [9, 10]},
            {"chance": 0.6, "cybermon": CybermonId.POMPILLAR, "level_range": [5, 8]},
            {"chance": 0.8, "cybermon": CybermonId.GOWATU, "level_range": [5, 8]},
            {"chance": 0.9, "cybermon": CybermonId.DRIBBINO, "level_range": [6, 9]},
            {"chance": 1, "cybermon": CybermonId.ACAFIA, "level_range": [6, 9]},
        ]},

        # GLITCH - TRUCK, SEE YOURSELF
        (15, 166, 140): {"chance": 1, "encounters": [
            {"chance": 1, "cybermon": CybermonId.GLITCH_YOURSELF, "level_range": [1, 100]}
        ]}

    }
