from .enums import *
from .item import ItemData
from .move import MoveData
from .cybermon import Cybermon
import random, math
import traceback

class Battle:
    def __init__(self, game, *teams):
        self.teams = teams
        self.queued_moves = {}
        self.uis = {}
        
        self.game = game
        self.game.battle_timeout_clock_callback(self)

        self.last_attacker_id = None
        self.last_attacker_ctx = None
        
        self.battle_log = ""
        self.turn_count = 0

        for team in self.teams:
            if team.get_active_cybermon() is None:
                decamark_id = random.choice([CybermonId.DECAMARK1, CybermonId.DECAMARK2, CybermonId.DECAMARK3, CybermonId.DECAMARK4])
                decamark = Cybermon(decamark_id)
                
                moves = []
                for i in range(random.randint(1, 4)):
                    moves.append(MoveData.get_move(MoveId(random.randint(1, 112))))
                
                pending_moves = []

                for i in range(random.randint(0, 1)):
                    pending_moves.append(MoveData.get_move(MoveId(random.randint(1, 112))))

                ivs = []
                evs = [31, 31, 31, 31, 31, 31]

                if decamark_id == CybermonId.DECAMARK1:
                    for i in range(6):
                        ivs.append(random.randint(1, 120))

                # fountain
                elif decamark_id == CybermonId.DECAMARK2:
                    ivs = [random.randint(100, 300), random.randint(1, 10), random.randint(100, 300), random.randint(2, 20), random.randint(60, 240), 1]

                # trucks
                elif decamark_id == CybermonId.DECAMARK3:
                    ivs = [random.randint(1, 5), random.randint(100, 250), random.randint(2, 6), random.randint(100, 250), random.randint(6, 7), random.randint(240, 999)]

                # shop
                elif decamark_id == CybermonId.DECAMARK4:
                    ivs = [random.randint(50, 200), random.randint(20, 100), random.randint(40, 300), random.randint(25, 50), random.randint(6, 10), 0]

                decamark.configure(team, random.randint(1, 63), '', moves, pending_moves, ivs = ivs, evs = evs)
                decamark.glitch_states.append(GlitchId.IF_EVERYONES_DEAD)

                if len(team.cybermon) == 0:
                    team.cybermon.append(decamark)
                else:
                    old_c = team.cybermon[0]
                    old_c.team = None

                    team.cybermon[0] = decamark


    # GLITCH - Use moves from other battles?
    async def battle_sanity_check(self, ctx, attacker_team, target_team):
        # Are the teams fighting each other in this same battle?
        if (attacker_team is not None and target_team is not None):
            if (attacker_team.battle == self and target_team.battle == self):
                return True
        
        if (attacker_team.id in self.queued_moves):
            await self.uis[attacker_team.id].set_message(ctx, "You have already made your move.")
            
        return False

    async def make_move(self, attacker_team, target_team, move, ctx):
        if (not await self.battle_sanity_check(ctx, attacker_team, target_team)):
            return

        attacker_cybermon = attacker_team.get_active_cybermon()
        target_cybermon = target_team.get_active_cybermon()

        if ((move not in attacker_cybermon.moves or move["pp"] <= 0) and move["id"] is not MoveId.STRUGGLE):
            await self.uis[attacker_team.id].set_message(ctx, "You have tried using an invalid move.")
        else:
            self.queued_moves[attacker_team.id] = {
                "attacker_team": attacker_team, "target_team": target_team,
                "attacker_cybermon": attacker_cybermon, "target_cybermon": target_cybermon,
                "action": "attack", "move": move
            }

            await self.__player_made_move(attacker_team, target_team, ctx)

    async def use_item(self, attacker_team, target_team, item, ctx):
        if (not await self.battle_sanity_check(ctx, attacker_team, target_team)):
            return

        if (item not in attacker_team.items):
            await self.uis[attacker_team.id].set_message(ctx, "You have tried using an item you don't have.")

            await attacker_team.notify_glitch(GlitchId.ITEM_DONT_HAVE, "If the UI decides things... Then we have a problem. Check your inventory.\n**X-MAS{STALE_STATES_18cn983fc1f819fcy831yf9c1yn398y}**")
            attacker_team.add_item(ItemId((item["id"].value + 1) % 58), (58 - item["id"].value))

        elif ((target_team.id is not None or len(target_team.name) > 0) and ItemUsability.BATTLE_TEAM not in item["usability"]):
            await self.uis[attacker_team.id].set_message(ctx, "You cannot use this item while you battle another player or npc.")

        elif (target_team.id is None and ItemUsability.BATTLE_WILD not in item["usability"]):
            await self.uis[attacker_team.id].set_message(ctx, "You cannot use this item while you battle a wild cybermon.")
        else:
            attacker_cybermon = attacker_team.get_active_cybermon()
            target_cybermon = target_team.get_active_cybermon()

            self.queued_moves[attacker_team.id] = {
                "attacker_team": attacker_team, "target_team": target_team,
                "attacker_cybermon": attacker_cybermon, "target_cybermon": target_cybermon,
                "action": "use_item", "item": item
            }

            await self.__player_made_move(attacker_team, target_team, ctx)

    async def swap_cybermon(self, attacker_team, target_team, cybermon_index, ctx):
        if (not await self.battle_sanity_check(ctx, attacker_team, target_team)):
            return
        
        if cybermon_index < 0 or cybermon_index >= len(attacker_team.cybermon):
            await self.uis[attacker_team.id].set_message(ctx, "That's an invalid cybermon index")
        elif (attacker_team.cybermon[cybermon_index].hp == 0):
            await self.uis[attacker_team.id].set_message(ctx, "That cybermon is fainted")
        elif (attacker_team.cybermon[cybermon_index] == attacker_team.get_active_cybermon()):
            await self.uis[attacker_team.id].set_message(ctx, "You can't swap with your active cybermon!")
        else:
            attacker_cybermon = attacker_team.get_active_cybermon()
            target_cybermon = target_team.get_active_cybermon()

            self.queued_moves[attacker_team.id] = {
                "attacker_team": attacker_team, "target_team": target_team,
                "attacker_cybermon": attacker_cybermon, "target_cybermon": target_cybermon,
                "action": "swap_cybermon", "swap_index": cybermon_index
            }

            await self.__player_made_move(attacker_team, target_team, ctx)

    async def run_away(self, attacker_team, target_team, ctx):
        if (not await self.battle_sanity_check(ctx, attacker_team, target_team)):
            return
        
        # GLITCH - literally kill an enemy cybermon for free
        if target_team.id is not None or len(target_team.name) > 0:
            await self.uis[attacker_team.id].set_message(ctx, "You can't run away from a player or an NPC!")
        else:
            attacker_cybermon = attacker_team.get_active_cybermon()
            target_cybermon = target_team.get_active_cybermon()
            
            self.queued_moves[attacker_team.id] = {
                "attacker_team": attacker_team, "target_team": target_team,
                "attacker_cybermon": attacker_cybermon, "target_cybermon": target_cybermon,
                "action": "run_away"
            }

            await self.__player_made_move(attacker_team, target_team, ctx)

    async def __player_made_move(self, attacker_team, target_team, ctx):
        attacker_cybermon = attacker_team.get_active_cybermon()
        target_cybermon = target_team.get_active_cybermon()

        # do AI stuff
        if (target_team.id is None):
            print("Fighting AI")

            moves_to_pick = []

            for m in target_cybermon.moves:
                if m["pp"] > 0:
                    moves_to_pick.append(m)

            if len(moves_to_pick) > 0:
                move = random.choice(moves_to_pick)
            else:
                move = MoveData.get_move(MoveId.STRUGGLE)

            self.queued_moves[target_team.id] = {
                "attacker_team": target_team, "target_team": attacker_team,
                "attacker_cybermon": target_cybermon, "target_cybermon": attacker_cybermon,
                "action": "attack", "move": move
            }
            
        if len(self.queued_moves) == len(self.teams):
            self.last_attacker_id = attacker_team.id
            self.last_attacker_ctx = ctx
            await self.__do_queued_moves()
        else:
            await self.uis[attacker_team.id].set_message(ctx, "Waiting for opponent's turn...")

    async def __do_queued_moves(self):
        self.turn_count += 1

        # hardcoded 2 teams
        team_1 = list(self.queued_moves.keys())[0]
        team_2 = list(self.queued_moves.keys())[1]

        action_1 = self.queued_moves[team_1]["action"]
        action_2 = self.queued_moves[team_1]["action"]

        speed_1 = self.queued_moves[team_1]["attacker_cybermon"].stats.spd
        speed_2 = self.queued_moves[team_2]["attacker_cybermon"].stats.spd
        
        swapped_queued_moves = {team_2: self.queued_moves[team_2], team_1: self.queued_moves[team_1]}

        # Swapping cybermon has the weakest priority
        if action_1 == "swap_cybermon" and action_2 != "swap_cybermon": speed_1 = speed_2 + 1
        if action_2 == "swap_cybermon" and action_1 != "swap_cybermon": speed_2 = speed_1 + 1

        # Using an item has better priority
        if action_1 == "use_item" and action_2 != "use_item": speed_1 = speed_2 + 1
        if action_2 == "use_item" and action_1 != "use_item": speed_2 = speed_1 + 1

        # The move first glitch has the most priority
        if "move_first_glitched" in self.queued_moves[team_1]["attacker_cybermon"].states: speed_1 = speed_2 + 1
        if "move_first_glitched" in self.queued_moves[team_2]["attacker_cybermon"].states: speed_2 = speed_1 + 1

        if speed_1 < speed_2:
            self.queued_moves = swapped_queued_moves
        elif speed_1 == speed_2:
            if random.random() < 0.5:
                self.queued_moves = swapped_queued_moves

        for team_id in self.queued_moves:
            queued_move = self.queued_moves[team_id]
            print("Doing move from: " + str(queued_move["attacker_team"].name) + " against " + str(queued_move["target_team"].name))

            attacker_team = queued_move["attacker_team"]
            target_team = queued_move["target_team"]
            attacker_cybermon = queued_move["attacker_cybermon"]
            target_cybermon = queued_move["target_cybermon"]

            if attacker_cybermon != attacker_team.get_active_cybermon() or target_team.get_active_cybermon() is None: # or target_cybermon != target_team.get_active_cybermon():
                continue
            
            if queued_move["action"] == "use_item":
                move_item = queued_move["item"]
                item = None

                for team_item in attacker_team.items:
                    if team_item["id"] == move_item["id"]:
                        item = team_item
                        break

                if item is None:
                    self.__append_to_log_message(f"{attacker_team.name} tried using an invalid item!")
                else:
                    item_use_text = item["use_function"](item, attacker_cybermon, target_cybermon)
                    attacker_team.add_item(item["id"], -1)

                    self.__append_to_log_item(attacker_team, target_team, item, item_use_text)

                if GlitchId.DOLLY in attacker_cybermon.glitch_states:
                    await attacker_team.notify_glitch(GlitchId.DOLLY, "We're going to have an army full of these!\n**X-MAS{DOLLY_1C989c8a98cadf9d8nc98f19ecy81}**")
                    
            elif queued_move["action"] == "attack":
                attacker_cybermon = attacker_team.get_active_cybermon()
                target_cybermon = target_team.get_active_cybermon()

                skip = False

                if attacker_cybermon.status == CybermonStatus.PARALYSIS:
                    if random.random() < 0.25:
                        skip = True

                if attacker_cybermon.status in [CybermonStatus.SLEEP, CybermonStatus.FREEZE]:
                    skip = True

                if skip:
                    status = "UNABLE TO MOVE"
                    if attacker_cybermon.status == CybermonStatus.PARALYSIS:
                        status = "PARALYSED"
                    elif attacker_cybermon.status == CybermonStatus.SLEEP:
                        status = "ASLEEP"
                    elif attacker_cybermon.status == CybermonStatus.FREEZE:
                        status = "FROZEN"

                    self.__append_to_log_message(f"\u274C {attacker_cybermon.name} IS {status}!")
                else:
                    attack_message = attacker_cybermon.attack(queued_move["move"], target_cybermon)
                    
                    self.__append_to_log_attack(attacker_team, target_team, attacker_cybermon, target_cybermon, queued_move["move"], attack_message)

                    # We just killed the enemy cybermon, add xp to team
                    if target_cybermon.hp == 0:
                        xp_divider = 0

                        for cybermon in attacker_team.cybermon:
                            if cybermon.damage_dealt > 0 and cybermon.hp > 0:
                                xp_divider += 1

                        for cybermon in attacker_team.cybermon:
                            if cybermon.damage_dealt > 0 and cybermon.hp > 0:
                                level_diff = target_cybermon.level - cybermon.level

                                xp = 3 * cybermon.level # base XP
                                xp *= (2/3) * math.tanh(level_diff / 4) + 4 ** (0.02 * level_diff) + 1
                                xp /= xp_divider
                                xp = round(xp)

                                if target_cybermon.id == CybermonId.Q:
                                    xp *= 100
                                    self.__append_to_log_message("???")

                                if (attacker_team.id is not None):
                                    self.__append_to_log_message(f"{attacker_team.name}'s {cybermon.name} got {xp} XP!")
                                elif len(attacker_team.name) == 0:
                                    self.__append_to_log_message(f"Wild {cybermon.name} got {xp} XP!")

                                cybermon_id = cybermon.id
                                old_moves = cybermon.moves + cybermon.pending_moves
                                old_level = cybermon.level
                                
                                cybermon.add_xp(xp)

                                evs = [0, 0, 0, 0, 0, 0]
                                enemy_base_stats = target_cybermon.base_stats.to_list()

                                for i in range(6):
                                    if random.random() < enemy_base_stats[i] / 2000:
                                        evs[i] = 1
                                
                                cybermon.add_evs(evs)
                                
                                if (attacker_team.id is not None):
                                    if old_level != cybermon.level:
                                        self.__append_to_log_message(f"!!! \U0001F506 {attacker_team.name}'s {cybermon.name} HAS LEVELED UP TO LV. {cybermon.level}!")

                                    if cybermon_id != cybermon.id:
                                        self.__append_to_log_message(f"!!! \U0001F506 {attacker_team.name}'s {cybermon.name} HAS EVOLVED!")

                                    if len(old_moves) != len(cybermon.moves + cybermon.pending_moves):
                                        self.__append_to_log_message(f"!!! \U0001F506 {attacker_team.name}'s {cybermon.name} CAN LEARN A NEW MOVE!")

                            cybermon.damage_dealt = 0

                if GlitchId.POWERFUL_NTURN in attacker_cybermon.glitch_states:
                    await attacker_team.notify_glitch(GlitchId.POWERFUL_NTURN, "Actually... I am most powerful.\n**X-MAS{ARBITRARY_ITEMS_c19u3nc13uf98c1ufc13}**")
                    attacker_team.cybermon.remove(attacker_cybermon)
                    attacker_cybermon.team = None
                    
                    if len(attacker_team.cybermon) == 0:
                        attacker_team.add_state(TeamState.NO_CYBERMON)

            elif queued_move["action"] == "swap_cybermon":
                attacker_cybermon = attacker_team.get_active_cybermon()
                swap_cybermon = attacker_team.cybermon[queued_move["swap_index"]]
                
                if attacker_cybermon != swap_cybermon and swap_cybermon.hp > 0:
                    attacker_index = attacker_team.cybermon.index(attacker_cybermon)

                    bak = attacker_cybermon
                    attacker_team.cybermon[attacker_index] = swap_cybermon
                    attacker_team.cybermon[queued_move["swap_index"]] = bak

                    self.__append_to_log_message(f"\U0001F501 Team {attacker_team.name} swapped {attacker_cybermon.name} with {swap_cybermon.name}!")
                else:
                    self.__append_to_log_message(f"\u274C Team {attacker_team.name} tried to swap {attacker_cybermon.name} but it failed!")

            elif queued_move["action"] == "run_away":
                speed_diff = attacker_cybermon.stats.spd - target_cybermon.stats.spd
                level_diff = attacker_cybermon.level - target_cybermon.level
                chance = 0.5 + math.tanh((speed_diff + level_diff) / 100) / 2
                
                if random.random() < chance:
                    target_team.cybermon[0].hp = 0
                    target_team.money = 0
                    self.__append_to_log_message(f"\U0001F4A8 You escaped successfully.")
                else:
                    self.__append_to_log_message(f"\u274C You tried escaping, but failed!")


        for cybermon in self.teams[0].cybermon + self.teams[1].cybermon:
            if cybermon.hp <= 0:
                continue

            if cybermon.status in [CybermonStatus.SLEEP, CybermonStatus.FREEZE]:
                if random.random() < 0.33:
                    cybermon.set_status(CybermonStatus.NONE)
                    self.__append_to_log_message(f"{cybermon.name} IS NOW ABLE TO MOVE!")

            if cybermon.status == CybermonStatus.BURN:
                cybermon.add_hp(-cybermon.stats.max_hp * 0.07)
                self.__append_to_log_message(f"{cybermon.name} TOOK BURN DAMAGE!")

            if cybermon.status == CybermonStatus.POISON:
                cybermon.add_hp(-cybermon.stats.max_hp * 0.06)
                self.__append_to_log_message(f"{cybermon.name} TOOK POISON DAMAGE!")

            if "white_cyberdrug" in cybermon.states:
                cybermon.states["white_cyberdrug"] -= 1
                if cybermon.states["white_cyberdrug"] <= 0:
                    del cybermon.states["white_cyberdrug"]
                    cybermon.add_hp(-999999)
                    self.__append_to_log_message(f"\U0001F480 {cybermon.name}'s WHITE CYBERDRUG wore off! It got knocked out.")
                else:
                    self.__append_to_log_message(f"{cybermon.states['white_cyberdrug']} turns until {cybermon.name}'s WHITE CYBERDRUG wears off.")

        self.queued_moves = {}

        def transfer_money(winner, loser):
            money = int(loser.money / 10)
            loser.money -= money
            winner.money += money
            
            money_text = ""
            if loser.id is not None:
                money_text += f"{loser.name} lost {money} bits! "

            if winner.id is not None:
                money_text += f"{winner.name} got {money} bits! "
            
            print(money_text)
            self.__append_to_log_message(money_text)

        # hardcoded 2 teams
        cybermon_1 = self.teams[0].get_active_cybermon()
        cybermon_2 = self.teams[1].get_active_cybermon()

        # copy pasted code :(
        if cybermon_1 is None:
            transfer_money(self.teams[1], self.teams[0])
        elif cybermon_2 is None:
            transfer_money(self.teams[0], self.teams[1])

        for team in self.teams:
            if team.id == self.last_attacker_id:
                await self.__refresh_team_ui(team.id, self.last_attacker_ctx)
            else:
                await self.__refresh_team_ui(team.id)
        
        self.game.battle_timeout_clock_callback(self)

        if cybermon_1 is None:
            await self.game.battle_end_callback(self.teams[1], self.teams[0])
        elif cybermon_2 is None:
            await self.game.battle_end_callback(self.teams[0], self.teams[1])

    def __append_to_log_attack(self, attacker_team, target_team, attacker_cybermon, target_cybermon, move, attack_message):
        entry_string = f"Wild " if len(attacker_team.name) == 0 else attacker_team.name + "'s "
        entry_string += attacker_cybermon.name
        entry_string += f" used {move['name'].upper()}! "
        entry_string += attack_message
        self.__append_to_log_message(entry_string)

    def __append_to_log_item(self, attacker_team, target_team, item, item_text):
        entry_string = "" + f"Wild cybermon" if len(attacker_team.name) == 0 else attacker_team.name
        entry_string += f" used item {item['name'].upper()}: {item_text}"
        self.__append_to_log_message(entry_string)

    def __append_to_log_message(self, message):
        message = "#" + str(len(self.battle_log.split('\n'))) + ". " + message + "\n"
        self.battle_log = self.battle_log + message

    async def battle_timeout_ui_update(self):
        for team in self.teams:
            if team.id not in self.uis:
                continue
            
            if team.id in self.queued_moves:
                await self.uis[team.id].set_message(None, "You have won, since your opponent took longer than 5 minutes to take their turn.")
            else:
                await self.uis[team.id].set_message(None, "You have lost since you took longer than 5 minutes to take your turn.")
                
    async def __refresh_team_ui(self, team_id, ctx = None):
        if team_id in self.uis:
            try:
                await self.uis[team_id].refresh(ctx)
            except Exception as e:
                print("Tried to refresh an expired UI!")
                traceback.print_exc()

    def set_team_ui(self, team_id, ui):
        self.uis[team_id] = ui
    
    def get_opponent_team(self, team_id):
        for team in self.teams:
            # hardcoded 2 teams
            if (team.id != team_id):
                return team
        
        return None
    
    def __get_team_by_id(self, team_id):
        for team in self.teams:
            if (team.id == team_id):
                return team

        return None
    