from .enums import *
from .move import MoveData
from copy import copy
import random

class Stats:
    def __init__(self, hp, patk, pdef, satk, sdef, spd):
        self.max_hp = hp
        self.patk = patk
        self.pdef = pdef
        self.satk = satk
        self.sdef = sdef
        self.spd = spd

    @staticmethod
    def from_list(data):
        return Stats(*data[:6])

    def to_list(self):
        return [self.max_hp, self.patk, self.pdef, self.satk, self.sdef, self.spd]

    def __copy__(self):
        return Stats(
            self.max_hp,
            self.patk,
            self.pdef,
            self.satk,
            self.sdef,
            self.spd
        )

    def __add__(self, other):
        if isinstance(other, tuple) or isinstance(other, list):
            return Stats(
                self.max_hp + other[0],
                self.patk + other[1],
                self.pdef + other[2],
                self.satk + other[3],
                self.sdef + other[4],
                self.spd + other[5]
            )

        elif isinstance(other, Stats):
            return Stats(
                self.max_hp + other.max_hp,
                self.patk + other.patk,
                self.pdef + other.pdef,
                self.satk + other.satk,
                self.sdef + other.sdef,
                self.spd + other.spd
            )

    def apply_stat_growth(self, level, ivs, evs):
        modifier = 0.01 * (level - 1)

        growths = self + ivs + evs

        return Stats(
            int(round(growths.max_hp / 4 + (growths.max_hp * modifier * 2) + level + 10)),
            int(round(growths.patk + (growths.patk * modifier))),
            int(round(growths.pdef + (growths.pdef * modifier))),
            int(round(growths.satk + (growths.satk * modifier))),
            int(round(growths.sdef + (growths.sdef * modifier))),
            int(round(growths.spd + (growths.spd * modifier)))
        )

    def normalize(self):
        self.max_hp = int(round(self.max_hp))
        self.patk = int(round(self.patk))
        self.pdef = int(round(self.pdef))
        self.satk = int(round(self.satk))
        self.sdef = int(round(self.sdef))
        self.spd = int(round(self.spd))

class Cybermon:
    @staticmethod
    def get_total_xp_level(level):  # preserve accuracy; this returns a float
        if 0 < level <= 100:
            return (3 * (level ** 3)) / 6
        else:
            return (level - 100) * (2 ** 16) - 1  # big numero

    @staticmethod
    def get_xp_to_level_up_from(level):
        return round(Cybermon.get_total_xp_level(level + 1) - Cybermon.get_total_xp_level(level)) 

    def __init__(self, template_id):
        if (type(template_id) is not CybermonId):
            template_id = CybermonId(template_id)
            
        if template_id not in Cybermon.templates:
            print("Attempted to create an invalid cybermon.")
            template_id = CybermonId.Q
            
        template = Cybermon.templates[template_id]

        self.id = template_id
        self.name = template["name"]
        self.types = template["types"]
        self.level = 1
        self.db_id = -1

        self.evolve_at = template["evolve_at"]
        self.evolve_into = template["evolve_into"]

        self.ivs = [0, 0, 0, 0, 0, 0]
        self.evs = [0, 0, 0, 0, 0, 0]

        self.base_stats = Stats(template["hp"], template["atk"], template["def"], template["satk"], template["sdef"], template["spd"])
        self.stats = self.base_stats
        self.hp = self.stats.max_hp
        self.stat_stages = [0, 0, 0, 0, 0, 0]
        self.status = CybermonStatus.NONE
        self.recalculate_stats()

        self.states = {}
        self.glitch_states = [] 

        self.moves = []
        self.pending_moves = []

        self.damage_dealt = 0
        
        self.xp = 0
        self.xp_to_level_up = Cybermon.get_xp_to_level_up_from(self.level)

    def add_stat_stages(self, new_stat_stages):
        for i in range(len(new_stat_stages)):
            self.stat_stages[i] += new_stat_stages[i]
            
            if self.stat_stages[i] > 6:
                self.stat_stages[i] = 6
            
            if self.stat_stages[i] < -6:
                self.stat_stages[i] = -6
        
        self.recalculate_stats()
    
    def set_id(self, new_id):
        # TODO - also change ability
        temp_cybermon = Cybermon(new_id)

        self.id = temp_cybermon.id
        self.base_stats = temp_cybermon.base_stats
        self.evolve_at = temp_cybermon.evolve_at
        self.evolve_into = temp_cybermon.evolve_into
        self.types = temp_cybermon.types

        for i in range(6):
            self.ivs[i] = random.randint(0, 31)
            
        self.recalculate_stats()
        
    # TODO
    def add_evs(self, evs):
        for i in range(len(evs)):
            self.evs[i] += evs[i]

            if self.evs[i] < 0:
                self.glitch_states.append(GlitchId.EV_UNDERFLOW)

            self.evs[i] %= 32

        self.recalculate_stats()

    def add_ivs(self, ivs):
        for i in range(len(ivs)):
            self.ivs[i] += ivs[i]
            self.ivs[i] %= 32

        self.recalculate_stats()

    def set_status(self, status):
        if self.status == CybermonStatus.NONE or status == CybermonStatus.NONE:
            self.status = status
            self.recalculate_stats()

    @staticmethod
    def get_stat_stage_multiplier(stage):
        if stage >= 0:
            return 1.2 ** stage
        else:
            return 0.8 ** (-stage)

    def recalculate_stats(self):
        ratio = self.hp / self.stats.max_hp

        self.stats = self.base_stats.apply_stat_growth(self.level, self.ivs, self.evs)
        
        multipliers = list(map(Cybermon.get_stat_stage_multiplier, self.stat_stages))
        self.stats.max_hp *= multipliers[0]
        self.stats.patk *= multipliers[1]
        self.stats.pdef *= multipliers[2]
        self.stats.satk *= multipliers[3]
        self.stats.sdef *= multipliers[4]
        self.stats.spd *= multipliers[5]

        if self.status == CybermonStatus.PARALYSIS:
            self.stats.spd = 0

        if self.status == CybermonStatus.BURN:
            self.stats.patk *= 0.8

        self.stats.normalize()
        self.hp = int(round(self.stats.max_hp * ratio))

    def configure(self, team, level = 1, nickname = '', moves=[], pending_moves=[], ivs = None, evs = None):
        self.team = team

        if ivs is not None:
            self.ivs = ivs
        else:
            for i in range(6):
                self.ivs[i] = random.randint(0, 31)

        if evs is not None:
            self.evs = evs

        if len(nickname) > 0:
            self.name = nickname

        if len(moves) > 0:
            self.set_level(level)
            self.moves = moves
            self.pending_moves = pending_moves
        else:
            for i in range(level + 1):
                self.set_level(i)

            # Clear naturally taught moves to replace them with random ones in the moveset
            self.pending_moves += self.moves
            self.moves = []

            print("For " + nickname + ": Pending moves: " + str(self.pending_moves))

            for i in range(4):
                if len(self.pending_moves) == 0:
                    break

                move = random.choice(self.pending_moves)
                self.moves.append(move)
                self.pending_moves.remove(move)

            print("Choosing moves: " + str(self.moves))

    def set_level(self, level):
        level = min(255, level)

        self.level = level
        
        if level == self.evolve_at and self.evolve_into is not None:
            template = Cybermon.templates[self.evolve_into]

            original_name = Cybermon.templates[self.id]["name"]
            self.id = self.evolve_into

            if (self.name == original_name):
                self.name = template["name"]

            self.types = template["types"]
            self.evolve_at = template["evolve_at"]
            self.evolve_into = template["evolve_into"]

            self.base_stats = Stats(template["hp"], template["atk"], template["def"], template["satk"], template["sdef"], template["spd"])

        self.recalculate_stats()
        self.xp_to_level_up = Cybermon.get_xp_to_level_up_from(self.level)
        
        if self.id in Cybermon.movesets:
            moveset = Cybermon.movesets[self.id]
        else:
            print(f"WARN, {self.id} has no moveset!")
            moveset = {1: MoveId.TACKLE}

        if (level in moveset):
            move = MoveData.get_move(moveset[level])
            if len(self.moves) < 4:
                self.moves.append(move)
            else:
                self.pending_moves.append(move)

    def add_hp(self, hp, damage_type = None, ignore_max_hp = False):
        self.hp += int(round(hp))

        if self.hp < 0:
            self.hp = 0

        if self.hp > self.stats.max_hp and not ignore_max_hp:
            self.hp = self.stats.max_hp

        if hp < 0 and self.status == CybermonStatus.SLEEP and random.random() < 0.5:
            self.set_status(CybermonStatus.NONE)

        if hp < 0 and self.status == CybermonStatus.FREEZE and damage_type == CybermonType.FIRE:
            self.set_status(CybermonStatus.NONE)


    def revive(self):
        self.hp = self.stats.max_hp
        for move in self.moves:
            move["pp"] = move["max_pp"]

        self.stat_stages = [0, 0, 0, 0, 0, 0]
        self.set_status(CybermonStatus.NONE)
        self.damage_dealt = 0
        self.states = {}

    def attack(self, move, enemy):
        if (self.hp > 0 and enemy.hp > 0):
            move["pp"] -= 1
            attack_message = move["attack_function"](move, self, enemy)

            self.damage_dealt += 1

            return attack_message
        else:
            return "Tried attacking without health."

    def add_xp(self, xp):
        self.xp += xp

        while self.xp >= self.xp_to_level_up:
            decrease_xp_with = self.xp_to_level_up

            self.set_level(self.level + 1)
            
            self.xp -= decrease_xp_with

    templates = {
        CybermonId.Q: {"name": "Q'", "types": [CybermonType.BIRD], "evolve_into": CybermonId.GILTALIS, "evolve_at": 127, "hp": 42, "atk": 754, "def": 2, "satk": 2, "sdef": 2, "spd": -34},
        CybermonId.GUILGILE: {"name": "Guilgile", "types": [CybermonType.WATER], "evolve_into": CybermonId.GROTUILLE, "evolve_at": 18, "hp": 70, "atk": 39, "def": 45, "satk": 76, "sdef": 56, "spd": 32},
        CybermonId.GROTUILLE: {"name": "Grotuille", "types": [CybermonType.WATER, CybermonType.ROCK], "evolve_into": CybermonId.GUILLORENT, "evolve_at": 34, "hp": 89, "atk": 52, "def": 66, "satk": 94, "sdef": 72, "spd": 46},
        CybermonId.GUILLORENT: {"name": "Guillorent", "types": [CybermonType.WATER, CybermonType.ROCK], "evolve_into": None, "evolve_at": 0, "hp": 109, "atk": 72, "def": 95, "satk": 112, "sdef": 84, "spd": 52},
        CybermonId.CROCOAL: {"name": "Crocoal", "types": [CybermonType.FIRE], "evolve_into": CybermonId.FEUCROTA, "evolve_at": 16, "hp": 54, "atk": 65, "def": 43, "satk": 78, "sdef": 32, "spd": 76},
        CybermonId.FEUCROTA: {"name": "Feucrota", "types": [CybermonType.FIRE], "evolve_into": CybermonId.BURUNGIN, "evolve_at": 32, "hp": 66, "atk": 91, "def": 55, "satk": 102, "sdef": 47, "spd": 88},
        CybermonId.BURUNGIN: {"name": "Burungin", "types": [CybermonType.FIRE, CybermonType.DARK], "evolve_into": None, "evolve_at": 0, "hp": 71, "atk": 124, "def": 67, "satk": 125, "sdef": 61, "spd": 91},
        CybermonId.LIZEROON: {"name": "Lizeroon", "types": [CybermonType.GRASS], "evolve_into": CybermonId.DRACOROON, "evolve_at": 17, "hp": 62, "atk": 50, "def": 72, "satk": 32, "sdef": 54, "spd": 41},
        CybermonId.DRACOROON: {"name": "Dracoroon", "types": [CybermonType.GRASS], "evolve_into": CybermonId.MANDRAGOON, "evolve_at": 34, "hp": 89, "atk": 57, "def": 96, "satk": 54, "sdef": 76, "spd": 65},
        CybermonId.MANDRAGOON: {"name": "Mandragoon", "types": [CybermonType.GRASS, CybermonType.DRAGON], "evolve_into": None, "evolve_at": 0, "hp": 119, "atk": 68, "def": 111, "satk": 78, "sdef": 88, "spd": 86},
        CybermonId.FINNIAL: {"name": "Finnial", "types": [CybermonType.ELECTRIC], "evolve_into": CybermonId.TERMELC, "evolve_at": 15, "hp": 40, "atk": 52, "def": 55, "satk": 77, "sdef": 55, "spd": 73},
        CybermonId.TERMELC: {"name": "Termelc", "types": [CybermonType.ELECTRIC], "evolve_into": CybermonId.DISTRIKE, "evolve_at": 32, "hp": 58, "atk": 64, "def": 66, "satk": 90, "sdef": 66, "spd": 87},
        CybermonId.DISTRIKE: {"name": "Distrike", "types": [CybermonType.ELECTRIC, CybermonType.STEEL], "evolve_into": None, "evolve_at": 0, "hp": 80, "atk": 81, "def": 80, "satk": 120, "sdef": 85, "spd": 97},
        CybermonId.GRAVENDOU: {"name": "Gravendou", "types": [CybermonType.ROCK], "evolve_into": CybermonId.CRAGENDOU, "evolve_at": 16, "hp": 60, "atk": 78, "def": 72, "satk": 43, "sdef": 69, "spd": 40},
        CybermonId.CRAGENDOU: {"name": "Cragendou", "types": [CybermonType.ROCK], "evolve_into": CybermonId.QUARENDOU, "evolve_at": 32, "hp": 72, "atk": 106, "def": 92, "satk": 56, "sdef": 78, "spd": 45},
        CybermonId.QUARENDOU: {"name": "Quarendou", "types": [CybermonType.ROCK, CybermonType.GROUND], "evolve_into": None, "evolve_at": 0, "hp": 81, "atk": 127, "def": 106, "satk": 70, "sdef": 90, "spd": 51},
        CybermonId.MELANYAN: {"name": "Melanyan", "types": [CybermonType.DARK], "evolve_into": CybermonId.OCELUNA, "evolve_at": 14, "hp": 60, "atk": 70, "def": 42, "satk": 81, "sdef": 43, "spd": 75},
        CybermonId.OCELUNA: {"name": "Oceluna", "types": [CybermonType.DARK], "evolve_into": CybermonId.PANUMBRA, "evolve_at": 28, "hp": 73, "atk": 89, "def": 53, "satk": 101, "sdef": 56, "spd": 89},
        CybermonId.PANUMBRA: {"name": "Panumbra", "types": [CybermonType.DARK, CybermonType.PSYCHIC], "evolve_into": CybermonId.PANUMBRATEST2, "evolve_at": 101, "hp": 83, "atk": 104, "def": 59, "satk": 134, "sdef": 63, "spd": 102},
        CybermonId.PANUMBRATEST2: {"name": "PanumbraTEST2", "types": [CybermonType.DARK, CybermonType.GHOST], "evolve_into": CybermonId.PANUMBRATEST2, "evolve_at": 0, "hp": 1, "atk": 1, "def": 1, "satk": 1, "sdef": 1, "spd": 1},
        CybermonId.MUDDLEBEE: {"name": "Muddlebee", "types": [CybermonType.BUG], "evolve_into": CybermonId.DRILLINDER, "evolve_at": 8, "hp": 32, "atk": 58, "def": 28, "satk": 28, "sdef": 28, "spd": 35},
        CybermonId.DRILLINDER: {"name": "Drillinder", "types": [CybermonType.BUG, CybermonType.GROUND], "evolve_into": CybermonId.DAUVESPA, "evolve_at": 20, "hp": 41, "atk": 71, "def": 45, "satk": 38, "sdef": 38, "spd": 57},
        CybermonId.DAUVESPA: {"name": "Dauvespa", "types": [CybermonType.BUG, CybermonType.GROUND], "evolve_into": None, "evolve_at": 0, "hp": 51, "atk": 88, "def": 61, "satk": 54, "sdef": 54, "spd": 82},
        CybermonId.POMPILLAR: {"name": "Pompillar", "types": [CybermonType.BUG], "evolve_into": CybermonId.GILTALIS, "evolve_at": 10, "hp": 34, "atk": 36, "def": 36, "satk": 45, "sdef": 40, "spd": 42},
        CybermonId.GILTALIS: {"name": "Giltalis", "types": [CybermonType.BUG, CybermonType.POISON], "evolve_into": CybermonId.VANITARCH, "evolve_at": 18, "hp": 46, "atk": 48, "def": 48, "satk": 58, "sdef": 52, "spd": 51},
        CybermonId.VANITARCH: {"name": "Vanitarch", "types": [CybermonType.BUG, CybermonType.POISON], "evolve_into": None, "evolve_at": 0, "hp": 54, "atk": 56, "def": 54, "satk": 78, "sdef": 64, "spd": 79},
        CybermonId.GOWATU: {"name": "Gowatu", "types": [CybermonType.FLYING], "evolve_into": CybermonId.TURATAL, "evolve_at": 20, "hp": 56, "atk": 72, "def": 46, "satk": 34, "sdef": 28, "spd": 59},
        CybermonId.TURATAL: {"name": "Turatal", "types": [CybermonType.FLYING], "evolve_into": CybermonId.JOSUCHE, "evolve_at": 38, "hp": 68, "atk": 98, "def": 62, "satk": 48, "sdef": 36, "spd": 84},
        CybermonId.JOSUCHE: {"name": "Josuche", "types": [CybermonType.FIGHTING, CybermonType.FLYING], "evolve_into": None, "evolve_at": 0, "hp": 84, "atk": 128, "def": 84, "satk": 62, "sdef": 48, "spd": 102},
        CybermonId.FAINTRICK: {"name": "Faintrick", "types": [CybermonType.NORMAL, CybermonType.GHOST], "evolve_into": CybermonId.FRAUDKILL, "evolve_at": 22, "hp": 14, "atk": 32, "def": 86, "satk": 62, "sdef": 68, "spd": 56},
        CybermonId.FRAUDKILL: {"name": "Fraudkill", "types": [CybermonType.NORMAL, CybermonType.GHOST], "evolve_into": None, "evolve_at": 0, "hp": 32, "atk": 56, "def": 120, "satk": 102, "sdef": 98, "spd": 88},
        CybermonId.DRIBBINO: {"name": "Dribbino", "types": [CybermonType.FLYING], "evolve_into": CybermonId.ACCELERET, "evolve_at": 18, "hp": 15, "atk": 46, "def": 32, "satk": 39, "sdef": 24, "spd": 86},
        CybermonId.ACCELERET: {"name": "Acceleret", "types": [CybermonType.FLYING], "evolve_into": CybermonId.VELOSWIFT, "evolve_at": 34, "hp": 22, "atk": 69, "def": 46, "satk": 58, "sdef": 32, "spd": 116},
        CybermonId.VELOSWIFT: {"name": "Veloswift", "types": [CybermonType.FLYING], "evolve_into": None, "evolve_at": 0, "hp": 28, "atk": 86, "def": 57, "satk": 76, "sdef": 45, "spd": 151},
        CybermonId.ACAFIA: {"name": "Acafia", "types": [CybermonType.GRASS], "evolve_into": CybermonId.CATALCIA, "evolve_at": 19, "hp": 70, "atk": 84, "def": 64, "satk": 18, "sdef": 26, "spd": 18},
        CybermonId.CATALCIA: {"name": "Catalcia", "types": [CybermonType.GRASS], "evolve_into": CybermonId.BOSSORNA, "evolve_at": 35, "hp": 96, "atk": 106, "def": 84, "satk": 23, "sdef": 34, "spd": 24},
        CybermonId.BOSSORNA: {"name": "Bossorna", "types": [CybermonType.GRASS, CybermonType.POISON], "evolve_into": None, "evolve_at": 0, "hp": 126, "atk": 144, "def": 112, "satk": 32, "sdef": 46, "spd": 31},
        CybermonId.GUMBWAAL: {"name": "Gumbwaal", "types": [CybermonType.NORMAL], "evolve_into": CybermonId.HANDERWAAL, "evolve_at": 20, "hp": 34, "atk": 77, "def": 20, "satk": 60, "sdef": 20, "spd": 33},
        CybermonId.HANDERWAAL: {"name": "Handerwaal", "types": [CybermonType.NORMAL], "evolve_into": None, "evolve_at": 0, "hp": 54, "atk": 120, "def": 32, "satk": 95, "sdef": 32, "spd": 51},
        CybermonId.GASTREL: {"name": "Gastrel", "types": [CybermonType.FLYING, CybermonType.POISON], "evolve_into": CybermonId.SKYRATE, "evolve_at": 25, "hp": 26, "atk": 61, "def": 21, "satk": 55, "sdef": 14, "spd": 71},
        CybermonId.SKYRATE: {"name": "Skyrate", "types": [CybermonType.FLYING, CybermonType.POISON], "evolve_into": None, "evolve_at": 0, "hp": 45, "atk": 99, "def": 32, "satk": 87, "sdef": 22, "spd": 109},
        CybermonId.ROBONBO: {"name": "Robonbo", "types": [CybermonType.FIRE, CybermonType.FLYING], "evolve_into": CybermonId.ROBUTANE, "evolve_at": 28, "hp": 12, "atk": 54, "def": 30, "satk": 43, "sdef": 39, "spd": 55},
        CybermonId.ROBUTANE: {"name": "Robutane", "types": [CybermonType.FIRE, CybermonType.FLYING], "evolve_into": None, "evolve_at": 0, "hp": 19, "atk": 78, "def": 47, "satk": 64, "sdef": 58, "spd": 74},
        CybermonId.JACKRAVAGE: {"name": "Jackravage", "types": [CybermonType.NORMAL, CybermonType.FIGHTING], "evolve_into": None, "evolve_at": 0, "hp": 79, "atk": 129, "def": 91, "satk": 21, "sdef": 22, "spd": 83},
        CybermonId.TINIMER: {"name": "Tinimer", "types": [CybermonType.BUG], "evolve_into": CybermonId.ALTAVAULT, "evolve_at": 20, "hp": 59, "atk": 30, "def": 53, "satk": 39, "sdef": 37, "spd": 10},
        CybermonId.ALTAVAULT: {"name": "Altavault", "types": [CybermonType.BUG, CybermonType.ROCK], "evolve_into": None, "evolve_at": 0, "hp": 97, "atk": 50, "def": 90, "satk": 68, "sdef": 64, "spd": 16},
        CybermonId.TUNNOWL: {"name": "Tunnowl", "types": [CybermonType.GROUND], "evolve_into": CybermonId.GRYPHAULT, "evolve_at": 25, "hp": 19, "atk": 44, "def": 16, "satk": 39, "sdef": 27, "spd": 66},
        CybermonId.GRYPHAULT: {"name": "Gryphault", "types": [CybermonType.GROUND], "evolve_into": CybermonId.SEISMOGRYPH, "evolve_at": 40, "hp": 34, "atk": 70, "def": 25, "satk": 60, "sdef": 42, "spd": 106},
        CybermonId.SEISMOGRYPH: {"name": "Seismogryph", "types": [CybermonType.GROUND, CybermonType.FLYING], "evolve_into": None, "evolve_at": 0, "hp": 48, "atk": 92, "def": 36, "satk": 83, "sdef": 58, "spd": 146},
        CybermonId.SWIMMOLE: {"name": "Swimmole", "types": [CybermonType.WATER], "evolve_into": CybermonId.NEURAMOLE, "evolve_at": 23, "hp": 35, "atk": 44, "def": 44, "satk": 42, "sdef": 48, "spd": 14},
        CybermonId.NEURAMOLE: {"name": "Neuramole", "types": [CybermonType.WATER, CybermonType.PSYCHIC], "evolve_into": CybermonId.NOSTRAMOLE, "evolve_at": 38, "hp": 49, "atk": 71, "def": 68, "satk": 66, "sdef": 66, "spd": 20},
        CybermonId.NOSTRAMOLE: {"name": "Nostramole", "types": [CybermonType.WATER, CybermonType.PSYCHIC], "evolve_into": None, "evolve_at": 0, "hp": 78, "atk": 103, "def": 101, "satk": 100, "sdef": 103, "spd": 34},
        CybermonId.SHRYMPH: {"name": "Shrymph", "types": [CybermonType.WATER], "evolve_into": CybermonId.SKRIMPISH, "evolve_at": 20, "hp": 9, "atk": 54, "def": 27, "satk": 13, "sdef": 12, "spd": 53},
        CybermonId.SKRIMPISH: {"name": "Skrimpish", "types": [CybermonType.WATER], "evolve_into": CybermonId.DUELANTIS, "evolve_at": 34, "hp": 16, "atk": 98, "def": 50, "satk": 24, "sdef": 23, "spd": 82},
        CybermonId.DUELANTIS: {"name": "Duelantis", "types": [CybermonType.WATER, CybermonType.FIGHTING], "evolve_into": None, "evolve_at": 0, "hp": 23, "atk": 143, "def": 69, "satk": 34, "sdef": 35, "spd": 127},
        CybermonId.WYRMAL: {"name": "Wyrmal", "types": [CybermonType.WATER, CybermonType.FIRE], "evolve_into": CybermonId.VENTORM, "evolve_at": 28, "hp": 16, "atk": 50, "def": 27, "satk": 57, "sdef": 40, "spd": 55},
        CybermonId.VENTORM: {"name": "Ventorm", "types": [CybermonType.WATER, CybermonType.FIRE], "evolve_into": None, "evolve_at": 0, "hp": 32, "atk": 100, "def": 55, "satk": 112, "sdef": 78, "spd": 104},
        CybermonId.LARVILL: {"name": "Larvill", "types": [CybermonType.WATER], "evolve_into": CybermonId.CAVERNOLM, "evolve_at": 20, "hp": 18, "atk": 25, "def": 8, "satk": 26, "sdef": 18, "spd": 4},
        CybermonId.CAVERNOLM: {"name": "Cavernolm", "types": [CybermonType.WATER], "evolve_into": None, "evolve_at": 0, "hp": 85, "atk": 95, "def": 81, "satk": 103, "sdef": 89, "spd": 84},
        CybermonId.STICKIBAT: {"name": "Stickibat", "types": [CybermonType.ELECTRIC], "evolve_into": CybermonId.TRICKIBAT, "evolve_at": 25, "hp": 23, "atk": 25, "def": 34, "satk": 69, "sdef": 55, "spd": 30},
        CybermonId.TRICKIBAT: {"name": "Trickibat", "types": [CybermonType.ELECTRIC], "evolve_into": CybermonId.BANDIBAT, "evolve_at": 37, "hp": 38, "atk": 37, "def": 52, "satk": 112, "sdef": 90, "spd": 52},
        CybermonId.BANDIBAT: {"name": "Bandibat", "types": [CybermonType.ELECTRIC, CybermonType.DARK], "evolve_into": None, "evolve_at": 0, "hp": 48, "atk": 49, "def": 64, "satk": 145, "sdef": 116, "spd": 63},
        CybermonId.DASFIX: {"name": "Dasfix", "types": [CybermonType.GHOST, CybermonType.STEEL], "evolve_into": CybermonId.MALRAJA, "evolve_at": 36, "hp": 80, "atk": 28, "def": 60, "satk": 40, "sdef": 80, "spd": 60},
        CybermonId.MALRAJA: {"name": "Malraja", "types": [CybermonType.GHOST, CybermonType.STEEL], "evolve_into": None, "evolve_at": 0, "hp": 130, "atk": 40, "def": 139, "satk": 70, "sdef": 110, "spd": 60},
    
        CybermonId.DECAMARK1: {"name": "?" * 10, "types": [CybermonType.NORMAL], "evolve_into": None, "evolve_at": 0, "hp": 1, "atk": 1, "def": 1, "satk": 1, "sdef": 1, "spd": 0},
        CybermonId.DECAMARK2: {"name": "?" * 10, "types": [CybermonType.NORMAL], "evolve_into": None, "evolve_at": 0, "hp": 1, "atk": 1, "def": 1, "satk": 1, "sdef": 1, "spd": 0},
        CybermonId.DECAMARK3: {"name": "?" * 10, "types": [CybermonType.NORMAL], "evolve_into": None, "evolve_at": 0, "hp": 1, "atk": 1, "def": 1, "satk": 1, "sdef": 1, "spd": 255},
        CybermonId.DECAMARK4: {"name": "?" * 10, "types": [CybermonType.NORMAL], "evolve_into": None, "evolve_at": 0, "hp": 1, "atk": 1, "def": 1, "satk": 1, "sdef": 1, "spd": -32768}
    }

    movesets = {
        CybermonId.Q: {
            1: MoveId.B_,
            2: MoveId.BASH,
            3: MoveId.BITE,
            4: MoveId.POWER_HIT,
            8: MoveId.WATER_JET,
            104: MoveId.SCALD,
            255: MoveId._	
        },

        CybermonId.GUILGILE: {
            1: MoveId.TACKLE,
            2: MoveId.WATER_JET,
            4: MoveId.CURL_UP,
            7: MoveId.BITE,
            9: MoveId.CARESS,
            11: MoveId.PSYCH_UP,
            15: MoveId.POWER_HIT	
        },

        CybermonId.GROTUILLE: {
            1: MoveId.TACKLE,
            2: MoveId.WATER_JET,
            4: MoveId.CURL_UP,
            7: MoveId.BITE,
            9: MoveId.CARESS,
            11: MoveId.PSYCH_UP,
            15: MoveId.POWER_HIT,
            18: MoveId.SONIC_SPLASH,
            22: MoveId.ROCK_THROW,
            26: MoveId.EARTHSHOCK,
            28: MoveId.ALL_OUT_ATTACK,
            31: MoveId.EROSION	
        },

        CybermonId.GUILLORENT: {
            1: MoveId.TACKLE,
            2: MoveId.WATER_JET,
            4: MoveId.CURL_UP,
            7: MoveId.BITE,
            9: MoveId.CARESS,
            11: MoveId.PSYCH_UP,
            15: MoveId.POWER_HIT,
            18: MoveId.SONIC_SPLASH,
            22: MoveId.ROCK_THROW,
            26: MoveId.EARTHSHOCK,
            28: MoveId.ALL_OUT_ATTACK,
            31: MoveId.EROSION,
            34: MoveId.DELUGE,
            36: MoveId.LANDSLIDE,
            41: MoveId.POWERBLAST,
            45: MoveId.EROSION,
            51: MoveId.BOULDERFALL,
            56: MoveId.TIDAL_WAVE	
        },

        CybermonId.CROCOAL: {
            1: MoveId.TACKLE,
            2: MoveId.SCALD,
            5: MoveId.CURL_UP,
            8: MoveId.BITE,
            11: MoveId.PSYCH_UP	
        },

        CybermonId.FEUCROTA: {
            1: MoveId.TACKLE,
            2: MoveId.SCALD,
            5: MoveId.CURL_UP,
            8: MoveId.BITE,
            11: MoveId.PSYCH_UP,
            16: MoveId.SCORCH,
            18: MoveId.SHADOW_STRIKE,
            22: MoveId.SABOTAGE,
            25: MoveId.IGNITE,
            27: MoveId.JUMPING_KICK,
            30: MoveId.POWER_HIT	
        },

        CybermonId.BURUNGIN: {
            1: MoveId.TACKLE,
            2: MoveId.SCALD,
            5: MoveId.CURL_UP,
            8: MoveId.BITE,
            11: MoveId.PSYCH_UP,
            16: MoveId.SCORCH,
            18: MoveId.SHADOW_STRIKE,
            22: MoveId.SABOTAGE,
            25: MoveId.IGNITE,
            27: MoveId.JUMPING_KICK,
            30: MoveId.POWER_HIT,
            32: MoveId.VOLCANIC_CUTTER,
            35: MoveId.DARKCUTTER,
            42: MoveId.CAUTERIZE,
            48: MoveId.BACKSTAB,
            54: MoveId.IMMOLATE	
        },

        CybermonId.LIZEROON: {
            1: MoveId.TACKLE,
            2: MoveId.SPIT_SEED,
            5: MoveId.BITE,
            9: MoveId.ABSORB,
            13: MoveId.PHOTOSYNTHESIS	
        },

        CybermonId.DRACOROON: {
            1: MoveId.TACKLE,
            2: MoveId.SPIT_SEED,
            5: MoveId.BITE,
            9: MoveId.ABSORB,
            13: MoveId.PHOTOSYNTHESIS,
            17: MoveId.SEEDCANNON,
            21: MoveId.SWOOP,
            23: MoveId.LIFEDRAIN,
            26: MoveId.COALESCE_AURA,
            29: MoveId.POWERBLAST	
        },

        CybermonId.MANDRAGOON: {
            1: MoveId.TACKLE,
            2: MoveId.SPIT_SEED,
            5: MoveId.BITE,
            9: MoveId.ABSORB,
            13: MoveId.PHOTOSYNTHESIS,
            17: MoveId.SEEDCANNON,
            21: MoveId.SWOOP,
            23: MoveId.LIFEDRAIN,
            26: MoveId.COALESCE_AURA,
            29: MoveId.POWERBLAST,
            34: MoveId.INFESTATION,
            38: MoveId.WING_BEAT,
            41: MoveId.METEOR,
            46: MoveId.DIVEBOMB,
            52: MoveId.ARCANIC_BLAST,
            55: MoveId.OVERGROWTH	
        },

        CybermonId.FINNIAL: {
            1: MoveId.TACKLE,
            4: MoveId.SHOCK,
            7: MoveId.BASH,
            10: MoveId.PSYCH_UP	
        },

        CybermonId.TERMELC: {
            1: MoveId.TACKLE,
            4: MoveId.SHOCK,
            7: MoveId.BASH,
            10: MoveId.PSYCH_UP,
            15: MoveId.LIGHTNINGBOLT,
            23: MoveId.THUNDER,
            30: MoveId.ALLOY_STRIKE	
        },

        CybermonId.DISTRIKE: {
            1: MoveId.TACKLE,
            4: MoveId.SHOCK,
            7: MoveId.BASH,
            10: MoveId.PSYCH_UP,
            15: MoveId.LIGHTNINGBOLT,
            23: MoveId.THUNDER,
            30: MoveId.ALLOY_STRIKE,
            32: MoveId.POWERBLAST,
            39: MoveId.ALL_OUT_ATTACK,
            44: MoveId.TITANIUM_INDUCTION,
            52: MoveId.LETHAL_CURRENT	
        },

        CybermonId.GRAVENDOU: {
            2: MoveId.BITE,
            6: MoveId.TACKLE,
            9: MoveId.ROCK_THROW,
            14: MoveId.SLAP	
        },

        CybermonId.CRAGENDOU: {
            2: MoveId.BITE,
            6: MoveId.TACKLE,
            9: MoveId.ROCK_THROW,
            14: MoveId.SLAP,
            16: MoveId.POWER_HIT,
            18: MoveId.LANDSLIDE,
            23: MoveId.EARTHQUAKE,
            29: MoveId.POWERBLAST	
        },

        CybermonId.QUARENDOU: {
            2: MoveId.BITE,
            6: MoveId.TACKLE,
            9: MoveId.ROCK_THROW,
            14: MoveId.SLAP,
            16: MoveId.POWER_HIT,
            18: MoveId.LANDSLIDE,
            23: MoveId.EARTHQUAKE,
            29: MoveId.POWERBLAST,
            32: MoveId.FISSURE,
            40: MoveId.ALL_OUT_ATTACK,
            51: MoveId.MOUNTAIN_BREAKER,
            58: MoveId.ERUPTION	
        },

        CybermonId.MELANYAN: {
            1: MoveId.BITE,
            6: MoveId.BASH,
            11: MoveId.PSYCH_UP	
        },

        CybermonId.OCELUNA: {
            1: MoveId.BITE,
            6: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            14: MoveId.MIND_TRICKS,
            19: MoveId.JUMPING_KICK,
            24: MoveId.FLAY,
            26: MoveId.SABOTAGE	
        },

        CybermonId.PANUMBRA: {
            1: MoveId.BITE,
            6: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            14: MoveId.MIND_TRICKS,
            19: MoveId.JUMPING_KICK,
            24: MoveId.FLAY,
            26: MoveId.SABOTAGE,
            28: MoveId.BACKSTAB,
            32: MoveId.FLAY,
            45: MoveId.MIND_BLAST,
            47: MoveId.PARADOX_CHANNELING	
        },

        CybermonId.PANUMBRATEST2: {
            1: MoveId.TACKLE,
            47: MoveId.REMOVE_THIS1,
            57: MoveId.REMOVE_THIS2,
            67: MoveId.REMOVE_THIS3	
        },

        CybermonId.MUDDLEBEE: {
            1: MoveId.SPIT_SEED,
            3: MoveId.NIP,
            5: MoveId.BITE	
        },

        CybermonId.DRILLINDER: {
            1: MoveId.SPIT_SEED,
            3: MoveId.NIP,
            5: MoveId.BITE,
            11: MoveId.ABSORB,
            17: MoveId.ARMOUR_CRUSH,
            19: MoveId.PUPATE	
        },

        CybermonId.DAUVESPA: {
            1: MoveId.SPIT_SEED,
            3: MoveId.NIP,
            5: MoveId.BITE,
            11: MoveId.ABSORB,
            17: MoveId.ARMOUR_CRUSH,
            19: MoveId.PUPATE,
            21: MoveId.CARESS,
            28: MoveId.SWARM,
            34: MoveId.INFESTATION,
            42: MoveId.ALL_OUT_ATTACK	
        },

        CybermonId.POMPILLAR: {
            1: MoveId.NIP,
            4: MoveId.BITE,
            7: MoveId.BASH	
        },

        CybermonId.GILTALIS: {
            1: MoveId.NIP,
            4: MoveId.BITE,
            7: MoveId.BASH,
            10: MoveId.PSYCH_UP,
            12: MoveId.POISON_SPIT,
            15: MoveId.CARESS	
        },

        CybermonId.VANITARCH: {
            1: MoveId.NIP,
            4: MoveId.BITE,
            7: MoveId.BASH,
            10: MoveId.PSYCH_UP,
            12: MoveId.POISON_SPIT,
            15: MoveId.CARESS,
            20: MoveId.ARMOUR_CRUSH,
            26: MoveId.SWARM,
            32: MoveId.DEBILITATE,
            39: MoveId.NEEDLER	
        },

        CybermonId.GOWATU: {
            1: MoveId.BITE,
            5: MoveId.BASH,
            11: MoveId.CURL_UP,
            14: MoveId.SLAP	
        },

        CybermonId.TURATAL: {
            1: MoveId.BITE,
            5: MoveId.BASH,
            11: MoveId.CURL_UP,
            14: MoveId.SLAP,
            23: MoveId.POWER_HIT,
            26: MoveId.SHOCK_PALM,
            29: MoveId.WING_BEAT,
            35: MoveId.ALL_OUT_ATTACK	
        },

        CybermonId.JOSUCHE: {
            1: MoveId.BITE,
            5: MoveId.BASH,
            11: MoveId.CURL_UP,
            14: MoveId.SLAP,
            23: MoveId.POWER_HIT,
            26: MoveId.SHOCK_PALM,
            29: MoveId.WING_BEAT,
            35: MoveId.ALL_OUT_ATTACK,
            38: MoveId.POWERBLAST,
            42: MoveId.DIVEBOMB,
            50: MoveId.LETHAL_FIST	
        },

        CybermonId.FAINTRICK: {
            1: MoveId.TRICK,
            6: MoveId.BASH,
            10: MoveId.CURL_UP,
            15: MoveId.SLAP,
            20: MoveId.CARESS	
        },

        CybermonId.FRAUDKILL: {
            1: MoveId.TRICK,
            6: MoveId.BASH,
            10: MoveId.CURL_UP,
            15: MoveId.SLAP,
            20: MoveId.CARESS,
            26: MoveId.POSESS,
            31: MoveId.SABOTAGE,
            38: MoveId.ALL_OUT_ATTACK,
            45: MoveId.EXORCISM	
        },

        CybermonId.DRIBBINO: {
            1: MoveId.TACKLE,
            6: MoveId.BITE,
            10: MoveId.CURL_UP,
            15: MoveId.SWOOP	
        },

        CybermonId.ACCELERET: {
            1: MoveId.TACKLE,
            6: MoveId.BITE,
            10: MoveId.CURL_UP,
            15: MoveId.SWOOP,
            19: MoveId.POWER_HIT,
            24: MoveId.WING_BEAT,
            30: MoveId.ALL_OUT_ATTACK	
        },

        CybermonId.VELOSWIFT: {
            1: MoveId.TACKLE,
            6: MoveId.BITE,
            10: MoveId.CURL_UP,
            15: MoveId.SWOOP,
            19: MoveId.POWER_HIT,
            24: MoveId.WING_BEAT,
            30: MoveId.ALL_OUT_ATTACK,
            32: MoveId.POWERBLAST,
            36: MoveId.LIMBER,
            41: MoveId.ALL_OUT_ATTACK,
            46: MoveId.DIVEBOMB	
        },

        CybermonId.ACAFIA: {
            1: MoveId.SPIT_SEED,
            5: MoveId.ABSORB,
            9: MoveId.IMBIBE,
            13: MoveId.PHOTOSYNTHESIS	
        },

        CybermonId.CATALCIA: {
            1: MoveId.SPIT_SEED,
            5: MoveId.ABSORB,
            9: MoveId.IMBIBE,
            13: MoveId.PHOTOSYNTHESIS,
            19: MoveId.SEEDCANNON,
            22: MoveId.LIFEDRAIN,
            29: MoveId.INFESTATION	
        },

        CybermonId.BOSSORNA: {
            1: MoveId.SPIT_SEED,
            5: MoveId.ABSORB,
            9: MoveId.IMBIBE,
            13: MoveId.PHOTOSYNTHESIS,
            19: MoveId.SEEDCANNON,
            22: MoveId.LIFEDRAIN,
            29: MoveId.INFESTATION,
            35: MoveId.DEBILITATE,
            42: MoveId.FATAL_POISON,
            51: MoveId.OVERGROWTH	
        },

        CybermonId.GUMBWAAL: {
            1: MoveId.BITE,
            6: MoveId.BASH,
            9: MoveId.CURL_UP,
            13: MoveId.PSYCH_UP	
        },

        CybermonId.HANDERWAAL: {
            1: MoveId.BITE,
            6: MoveId.BASH,
            9: MoveId.CURL_UP,
            13: MoveId.PSYCH_UP,
            21: MoveId.POWER_HIT,
            28: MoveId.INFESTATION,
            35: MoveId.ALL_OUT_ATTACK,
            39: MoveId.LETHAL_FIST	
        },

        CybermonId.GASTREL: {
            1: MoveId.FEATHER_WHIP,
            4: MoveId.BITE,
            7: MoveId.BASH,
            13: MoveId.CURL_UP,
            19: MoveId.SLAP	
        },

        CybermonId.SKYRATE: {
            1: MoveId.FEATHER_WHIP,
            4: MoveId.BITE,
            7: MoveId.BASH,
            13: MoveId.CURL_UP,
            19: MoveId.SLAP,
            25: MoveId.IMBIBE,
            28: MoveId.WING_BEAT,
            32: MoveId.DEBILITATE,
            38: MoveId.FATAL_POISON,
            47: MoveId.DIVEBOMB	
        },

        CybermonId.ROBONBO: {
            2: MoveId.SCALD,
            4: MoveId.FEATHER_WHIP,
            7: MoveId.BASH,
            11: MoveId.SLAP,
            15: MoveId.IGNITE,
            20: MoveId.CARESS	
        },

        CybermonId.ROBUTANE: {
            2: MoveId.SCALD,
            4: MoveId.FEATHER_WHIP,
            7: MoveId.BASH,
            11: MoveId.SLAP,
            15: MoveId.IGNITE,
            20: MoveId.CARESS,
            28: MoveId.WING_BEAT,
            33: MoveId.CAUTERIZE,
            39: MoveId.ALL_OUT_ATTACK,
            46: MoveId.DIVEBOMB,
            53: MoveId.IMMOLATE	
        },

        CybermonId.JACKRAVAGE: {
            1: MoveId.TACKLE,
            5: MoveId.CURL_UP,
            9: MoveId.BITE,
            13: MoveId.SLAP,
            17: MoveId.CARESS,
            22: MoveId.SHOCK_PALM,
            28: MoveId.ALL_OUT_ATTACK,
            33: MoveId.POWERBLAST,
            39: MoveId.LETHAL_FIST	
        },

        CybermonId.TINIMER: {
            1: MoveId.TACKLE,
            3: MoveId.NIP,
            6: MoveId.BASH,
            10: MoveId.CURL_UP,
            14: MoveId.SLAP,
            18: MoveId.INFEST	
        },

        CybermonId.ALTAVAULT: {
            1: MoveId.TACKLE,
            3: MoveId.NIP,
            6: MoveId.BASH,
            10: MoveId.CURL_UP,
            14: MoveId.SLAP,
            18: MoveId.INFEST,
            20: MoveId.ARMOUR_CRUSH,
            25: MoveId.CARESS,
            28: MoveId.TECTONICS,
            33: MoveId.POWERBLAST,
            39: MoveId.NEEDLER,
            48: MoveId.MOUNTAIN_BREAKER	
        },

        CybermonId.TUNNOWL: {
            1: MoveId.BITE,
            5: MoveId.EARTHSHOCK,
            9: MoveId.CURL_UP,
            13: MoveId.SLAP,
            18: MoveId.POWER_HIT	
        },

        CybermonId.GRYPHAULT: {
            1: MoveId.BITE,
            5: MoveId.EARTHSHOCK,
            9: MoveId.CURL_UP,
            13: MoveId.SLAP,
            18: MoveId.POWER_HIT,
            25: MoveId.LIMBER,
            29: MoveId.WING_BEAT,
            32: MoveId.EARTHQUAKE,
            38: MoveId.ALL_OUT_ATTACK	
        },

        CybermonId.SEISMOGRYPH: {
            1: MoveId.BITE,
            5: MoveId.EARTHSHOCK,
            9: MoveId.CURL_UP,
            13: MoveId.SLAP,
            18: MoveId.POWER_HIT,
            25: MoveId.LIMBER,
            29: MoveId.WING_BEAT,
            32: MoveId.EARTHQUAKE,
            38: MoveId.ALL_OUT_ATTACK,
            40: MoveId.DIVEBOMB,
            43: MoveId.FISSURE,
            52: MoveId.ERUPTION	
        },

        CybermonId.SWIMMOLE: {
            1: MoveId.WATER_JET,
            4: MoveId.BITE,
            7: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            15: MoveId.SLAP,
            20: MoveId.POWER_HIT	
        },

        CybermonId.NEURAMOLE: {
            1: MoveId.WATER_JET,
            4: MoveId.BITE,
            7: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            15: MoveId.SLAP,
            20: MoveId.POWER_HIT,
            23: MoveId.SONIC_SPLASH,
            27: MoveId.FLAY,
            31: MoveId.DELUGE,
            35: MoveId.TELEPATHIC_BLAST	
        },

        CybermonId.NOSTRAMOLE: {
            1: MoveId.WATER_JET,
            4: MoveId.BITE,
            7: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            15: MoveId.SLAP,
            20: MoveId.POWER_HIT,
            23: MoveId.SONIC_SPLASH,
            27: MoveId.FLAY,
            31: MoveId.DELUGE,
            35: MoveId.TELEPATHIC_BLAST,
            38: MoveId.MIND_BLAST,
            46: MoveId.PARADOX_CHANNELING,
            53: MoveId.TIDAL_WAVE	
        },

        CybermonId.SHRYMPH: {
            1: MoveId.WATER_JET,
            4: MoveId.TACKLE,
            7: MoveId.BASH,
            10: MoveId.CURL_UP,
            14: MoveId.UPPERCUT	
        },

        CybermonId.SKRIMPISH: {
            1: MoveId.WATER_JET,
            4: MoveId.TACKLE,
            7: MoveId.BASH,
            10: MoveId.CURL_UP,
            14: MoveId.UPPERCUT,
            22: MoveId.SHOCK_PALM,
            28: MoveId.DELUGE,
            31: MoveId.POWERBLAST	
        },

        CybermonId.DUELANTIS: {
            1: MoveId.WATER_JET,
            4: MoveId.TACKLE,
            7: MoveId.BASH,
            10: MoveId.CURL_UP,
            14: MoveId.UPPERCUT,
            22: MoveId.SHOCK_PALM,
            28: MoveId.DELUGE,
            31: MoveId.POWERBLAST,
            34: MoveId.GLACIER_SLAM,
            37: MoveId.DELUGE,
            40: MoveId.LETHAL_FIST,
            47: MoveId.TIDAL_WAVE	
        },

        CybermonId.WYRMAL: {
            1: MoveId.WATER_JET,
            3: MoveId.EARTHSHOCK,
            8: MoveId.BITE,
            11: MoveId.UPPERCUT,
            18: MoveId.IGNITE,
            20: MoveId.SHOCK_PALM	
        },

        CybermonId.VENTORM: {
            1: MoveId.WATER_JET,
            3: MoveId.EARTHSHOCK,
            8: MoveId.BITE,
            11: MoveId.UPPERCUT,
            18: MoveId.IGNITE,
            20: MoveId.SHOCK_PALM,
            28: MoveId.BURNING_COLD,
            29: MoveId.GLACIER_SLAM,
            35: MoveId.POWERBLAST,
            42: MoveId.ALL_OUT_ATTACK,
            50: MoveId.ARCTIC_FREEZE,
            58: MoveId.ERUPTION	
        },

        CybermonId.LARVILL: {
            1: MoveId.WATER_JET,
            4: MoveId.BITE,
            7: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            15: MoveId.FROSTBITE	
        },

        CybermonId.CAVERNOLM: {
            1: MoveId.WATER_JET,
            4: MoveId.BITE,
            7: MoveId.BASH,
            11: MoveId.PSYCH_UP,
            15: MoveId.FROSTBITE,
            20: MoveId.JUMPING_KICK,
            28: MoveId.BURNING_COLD,
            31: MoveId.DELUGE,
            37: MoveId.POWERBLAST,
            46: MoveId.TIDAL_WAVE	
        },

        CybermonId.STICKIBAT: {
            1: MoveId.FEATHER_WHIP,
            5: MoveId.BITE,
            12: MoveId.CURL_UP,
            15: MoveId.SLAP,
            20: MoveId.CARESS	
        },

        CybermonId.TRICKIBAT: {
            1: MoveId.FEATHER_WHIP,
            5: MoveId.BITE,
            12: MoveId.CURL_UP,
            15: MoveId.SLAP,
            20: MoveId.CARESS,
            25: MoveId.THUNDER,
            29: MoveId.WING_BEAT,
            35: MoveId.ALL_OUT_ATTACK	
        },

        CybermonId.BANDIBAT: {
            1: MoveId.FEATHER_WHIP,
            5: MoveId.BITE,
            12: MoveId.CURL_UP,
            15: MoveId.SLAP,
            20: MoveId.CARESS,
            25: MoveId.THUNDER,
            29: MoveId.WING_BEAT,
            35: MoveId.ALL_OUT_ATTACK,
            39: MoveId.BACKSTAB,
            45: MoveId.LETHAL_CURRENT,
            48: MoveId.DIVEBOMB	
        },

        CybermonId.DASFIX: {
            1: MoveId.TRICK,
            3: MoveId.RIDDLE,
            6: MoveId.BITE,
            9: MoveId.BASH,
            13: MoveId.PSYCH_UP,
            16: MoveId.PRECISION_SHOT,
            25: MoveId.SHED_CASING,
            29: MoveId.POSESS	
        },

        CybermonId.MALRAJA: {
            1: MoveId.TRICK,
            3: MoveId.RIDDLE,
            6: MoveId.BITE,
            9: MoveId.BASH,
            13: MoveId.PSYCH_UP,
            16: MoveId.PRECISION_SHOT,
            25: MoveId.SHED_CASING,
            29: MoveId.POSESS,
            34: MoveId.SUPER_GYROSCOPE,
            37: MoveId.POLISH,
            41: MoveId.EXORCISM,
            47: MoveId.TITANIUM_INDUCTION	
        },

        CybermonId.DECAMARK1: {},
        CybermonId.DECAMARK2: {},
        CybermonId.DECAMARK3: {},
        CybermonId.DECAMARK4: {}
    }
