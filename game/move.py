from .enums import *
import random

class MoveData:
    
    def get_base_damage(move, attacker, enemy):
        opts = move["attack_options"]
        attack = attacker.stats.patk
        defense = enemy.stats.pdef

        if move["dmg_type"] == MoveDamageType.SPECIAL:
            attack = attacker.stats.satk
            defense = enemy.stats.sdef

        power = move["power"]
        if power == -1:
            power = 0

        damage = power * (attack / defense) * (2 * attacker.level / 5 + 2)
        damage /= 50
        damage += 2

        # STAB
        if move["type"] in attacker.types:
            damage *= 1.3
        
        # EFFECTIVENESS
        effectiveness = 1
        for t in enemy.types:
            effectiveness *= MoveData.effectiveness_multiplier[MoveData.matchups[move["type"]][t]]

        damage *= effectiveness

        # CRIT
        crit = False
        
        if random.random() < 0.15 * opts.get("crit_chance", 1):
            crit = True
            damage *= 2
        
        return damage, effectiveness, crit

    def damage_enemy_and_get_log(move, enemy, damage, effectiveness, crit):
        # DEAL DAMAGE
        if damage > 0:
            damage = int(round(damage))
            enemy.add_hp(-damage, move["type"])

            # LOG ATTACK
            out = f"It dealt {damage}dmg "
            if crit:
                out += "(CRIT) "

            out += f"to {enemy.name}!"

            if enemy.hp <= 0:
                out += " \U0001F480 It knocked them out!"

            if (effectiveness <= 0.4):
                out += " It's barely effective."
            elif (effectiveness < 1):
                out += " It's not too effective."
            elif (effectiveness > 1):
                out += " It's super effective!"
            elif (effectiveness > 2):
                out += " It's hyper effective!"
        else:
            out = ""

        return out

    def missed_move(move, attacker, enemy):
        opts = move["attack_options"]
        missed = not (move["accuracy"] == -1 or opts.get("cannot_miss") == True or random.random() < move["accuracy"])
        
        if move["accuracy"] != -1 and move["accuracy"] != 1 and "white_cyberdrug" in enemy.states:
            if enemy.states["white_cyberdrug"] > 0:
                missed = True

        if "guarantee_hit" in attacker.states:
            missed = False
            del attacker.states["guarantee_hit"]
        
        return missed

    def generic_move_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return "It missed!"

        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        out = MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)
        
        # CUSTOM ATTACK EFFECTS
        opts = move["attack_options"]
        
        if opts.get("recoil") is not None:
            recoil_damage = int(round(damage * opts.get("recoil")))
            attacker.add_hp(-recoil_damage)

            out += f" {attacker.name} took {recoil_damage}dmg in recoil!"

        if opts.get("leech") is not None:
            leech_heal = int(round(damage * opts.get("leech")))
            attacker.add_hp(leech_heal)

            out += f" {attacker.name} healed {leech_heal} HP!"

        if opts.get("heal") is not None:
            heal = int(round(attacker.stats.max_hp * opts.get("heal")))
            attacker.add_hp(heal)
            
            out += f" {attacker.name} healed {heal} HP!"

        if opts.get("stat_bonus") is not None:
            attacker.add_stat_stages(opts.get("stat_bonus"))

            out += f" {attacker.name} is now stronger!"

        if opts.get("enemy_stat_penalty") is not None:
            stat_penalty = opts.get("enemy_stat_penalty")
            
            for i in range(len(stat_penalty)):
                stat_penalty[i] = -stat_penalty[i]
            
            enemy.add_stat_stages(stat_penalty)

            out += f" {enemy.name} is now weaker!"
        
        status_to_string = {
            CybermonStatus.STUN: "stunned",
            CybermonStatus.PARALYSIS: "paralyzed",
            CybermonStatus.SLEEP: "asleep",
            CybermonStatus.FREEZE: "frozen",
            CybermonStatus.BURN: "burned",
            CybermonStatus.POISON: "poisoned",
            CybermonStatus.GLITCH: "?"
        }

        if opts.get("status") is not None:
            status = opts.get("status")
            if random.random() < status[1]:
                enemy.set_status(status[0])
                out += f" {enemy.name} is now {status_to_string[status[0]]}!"

        if opts.get("status_self") is not None:
            status = opts.get("status_self")
            if random.random() < status[1]:
                attacker.set_status(status[0])
                out += f" {attacker.name} is now {status_to_string[status[0]]}!"

        return out

    def spitseed_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return "It missed!"

        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        has_berry = False
        for item in attacker.team.items:
            if item["type"] == ItemType.BERRY:
                has_berry = True
                damage *= 2
                attacker.team.add_item(item["id"], -1)
                break
        
        out = MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)
        if has_berry:
            out += f" {attacker.name} used a BERRY! It dealt double damage."
        
        return out

    def divebomb_handler(move, attacker, enemy):
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)

        if (MoveData.missed_move(move, attacker, enemy)):
            damage *= 0.5
            attacker.add_hp(-damage)
            return f"It missed! {attacker.name} took {damage} in recoil!"

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def magma_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        attacker.add_hp(ord(attacker.name[-1]), damage_type = None, ignore_max_hp = True)
        enemy.set_status(CybermonStatus.BURN)

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def removethis2_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"

        attacker.hp = 24

        if attacker.level < 24:
            attacker.add_xp(1241)
        
        return "This move failed!"

    def removethis3_handler(move, attacker, enemy):
        # always misses
        return "It missed!"

    def knockout_punch_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        if enemy.status == CybermonStatus.STUN:
            enemy.set_status(CybermonStatus.NONE)
            enemy.set_status(CybermonStatus.SLEEP)
            damage *= 2
            
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)


    def puddles_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        attacker.add_stat_stages([0, 0, 1, 0, 1, 0])

        current_status = attacker.status.value
        current_status ^= 5
        attacker.set_status(CybermonStatus(current_status))
            
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit) + f" {attacker.name} is now stronger!"
        

    def blazing_strike_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        out = MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

        if random.random() < 0.8:
            out += f" {enemy.name} is now burned!"

        attacker.states["move_first_glitched"] = True
        return out


    def at_glitch_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        if len(attacker.team.name) > 1:
            attacker.team.name = attacker.team.name[:-1]
        elif len(attacker.name) > 1:
            attacker.name = attacker.name[:-1]
        else:
            new_id = attacker.id.value - 1
            if (new_id < 0):
                new_id = 63

            attacker.set_id(CybermonId(new_id))
            # TODO - also change ability
            
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def overgrowth_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        attacker.add_hp(damage * 0.33, ignore_max_hp = True)

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit) + f" {attacker.name} healed for {damage * 0.33} HP!"

    def pluck_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        if attacker.stats.spd > enemy.stats.spd:
            damage *= 2

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def debilitate_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        if enemy.status == CybermonStatus.POISON:
            damage *= 2

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def fatal_poison_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        damage = 999999

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def draconic_power_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        if enemy.status != CybermonStatus.NONE:
            damage *= 2

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def meteor_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        enemy.set_status(random.choice([CybermonStatus.PARALYSIS, CybermonStatus.BURN, CybermonStatus.STUN]))

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def super_gyroscope_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        move["power"] = attacker.stats.spd / 2
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def flay_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        enemy.hp = int(round(enemy.hp / 2))
        return f" It halved {enemy.name}'s HP!"

    def mind_blast_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        damage *= 1 + (0.2 * sum(enemy.stat_stages))
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def exorcism_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        
        enemy.set_status(CybermonStatus(random.choice([1,2,3,4,5,6])))

        stat_stages = [0, 0, 0, 0, 0, 0]
        stat_stages[random.choice([0,1,2,3,4,5])] = -3
        enemy.add_stat_stages(stat_stages)
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit) + f" {enemy.name} is now weaker!"
        
    def soul_touch_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"

        damage = 1
        eff = 1
        crit = False

        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def curse_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)

        enemy.set_status(CybermonStatus.POISON)
        enemy.add_stat_stages([-1, -1, -1, -1, -1, -1])
        attacker.add_stat_stages([1, 1, 1, 1, 1, 1])
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def spirit_siphon_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        damage = enemy.stats.max_hp * 0.2
        attacker.add_hp(damage)
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def sp_si_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        move["power"] = enemy.stats.max_hp
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def normal_handler(move, attacker, enemy):
        move = MoveData.get_move(MoveId(attacker.hp % 112))
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def sure_hit_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        attacker.states["guarantee_hit"] = True
        return "Using invalid special move."

    def dark_thoughts_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)

        if (enemy.stat_stages[5] % 2):
            enemy.add_stat_stages([0, 0, 0, 0, 0, -1])
        else:
            enemy.add_stat_stages([0, 0, 0, 0, 0, 1])

        current_status = enemy.status.value
        current_status ^= 1
        enemy.set_status(CybermonStatus(current_status))
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)


    def bubble_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        damage, eff, crit = MoveData.get_base_damage(move, attacker, enemy)

        c = ord(attacker.name[-1]) - 1
        if c <= 0:
            c = 255
        
        attacker.name = attacker.name[:-1] + chr(c)
        
        return MoveData.damage_enemy_and_get_log(move, enemy, damage, eff, crit)

    def nturn_handler(move, attacker, enemy):
        if (MoveData.missed_move(move, attacker, enemy)): return f"It missed!"
        tc = attacker.team.battle.turn_count
        attacker.name = str(tc)

        if tc < 16:
            new_id = attacker.id
        else:
            new_id = CybermonId((attacker.id.value - 1) % 64)
        
        if new_id == CybermonId.PANUMBRA:
            attacker.team.add_item(ItemId(sum(map(ord, enemy.name)) % 64), attacker.level)
            attacker.glitch_states.append(GlitchId.POWERFUL_NTURN)
            
        attacker.set_id(CybermonId(new_id))

        new_id = max(0, (tc - 64) % 64)
        return "Using invalid special move." 

    @staticmethod
    def get_move(move_id):
        ret_move = dict(MoveData.move[move_id])
        ret_move["id"] = move_id
        ret_move["pp"] = ret_move["max_pp"]
        return ret_move
    
    #{"recoil": 0.4, "crit_chance": 999, "leech": 0.5, "heal": 0.1, "status": (CybermonStatus.PARALYSIS, 1), "status_self": (CybermonStatus.GLITCH, 0.8)}

    move = {
        MoveId.STRUGGLE: {
            "name": "Struggle", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 25, "accuracy": 1.0, "max_pp": 255, "description": "Your cybermon is out of moves!",
            "attack_function": generic_move_handler, "attack_options": {"recoil": 0.4}
        },

        MoveId.B_: {
            "name": "..b.", "emoji": "\u2694", "type": CybermonType.BIRD, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 256, "accuracy": 0.12, "max_pp": 91, "description": "The target is struck with a head-on physical attack.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.TACKLE: {
            "name": "Tackle", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 40, "accuracy": 1.0, "max_pp": 25, "description": "The target is struck with a head-on physical attack.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.BASH: {
            "name": "Bash", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 40, "accuracy": 0.95, "max_pp": 25, "description": "The target is attacked using a solid or heavy appendage.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.BITE: {
            "name": "Bite", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 45, "accuracy": 1.0, "max_pp": 25, "description": "A biting attack that is more likely to cause critical hits.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 2}
        },
        
        MoveId.POWER_HIT: {
            "name": "Power Hit", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 60, "accuracy": 1.0, "max_pp": 20, "description": "The user throws their entire weight into a full-body tackle on the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.SLAP: {
            "name": "Slap", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 30, "accuracy": 0.75, "max_pp": 25, "description": "A powerful slap that has a small chance to send the target to sleep.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.SLEEP, 0.1)}
        },
        
        MoveId.SPIT_SEED: {
            "name": "Spit Seed", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 45, "accuracy": 1.0, "max_pp": 25, "description": "The user spits a seed at the target. If the user is holding a Berry, they consume it immediately and cause this attack to do double damage.",
            "attack_function": spitseed_handler, "attack_options": {}
        },
        
        MoveId.SEEDCANNON: {
            "name": "Seedcannon", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 80, "accuracy": 1.0, "max_pp": 20, "description": "The user spits a salvo of seeds at the target. If the user is holding a Berry, they consume it immediately and cause this attack to do double damage.",
            "attack_function": spitseed_handler, "attack_options": {}
        },
        
        MoveId.WATER_JET: {
            "name": "Water Jet", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.SPECIAL,
            "power": 50, "accuracy": 1.0, "max_pp": 25, "description": "The target is hit with a high-speed jet of water.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.SONIC_SPLASH: {
            "name": "Sonic Splash", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.SPECIAL,
            "power": 80, "accuracy": 1.0, "max_pp": 20, "description": "The user fires a supersonic stream of water at the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.SCALD: {
            "name": "Scald", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 50, "accuracy": 0.95, "max_pp": 25, "description": "The target is burned by scalding flames. Small chance to cause a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 0.33)}
        },
        
        MoveId.SCORCH: {
            "name": "Scorch", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 80, "accuracy": 0.95, "max_pp": 20, "description": "The user surrounds the target with white-hot fire. Small chance to cause a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 0.33)}
        },
        
        MoveId.ROCK_THROW: {
            "name": "Rock Throw", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 65, "accuracy": 0.8, "max_pp": 25, "description": "The user throws a rock at the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.LANDSLIDE: {
            "name": "Landslide", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 0.9, "max_pp": 20, "description": "The target is caught in a violent avalanche, which also damages the user.",
            "attack_function": generic_move_handler, "attack_options": {"recoil": 0.25}
        },
        
        MoveId.SHADOW_STRIKE: {
            "name": "Shadow Strike", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.SPECIAL,
            "power": 40, "accuracy": -1, "max_pp": 25, "description": "The user descends into shadows and strikes from the back. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True}
        },
        
        MoveId.DARKCUTTER: {
            "name": "Darkcutter", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.SPECIAL,
            "power": 45, "accuracy": -1, "max_pp": 20, "description": "The user descends into shadows and inflicts a critical stealth attack. This attack cannot miss and always deals a critical hit.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True, "crit_chance": 999}
        },
        
        MoveId.SHOCK: {
            "name": "Shock", "emoji": "\u2694", "type": CybermonType.ELECTRIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 55, "accuracy": 1.0, "max_pp": 25, "description": "The target is struck by a static charge. Small chance to inflict paralysis.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.25)}
        },
        
        MoveId.LIGHTNINGBOLT: {
            "name": "Lightningbolt", "emoji": "\u2694", "type": CybermonType.ELECTRIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 85, "accuracy": 1.0, "max_pp": 20, "description": "The target is struck by a lightning bolt. Small chance to inflict paralysis.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.33)}
        },
        
        MoveId.SWOOP: {
            "name": "Swoop", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 70, "accuracy": 0.9, "max_pp": 25, "description": "The user flies high in the sky and swoops down on the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.WING_BEAT: {
            "name": "Wing Beat", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.SPECIAL,
            "power": 95, "accuracy": 1.0, "max_pp": 20, "description": "Using its large wings, the user drives the air around it into a hurricane, dealing damage.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.DIVEBOMB: {
            "name": "Divebomb", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 140, "accuracy": 0.75, "max_pp": 15, "description": "The user flies to a great height and drops directly on the target, taking heavy recoil damage even if the attack misses.",
            "attack_function": divebomb_handler, "attack_options": {}
        },
        
        MoveId.UPPERCUT: {
            "name": "Uppercut", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 60, "accuracy": 0.95, "max_pp": 25, "description": "The user strikes with a powerful upward strike.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.DOUBLE_PUNCH: {
            "name": "Double Punch", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 80, "accuracy": 0.95, "max_pp": 20, "description": "The user strikes twice with fists.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.EARTHSHOCK: {
            "name": "Earthshock", "emoji": "\u2694", "type": CybermonType.GROUND, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 40, "accuracy": 0.8, "max_pp": 25, "description": "The user pounds the ground and causes tremors. Can stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 0.4)}
        },
        
        MoveId.MAGMA: {
            "name": "Magma", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 50, "accuracy": 0.8, "max_pp": 15, "description": "The target is submerged in magma. Always causes a burn. R",
            "attack_function": magma_handler, "attack_options": {}
        },
        
        MoveId.VOLCANIC_CUTTER: {
            "name": "Volcanic Cutter", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 1.0, "max_pp": 15, "description": "The target is sliced with a blade wreathed in flame. May cause a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 0.5)}
        },
        
        MoveId.REMOVE_THIS1: {
            "name": "REMOVE_THIS", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 2, "accuracy": 1.0, "max_pp": 2, "description": "?????????????a??????????????????????????????",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.POISON, 0.75)}
        },
        
        MoveId.REMOVE_THIS2: {
            "name": "REMOVE_THIS", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 0, "accuracy": 1.0, "max_pp": 127, "description": ".x",
            "attack_function": removethis2_handler, "attack_options": {}
        },
        
        MoveId.REMOVE_THIS3: {
            "name": "REMOVE_THIS", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 1024, "accuracy": 0.0, "max_pp": 1, "description": "asdasdasdasdasdasdasdasdasdasdasdasdasdasdasd",
            "attack_function": removethis3_handler, "attack_options": {}
        },
        
        MoveId.KNOCKOUT_PUNCH: {
            "name": "Knockout Punch", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 50, "accuracy": 1.0, "max_pp": 15, "description": "A punch that deals double damage to a stunned target, also putting them to sleep.",
            "attack_function": knockout_punch_handler, "attack_options": {}
        },
        
        MoveId.IGNITE: {
            "name": "Ignite", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": 1.0, "max_pp": 15, "description": "The user uses white-hot flames to inflict a burn on the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 1)}
        },
        
        MoveId.EARTHQUAKE: {
            "name": "Earthquake", "emoji": "\u2694", "type": CybermonType.GROUND, "dmg_type": MoveDamageType.SPECIAL,
            "power": 80, "accuracy": 0.9, "max_pp": 20, "description": "The user causes a miniature earthquake in the surrounding area. May stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 0.4)}
        },
        
        MoveId.VITAL_PULSE: {
            "name": "Vital Pulse", "emoji": "\u2694", "type": CybermonType.ELECTRIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 0, "accuracy": 1.0, "max_pp": 10, "description": "The user uses electrical impulses to soothe its wounds, healing a third of their maximum health.",
            "attack_function": generic_move_handler, "attack_options": {"heal": 0.33}
        },
        
        MoveId.PHOTOSYNTHESIS: {
            "name": "Photosynthesis", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 10, "description": "The user bathes in the sun to regain energy, healing themselves somewhat and gaining a bonus to all stats.",
            "attack_function": generic_move_handler, "attack_options": {"heal": 0.15, "stat_bonus": [1,1,1,1,1,1]}
        },
        
        MoveId.FEATHER_WHIP: {
            "name": "Feather Whip", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 45, "accuracy": 1.0, "max_pp": 25, "description": "The user strikes the target with cast-off feathers.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.PUDDLES: {
            "name": "Puddles", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 20, "description": "The user surrounds itself with deep water, healing all status effects while increasing its DEF and SpDEF. Heals a burn.",
            "attack_function": puddles_handler, "attack_options": {}
        },
        
        MoveId.IMBIBE: {
            "name": "Imbibe", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 20, "description": "The user drinks toxic sludge, poisoning themselves.",
            "attack_function": generic_move_handler, "attack_options": {"status_self": (CybermonStatus.POISON, 1)}
        },
        
        MoveId.TOXIC_GAS: {
            "name": "Toxic Gas", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 15, "description": "The user releases toxic fumes, inflicting poison on all combatants - including themselves.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.POISON, 1), "status_self": (CybermonStatus.POISON, 1)}
        },
        
        MoveId.POISON_SPIT: {
            "name": "Poison Spit", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 25, "accuracy": 1.0, "max_pp": 15, "description": "The user spits poison at the target, which is sure to poison them.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.POISON, 1)}
        },
        
        MoveId.EROSION: {
            "name": "Erosion", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 85, "accuracy": 1.0, "max_pp": 20, "description": "The user wears away at the target, dealing heavy damage.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.TECTONICS: {
            "name": "Tectonics", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.SPECIAL,
            "power": 100, "accuracy": 0.8, "max_pp": 15, "description": "Causing heavy tectonic movement, the user causes an earthquake to deal damage.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.BLAZING_STRIKE: {
            "name": "Blazing Strike", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 40, "accuracy": 0.9, "max_pp": 15, "description": "The user uses blazing speed to deal damage, with a high chance of inflicting a burn. This move always goes first.",
            "attack_function": blazing_strike_handler, "attack_options": {}
        },
        
        MoveId.SHOCK_PALM: {
            "name": "Shock Palm", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 40, "accuracy": 0.9, "max_pp": 10, "description": "The user uses lightning movements to attack, with a chance of causing paralysis.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.5)}
        },
        
        MoveId.ABSORB: {
            "name": "Absorb", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.SPECIAL,
            "power": 30, "accuracy": 1.0, "max_pp": 15, "description": "The user absorbs vital energies from the target, healing based on damage dealt.",
            "attack_function": generic_move_handler, "attack_options": {"leech": 0.33}
        },
        
        MoveId.LIFEDRAIN: {
            "name": "Lifedrain", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.SPECIAL,
            "power": 60, "accuracy": 1.0, "max_pp": 15, "description": "The user absorbs all energy it can reach from the target, healing based on damage dealt.",
            "attack_function": generic_move_handler, "attack_options": {"leech": 0.33}
        },
        
        MoveId.CARESS: {
            "name": "Caress", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": 0.8, "max_pp": 10, "description": "A loving caress that is highly likely to send the target to sleep.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.SLEEP, 0.8)}
        },
        
        MoveId.JUMPING_KICK: {
            "name": "Jumping Kick", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 75, "accuracy": 0.75, "max_pp": 20, "description": "An inaccurate attack that also causes recoil damage.",
            "attack_function": generic_move_handler, "attack_options": {"recoil": 0.25}
        },
        
        MoveId.CURL_UP: {
            "name": "Curl Up", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 30, "description": "The user curls up into a defensive ball.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,0,1,0,0,0]}
        },
        
        MoveId.PSYCH_UP: {
            "name": "Psych Up", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 25, "description": "The user psyches itself up, gaining bonus attack power.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,1,0,1,0,0]}
        },
        
        MoveId._: {
            "name": "@", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 96, "description": "-",
            "attack_function": at_glitch_handler, "attack_options": {}
        },
        
        MoveId.POWERBLAST: {
            "name": "Powerblast", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.SPECIAL,
            "power": 120, "accuracy": 1.0, "max_pp": 10, "description": "The user channels vital energies into a spiritual blast. The shock leaves them stunned.",
            "attack_function": generic_move_handler, "attack_options": {"status_self": (CybermonStatus.STUN, 1)}
        },
        
        MoveId.ALL_OUT_ATTACK: {
            "name": "All-Out Attack", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 160, "accuracy": 1.0, "max_pp": 5, "description": "The user gives their all for a final attack, taking exactly the damage dealt as recoil damage.",
            "attack_function": generic_move_handler, "attack_options": {"recoil": 1}
        },
        
        MoveId.INFESTATION: {
            "name": "Infestation", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.SPECIAL,
            "power": 105, "accuracy": 0.95, "max_pp": 10, "description": "The user injects spores into the target, which deal heavy damage.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.OVERGROWTH: {
            "name": "Overgrowth", "emoji": "\u2694", "type": CybermonType.GRASS, "dmg_type": MoveDamageType.SPECIAL,
            "power": 125, "accuracy": 0.95, "max_pp": 5, "description": "The target is surrounded by vines and carnivorous plants which deal intense damage and heal the user for a third of the damage dealt.",
            "attack_function": overgrowth_handler, "attack_options": {}
        },
        
        MoveId.DELUGE: {
            "name": "Deluge", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.SPECIAL,
            "power": 105, "accuracy": 1.0, "max_pp": 10, "description": "The user releases a deluge of water at the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.TIDAL_WAVE: {
            "name": "Tidal Wave", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.SPECIAL,
            "power": 140, "accuracy": 1.0, "max_pp": 5, "description": "The user causes a tidal wave to wreak the battlefield.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.CAUTERIZE: {
            "name": "Cauterize", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 110, "accuracy": 0.95, "max_pp": 10, "description": "The user uses incredibly strong flames to burn the target. May cause a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 0.5)}
        },
        
        MoveId.IMMOLATE: {
            "name": "Immolate", "emoji": "\u2694", "type": CybermonType.FIRE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 130, "accuracy": 0.9, "max_pp": 5, "description": "The user strikes the target with lethal fire. Always causes a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 1)}
        },
        
        MoveId.BOULDERFALL: {
            "name": "Boulderfall", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 140, "accuracy": 0.8, "max_pp": 10, "description": "Huge boulders fall and crush all in their path.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.MOUNTAIN_BREAKER: {
            "name": "Mountain Breaker", "emoji": "\u2694", "type": CybermonType.ROCK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 200, "accuracy": 0.7, "max_pp": 5, "description": "The user breaks an entire mountain to pieces, letting its debris fall on the battlefield.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.BACKSTAB: {
            "name": "Backstab", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 0.8, "max_pp": 15, "description": "The user stabs the target in a critical extremity, which always deals critical damage.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 999}
        },
        
        MoveId.SABOTAGE: {
            "name": "Sabotage", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.SPECIAL,
            "power": 30, "accuracy": -1, "max_pp": 10, "description": "The user heavily sabotages the target, reducing all their stats and applying poison. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.POISON, 1), "enemy_stat_penalty": [1,1,1,1,1,1]}
        },
        
        MoveId.THUNDER: {
            "name": "Thunder", "emoji": "\u2694", "type": CybermonType.ELECTRIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 105, "accuracy": 1.0, "max_pp": 10, "description": "The user summons a thunderbolt to strike the target. Can cause paralysis.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.4)}
        },
        
        MoveId.LETHAL_CURRENT: {
            "name": "Lethal Current", "emoji": "\u2694", "type": CybermonType.ELECTRIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 135, "accuracy": 1.0, "max_pp": 5, "description": "The user discharges millions of volts inside the target, also causing paralysis very commonly.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.8)}
        },
        
        MoveId.LIMBER: {
            "name": "Limber", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 30, "description": "The user stretches their limbs, increasing their SPD, ATK and SpATK.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,1,0,1,0,1]}
        },
        
        MoveId.PLUCK: {
            "name": "Pluck", "emoji": "\u2694", "type": CybermonType.FLYING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 50, "accuracy": 1.0, "max_pp": 20, "description": "The user swiftly attacks the target, dealing double damage if their SPD is higher than the target's.",
            "attack_function": pluck_handler, "attack_options": {}
        },
        
        MoveId.LETHAL_FIST: {
            "name": "Lethal Fist", "emoji": "\u2694", "type": CybermonType.FIGHTING, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 130, "accuracy": 0.75, "max_pp": 10, "description": "The user executes a final attack, which is twice as likely to cause a critical hit.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 2}
        },
        
        MoveId.FISSURE: {
            "name": "Fissure", "emoji": "\u2694", "type": CybermonType.GROUND, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 120, "accuracy": 0.65, "max_pp": 10, "description": "The user opens a fissure in the ground below the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.ERUPTION: {
            "name": "Eruption", "emoji": "\u2694", "type": CybermonType.GROUND, "dmg_type": MoveDamageType.SPECIAL,
            "power": 200, "accuracy": 0.35, "max_pp": 5, "description": "The user summons a volcanic eruption that is guaranteed to stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 1)}
        },
        
        MoveId.DEBILITATE: {
            "name": "Debilitate", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 1.0, "max_pp": 10, "description": "The user deals heavy damage using a cocktail of poisons. Deals double damage if the target is poisoned.",
            "attack_function": debilitate_handler, "attack_options": {}
        },
        
        MoveId.FATAL_POISON: {
            "name": "Fatal Poison", "emoji": "\u2694", "type": CybermonType.POISON, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": 0.15, "max_pp": 5, "description": "The user injects the target with a poison that is guaranteed to make them faint immediately.",
            "attack_function": fatal_poison_handler, "attack_options": {}
        },
        
        MoveId.SNOWBALL: {
            "name": "Snowball", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 35, "accuracy": 1.0, "max_pp": 25, "description": "The user throws a snowball at the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.FROSTBITE: {
            "name": "Frostbite", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 55, "accuracy": 0.95, "max_pp": 15, "description": "The user drastically drops the temperature of the environment and inflicts damage upon the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.FREEZE, 0.25)}
        },
        
        MoveId.GLACIER_SLAM: {
            "name": "Glacier Slam", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 100, "accuracy": 0.75, "max_pp": 10, "description": "The user crushes the target with a giant glacier, which is likely to cause freezing.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.FREEZE, 0.5)}
        },
        
        MoveId.ARCTIC_FREEZE: {
            "name": "Arctic Freeze", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 150, "accuracy": 0.6, "max_pp": 5, "description": "The user creates an intensely chilling wind with a very high chance to freeze the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.FREEZE, 0.9)}
        },
        
        MoveId.BLIZZARD: {
            "name": "Blizzard", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.SPECIAL,
            "power": 40, "accuracy": 0.7, "max_pp": 10, "description": "The target is buffeted with arctic winds, freezing them.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.FREEZE, 1)}
        },
        
        MoveId.BURNING_COLD: {
            "name": "Burning Cold", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": 1.0, "max_pp": 10, "description": "Freezing cold weakens the target, drastically reducing speed and causing a burn.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.BURN, 1), "enemy_stat_penalty": [0,0,0,0,0,2]}
        },
        
        MoveId.ICICLE_CRASH: {
            "name": "Icicle Crash", "emoji": "\u2694", "type": CybermonType.ICE, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 100, "accuracy": 0.9, "max_pp": 15, "description": "The target is torn apart with sharp blades of ice, possibly causing them to be stunned.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 0.4)}
        },
        
        MoveId.CLAW_SLASH: {
            "name": "Claw Slash", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 45, "accuracy": 1.0, "max_pp": 25, "description": "The user slashes the target with sharp claws.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.TAIL_THRUST: {
            "name": "Tail Thrust", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 60, "accuracy": 1.0, "max_pp": 15, "description": "The user thrusts their tail straight towards the target. Can cause paralysis.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.PARALYSIS, 0.2)}
        },
        
        MoveId.ARCANIC_BLAST: {
            "name": "Arcanic Blast", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 100, "accuracy": 0.85, "max_pp": 10, "description": "The user whirls a sphere of magic power towards the target. This move is more likely to cause critical hits.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 1.5}
        },
        
        MoveId.DRACONIC_POWER: {
            "name": "Draconic Power", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 140, "accuracy": 0.8, "max_pp": 5, "description": "The user channels their inner draconic power, dealing double damage if the target is affected by any status.",
            "attack_function": draconic_power_handler, "attack_options": {}
        },
        
        MoveId.METEOR: {
            "name": "Meteor", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.SPECIAL,
            "power": 80, "accuracy": 1.0, "max_pp": 15, "description": "A meteor is launched from the sky, dealing damage and applying either paralysis, a burn or a stun.",
            "attack_function": meteor_handler, "attack_options": {}
        },
        
        MoveId.ANGER: {
            "name": "Anger", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 20, "description": "The user allows themselves to succumb to fury, stunning themselves but heavily increasing ATK and SpATK.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,1,0,1,0,0], "status_self": (CybermonStatus.STUN, 1)}
        },
        
        MoveId.COALESCE_AURA: {
            "name": "Coalesce Aura", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 15, "description": "The user coalesces their draconic aura, sharply increasing SPD and ATK.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,2,0,0,0,2]}
        },
        
        MoveId.CLAW_RIPPER: {
            "name": "Claw Ripper", "emoji": "\u2694", "type": CybermonType.DRAGON, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 45, "accuracy": 0.9, "max_pp": 15, "description": "The user tears the target to pieces with sharp claws, which is guaranteed to cause a critical hit.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 999}
        },
        
        MoveId.IRON_FIST: {
            "name": "Iron Fist", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 55, "accuracy": 1.0, "max_pp": 25, "description": "The user thrusts a mechanized iron punch towards the target.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.PRECISION_SHOT: {
            "name": "Precision Shot", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 1.0, "max_pp": 15, "description": "The user focuses a precise powerful shot onto the target. The fatigue from this move lowers the user's SPD stat.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,0,0,0,0,-1]}
        },
        
        MoveId.ALLOY_STRIKE: {
            "name": "Alloy Strike", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 115, "accuracy": 0.9, "max_pp": 10, "description": "The user executes a powerful strike using metallic arms.",
            "attack_function": generic_move_handler, "attack_options": {}
        },
        
        MoveId.TITANIUM_INDUCTION: {
            "name": "Titanium Induction", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 170, "accuracy": 0.8, "max_pp": 5, "description": "The user channels immense power and strikes the target with a powerful shot, lowering the speed of the user greatly.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,0,0,0,0,-2]}
        },
        
        MoveId.SUPER_GYROSCOPE: {
            "name": "Super-Gyroscope", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.PHYSICAL,
            "power": -1, "accuracy": 1.0, "max_pp": 15, "description": "The user spins at high velocity, dealing damage based on their SPD.",
            "attack_function": super_gyroscope_handler, "attack_options": {}
        },
        
        MoveId.POLISH: {
            "name": "Polish", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 15, "description": "The user polishes their body, drastically increasing their SPD.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,0,0,0,0,2]}
        },
        
        MoveId.SHED_CASING: {
            "name": "Shed Casing", "emoji": "\u2694", "type": CybermonType.STEEL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 10, "description": "The user sheds a large amount of their exterior, harshly reducing DEF and SpDEF but raising SPD to the maximum.",
            "attack_function": generic_move_handler, "attack_options": {"stat_bonus": [0,0,-3,0,-3,6]}
        },
        
        MoveId.RIDDLE: {
            "name": "Riddle", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 40, "accuracy": 1.0, "max_pp": 25, "description": "The user proposes a difficult riddle, dealing damage and having a small chance to stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 0.15)}
        },
        
        MoveId.MIND_TRICKS: {
            "name": "Mind Tricks", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 70, "accuracy": 1.0, "max_pp": 15, "description": "The user plays various mind tricks. Has a slight chance to stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.STUN, 0.3)}
        },
        
        MoveId.TELEPATHIC_BLAST: {
            "name": "Telepathic Blast", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 100, "accuracy": 1.0, "max_pp": 10, "description": "The user connects to the target's mind, deals damage and lowers the target's SPD.",
            "attack_function": generic_move_handler, "attack_options": {"enemy_stat_penalty": [0,0,0,0,0,1]}
        },
        
        MoveId.PARADOX_CHANNELING: {
            "name": "Paradox Channeling", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 120, "accuracy": 1.0, "max_pp": 5, "description": "The user channels a dfficult paradox right into the target's mind, which deals damage to the target and lowers their SPD, ATK and SpATK stats. This also has a chance to stun the target.",
            "attack_function": generic_move_handler, "attack_options": {"enemy_stat_penalty": [0,1,0,1,0,1], "status": (CybermonStatus.STUN, 0.5)}
        },
        
        MoveId.FLAY: {
            "name": "Flay", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": -1, "accuracy": 0.8, "max_pp": 10, "description": "The user rends their target's mind in two, halving their current health.",
            "attack_function": flay_handler, "attack_options": {}
        },
        
        MoveId.MIND_BLAST: {
            "name": "Mind Blast", "emoji": "\u2694", "type": CybermonType.PSYCHIC, "dmg_type": MoveDamageType.SPECIAL,
            "power": 50, "accuracy": 1.0, "max_pp": 10, "description": "The user exploits a target's weakness, dealing 20% more damage per level of stat reduction effective on the target.",
            "attack_function": mind_blast_handler, "attack_options": {}
        },
        
        MoveId.NIP: {
            "name": "Nip", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 25, "accuracy": -1, "max_pp": 30, "description": "The user bites an unaware target, also reducing their ATK and SpATK. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True, "enemy_stat_penalty": [0,1,0,1,0,0]}
        },
        
        MoveId.INFEST: {
            "name": "Infest", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 50, "accuracy": 1.0, "max_pp": 25, "description": "The user uses pheromones to summon many other aggressive insects, lowering the target's ATK, SpATK and DEF.",
            "attack_function": generic_move_handler, "attack_options": {"enemy_stat_penalty": [0,1,1,1,0,0]}
        },
        
        MoveId.SWARM: {
            "name": "Swarm", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 70, "accuracy": 1.0, "max_pp": 20, "description": "The user calls a swarm, lowering the target's DEF and SpDEF.",
            "attack_function": generic_move_handler, "attack_options": {"enemy_stat_penalty": [0,0,1,0,1,0]}
        },
        
        MoveId.NEEDLER: {
            "name": "Needler", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 110, "accuracy": -1, "max_pp": 15, "description": "The user pierces the target with needle-like limbs, lowering all of the target's stats. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True, "enemy_stat_penalty": [1,1,1,1,1,1]}
        },
        
        MoveId.ARMOUR_CRUSH: {
            "name": "Armour Crush", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 35, "accuracy": 1.0, "max_pp": 20, "description": "The user destroys the target's armour, harshly reducing their DEF.",
            "attack_function": generic_move_handler, "attack_options": {"enemy_stat_penalty": [0,0,3,0,0,0]}
        },
        
        MoveId.PINCER_CUTTER: {
            "name": "Pincer Cutter", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 90, "accuracy": 0.9, "max_pp": 20, "description": "The user uses two pincers to cut the target. This attack is more likely to deal critical hits.",
            "attack_function": generic_move_handler, "attack_options": {"crit_chance": 1.5}
        },
        
        MoveId.PUPATE: {
            "name": "Pupate", "emoji": "\u2694", "type": CybermonType.BUG, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 15, "description": "The user enters a cocoon and undergoes metamorphosis, healing 15% max HP and gaining a bonus to all stats.",
            "attack_function": generic_move_handler, "attack_options": {"heal": 0.15, "stat_bonus": [1,1,1,1,1,1]}
        },
        
        MoveId.TRICK: {
            "name": "Trick", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.SPECIAL,
            "power": 30, "accuracy": -1, "max_pp": 25, "description": "The user plays a nasty trick on the target. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True}
        },
        
        MoveId.HAUNT: {
            "name": "Haunt", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.SPECIAL,
            "power": 45, "accuracy": -1, "max_pp": 15, "description": "The user uses ghostly abilities to bewilder the target, paralyzing them. This attack cannot miss.",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True, "status": (CybermonStatus.PARALYSIS, 1)}
        },
        
        MoveId.POSESS: {
            "name": "Posess", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.SPECIAL,
            "power": 70, "accuracy": 0.75, "max_pp": 10, "description": "The target is overwhelmed with the user's presence, damaging them and sending them to sleep.",
            "attack_function": generic_move_handler, "attack_options": {"status": (CybermonStatus.SLEEP, 1)}
        },
        
        MoveId.EXORCISM: {
            "name": "Exorcism", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.SPECIAL,
            "power": 110, "accuracy": -1, "max_pp": 5, "description": "Ghosts of previous fallen are resurrected to swarm the target This attack is guaranteed to inflict a random status effect and lower a random selection of three of the target's stats. Cannot miss.",
            "attack_function": exorcism_handler, "attack_options": {}
        },
        
        MoveId.SOUL_TOUCH: {
            "name": "Soul Touch", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.SPECIAL,
            "power": -1, "accuracy": -1, "max_pp": 40, "description": "The user reaches into the target, dealing exactly 1 damage. This attack cannot miss.",
            "attack_function": soul_touch_handler, "attack_options": {}
        },
        
        MoveId.CURSE: {
            "name": "Curse", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": 0.9, "max_pp": 15, "description": "The target is afflicted with a terrible curse, poisoning them and reducing all their stats. The user gains a bonus to all stats.",
            "attack_function": curse_handler, "attack_options": {}
        },
        
        MoveId.SPIRIT_SIPHON: {
            "name": "Spirit Siphon", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 10, "description": "Using spiritual abilities, the user steals a part of the target's soul. This attack deals exactly one fifth of the enemy's max HP, cannot miss and heals the user for 100% of the damage dealt.",
            "attack_function": spirit_siphon_handler, "attack_options": {}
        },
        
        MoveId.SP_____SI: {
            "name": "Sp▲ïÖ« Si", "emoji": "\u2694", "type": CybermonType.GHOST, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 10, "description": "damage dealt. damage dealt. vamaje4dealr. damage dealt. aamage derlt. ´amagu de{lt.iÛamage depbt.xramag{ dealt. damage+}eaa¿^ _£mage g×azq.¹daX[a¿ xealÏ.\x06deTpee,]^¹¢tC:ºsmfªg ´ea¢¯: d{mswµ+leaet.utam\\i]\x12i£a^µ6 djw`de\x0bV]Ì¡t*\x19damugÕ\x0fLnH¥¬< ]x",
            "attack_function": sp_si_handler, "attack_options": {}
        },
        
        MoveId.NORMAL: {
            "name": "Normal", "emoji": "\u2694", "type": CybermonType.POWERBLAST, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 0, "accuracy": 0.76, "max_pp": 76, "description": "t",
            "attack_function": normal_handler, "attack_options": {}
        },
        
        MoveId.SURE_HIT: {
            "name": "Sure Hit", "emoji": "\u2694", "type": CybermonType.NORMAL, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 999, "description": "move -> gauranteed hit next hit ?",
            "attack_function": sure_hit_handler, "attack_options": {}
        },
        
        MoveId.SUPER_REVERSAL: {
            "name": "Super-Reversal", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.PHYSICAL,
            "power": 320, "accuracy": -1, "max_pp": 2, "description": "This is way too strong...",
            "attack_function": generic_move_handler, "attack_options": {"cannot_miss": True}
        },
        
        MoveId.DARK_THOUGHTS: {
            "name": "Dark Thoughts", "emoji": "\u2694", "type": CybermonType.DARK, "dmg_type": MoveDamageType.SPECIAL,
            "power": 33, "accuracy": 0.8, "max_pp": 25, "description": "The user projects terrible thoughts towards the target, making them unable to do anything but wallow in despair. They are stunned and lose SPD",
            "attack_function": dark_thoughts_handler, "attack_options": {}
        },
        
        MoveId.BUBBLE: {
            "name": "Bubble", "emoji": "\u2694", "type": CybermonType.WATER, "dmg_type": MoveDamageType.SPECIAL,
            "power": 0, "accuracy": 1.0, "max_pp": 40, "description": "The user throws a single bubble",
            "attack_function": bubble_handler, "attack_options": {}
        },
        
        MoveId.NTURN_: {
            "name": "NTURN16?", "emoji": "\u2694", "type": CybermonType.BIRD, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 256, "description": "turn_count ???? ??? ???? ??????",
            "attack_function": nturn_handler, "attack_options": {}
        },

        MoveId.DELETER_MOVE: {
            "name": "-", "emoji": "\u2694", "type": CybermonType.BIRD, "dmg_type": MoveDamageType.NONE,
            "power": -1, "accuracy": -1, "max_pp": 256, "description": "C",
            "attack_function": generic_move_handler, "attack_options": {}
        }
    }

    effectiveness_multiplier = {
        MoveEffectiveness.ZERO: 0.25, # TODO - 0 is OP???
        MoveEffectiveness.NORMAL: 1,
        MoveEffectiveness.NONEFFECTIVE: 0.5,
        MoveEffectiveness.SUPEREFFECTIVE: 2
    }

    matchups = {
		CybermonType.NORMAL: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.ZERO,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.FIGHTING: {
			CybermonType.NORMAL: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.ZERO,
			CybermonType.STEEL: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ICE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.FLYING: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.POISON: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ROCK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.STEEL: MoveEffectiveness.ZERO,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.GROUND: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.ZERO,
			CybermonType.POISON: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.ROCK: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FLYING: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.BUG: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FLYING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.GHOST: {
			CybermonType.NORMAL: MoveEffectiveness.ZERO,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.STEEL: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.FIRE: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DRAGON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.WATER: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ROCK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.GRASS: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ROCK: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.BUG: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.ELECTRIC: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.ZERO,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.PSYCHIC: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.ZERO,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.ICE: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.WATER: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.GRASS: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.DRAGON: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.DRAGON: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.DARK: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NONEFFECTIVE,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		},

		CybermonType.BIRD: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.SUPEREFFECTIVE,
			CybermonType.POWERBLAST: MoveEffectiveness.ZERO
		},

		CybermonType.POWERBLAST: {
			CybermonType.NORMAL: MoveEffectiveness.NORMAL,
			CybermonType.FIGHTING: MoveEffectiveness.NORMAL,
			CybermonType.FLYING: MoveEffectiveness.NORMAL,
			CybermonType.POISON: MoveEffectiveness.NORMAL,
			CybermonType.GROUND: MoveEffectiveness.NORMAL,
			CybermonType.ROCK: MoveEffectiveness.NORMAL,
			CybermonType.BUG: MoveEffectiveness.NORMAL,
			CybermonType.GHOST: MoveEffectiveness.NORMAL,
			CybermonType.STEEL: MoveEffectiveness.NORMAL,
			CybermonType.FIRE: MoveEffectiveness.NORMAL,
			CybermonType.WATER: MoveEffectiveness.NORMAL,
			CybermonType.GRASS: MoveEffectiveness.NORMAL,
			CybermonType.ELECTRIC: MoveEffectiveness.NORMAL,
			CybermonType.PSYCHIC: MoveEffectiveness.NORMAL,
			CybermonType.ICE: MoveEffectiveness.NORMAL,
			CybermonType.DRAGON: MoveEffectiveness.NORMAL,
			CybermonType.DARK: MoveEffectiveness.NORMAL,
			CybermonType.BIRD: MoveEffectiveness.ZERO,
			CybermonType.POWERBLAST: MoveEffectiveness.SUPEREFFECTIVE
		}
	}