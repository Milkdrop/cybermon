from .enums import *
import random
from .move import MoveData
from .map import MapData

class ItemData:
    @staticmethod
    def get_item(item_id):
        item = dict(ItemData.item[item_id])
        item["id"] = item_id
        
        return item
        
    def cyberbox_handler(item, user, enemy):
        status_eff = 1

        if enemy.hp < enemy.stats.max_hp * 0.33:
            status_eff *= 2
        
        if enemy.status in [CybermonStatus.SLEEP, CybermonStatus.FREEZE, CybermonStatus.STUN, CybermonStatus.PARALYSIS]:
            status_eff *= 3
        
        chance = 5 * (status_eff ** 0.67) / (10 + 2 * enemy.level / (status_eff ** 0.67))
        chance += (100 - enemy.level) / 200

        if "chance_custom" in item and enemy.id.value % 2:
            chance *= item["chance_custom"]
        else:
            chance *= item["chance_weight"]

        if random.random() < chance:
            out = f"You caught wild {enemy.name}!"

            enemy_team = enemy.team
            enemy_team.cybermon.remove(enemy)

            enemy.team = user.team

            if len(user.team.cybermon) >= 6:
                user.team.pc_cybermon.append(enemy)
                out += " It was stored in your PC."
            else:
                user.team.cybermon.append(enemy)

            if (GlitchId.DOLLY in enemy.glitch_states):
                user.glitch_states.append(GlitchId.DOLLY)

            return out
        else:
            return f"Your cyberbox failed!"

    def null_berry_handler(item, user, enemy):
        user.set_status(CybermonStatus(random.choice([0,1,2,3,4,5,6,7])))
        return "huh?"
        
    def generic_item_handler(item, user, enemy):
        opts = item["use_options"]

        out = ""

        if "remove" in opts:
            to_remove = []
            if type(opts["remove"]) == list:
                to_remove = opts["remove"]
            else:
                to_remove = [opts["remove"]]
            
            for effect in to_remove:
                if effect == CybermonStatus.NONE or user.status == effect:
                    user.set_status(CybermonStatus.NONE)
                    out += f" It removed {user.name}'s {effect.name}!"

        if "apply" in opts:
            if user.status == CybermonStatus.NONE:
                out += f" It applied {opts['apply'].name} to {user.name}!"

            user.set_status(opts["apply"])

        heal = 0

        if "heal" in opts:
            heal = user.stats.max_hp * opts["heal"]
        
        if "heal_fixed" in opts:
            heal = opts["heal_fixed"]

        if heal != 0:
            user.add_hp(heal)
            out += f" It healed {heal} HP for {user.name}!"

        if "raise" in opts:
            user.add_stat_stages(opts["raise"])
            out += f" {user.name} became stronger!"

        return out

    def piu_berry_handler(item, user, enemy):
        enemy.set_status(CybermonStatus.STUN)
        
        if enemy.status == CybermonStatus.STUN:
            out = f"{enemy.name} is now stunned!"
        else:
            out = "It didn't do anything."
        
        return out

    def blanc_berry_handler(item, user, enemy):
        user.set_status(CybermonStatus.NONE)

        stat_stages = [-stage for stage in user.stat_stages]
        user.add_stat_stages(stat_stages)

        return f"{user.name}'s effects got cleared!"

    def med_berry_handler(item, user, enemy):
        heal = 0.4 * user.stats.max_hp

        if CybermonType.GRASS in user.types:
            heal = 0.6 * user.stats.max_hp
            
        return f"It healed {user.name} {heal} HP!"

    def pulpa_berry_handler(item, user, enemy):
        enemy.add_hp(-999999)

        return f"It knocked out {enemy.name}!"

    def white_cyberdrug_handler(item, user, enemy):
        user.states["white_cyberdrug"] = 4

        return f"{user.name} is now SUPER-CHARGED!"

    def green_powderbag_handler(item, user, enemy):
        if user.level < 255:
            user.add_xp(user.xp_to_level_up - user.xp)

            return f"{user.name} leveled up!"
        else:
            return "Fail!"

    def generic_teach_move(item, user, enemy):
        move = MoveData.get_move(item["teach_move"])
        user.pending_moves.append(move)

        return f"{user.name} can now learn {move['name']}! Use `/teach-move`"

    def queque_handler(item, user, enemy):
        user.add_hp(76)
        item["quantity"] += 2
        
        return "heal!"
    
    def back_handler(item, user, enemy):
        if user.team.is_in_state(TeamState.BATTLE):
            if user.team.battle.get_opponent_team(user.team.id).id is None:
                user.team.remove_state(TeamState.BATTLE)
                user.team.battle = None

                for c in user.team.cybermon:
                    c.stat_stages = [0, 0, 0, 0, 0, 0]
                    c.states = {}
            else:
                return "This move failed!"

        else:
            spawn_location = random.choice(MapData.get_all_objects_of_type(MapObject.SPAWN_LOCATION))
            user.team.coords.x = spawn_location["x"]
            user.team.coords.y = spawn_location["y"]
            
            if user.team.is_in_state(TeamState.MOVING):
                user.team.remove_state(TeamState.MOVING)
                user.team.movement_path = []

        return "random.choice(MapData.get_all_objects_of_type(MapObject.SPAWN_LOCATION))\
            user.team.coords.x = spawn_location[\"x\"]\
            user.team.coords.y = s"
    
    def menu_handler(item, user, enemy):
        team = user.team
        team.add_state(TeamState.GLITCH_CAN_MOVE_FOREVER)
        user.glitch_states.append(GlitchId.MENU)
            
        return "move."

    def coin_handler(item, user, enemy):
        user.states["glitched_sprite_path"] = "glitch/coin.png"
        return "."

    def qz_handler(item, user, enemy):
        for c in user.team.cybermon:
            c.add_hp(999999)

        item["quantity"] += 1
        return "[MACHINERY NOISES]\nYour cybermon are now fully healed!"

    def t_handler(item, user, enemy):
        if user.level < 253:
            user.set_level(user.level + 2)

        return f"Debug: {user.id} {user.name} {user.level}"

    def dokokashira_handler(item, user, enemy):
        stuck = True

        while stuck:
            user.team.coords.x = random.randint(0, MapData.map_size[0] - 1)
            user.team.coords.y = random.randint(0, MapData.map_size[1] - 1)

            stuck = False
            if (MapData.collision_zones[0][user.team.coords.x, user.team.coords.y][0] != 0):
                stuck = True
            
        user.glitch_states.append(GlitchId.TELEPORT)

        return "aKevin: Hey! W"

    def cm00_handler(item, user, enemy):
        user.set_id(CybermonId.MALRAJA)
        return "."

    def cm_10_handler(item, user, enemy):
        user.set_id(CybermonId((user.id.value - 1) % 64))
        return "."

    # Effect: Raises DEF, SpDEF, HP EVs by 4 each. Lowers ATK, SpATK EVs by 4 each. These values can under/overflow.
    #    self.max_hp = hp
    #    self.patk = patk
    #    self.pdef = pdef
    #    self.satk = satk
    #    self.sdef = sdef
    #    self.spd = spd

    def green_cyberdrug_handler(item, user, enemy):
        user.add_evs([4, -4, 4, -4, 4, 0])
        return f"{user.name} is now more resilient!"

    # Effect: Raises ATK, SpATK, SPD EVs by 4 each. Lowers HP EV by 6. This is a pathway to the Pomeg glitch.
    def purple_cyberdrug_handler(item, user, enemy):
        user.add_evs([-6, 4, 0, 4, 0, 4])
        return f"{user.name} is now stronger but can be knocked out easier!"
    
    def item_chest_handler(item, user, enemy):
        item_selection = item["drops"]
        items_to_drop = random.randint(item["drop_chance"][0], item["drop_chance"][1])
        
        drops = []
        for i in range(items_to_drop):
            drop = ItemData.get_item(random.choice(item_selection))
            drops.append(drop)
            user.team.add_item(drop["id"], 1)

        return "You got: " + ", ".join(["**" + i["name"] + "**" for i in drops])
    
    def exit_handler(item, user, empty):
        user.team.add_state(TeamState.NOCLIP)
        user.glitch_states.append(GlitchId.NOCLIP)
        return "You exited the room!"

    def itemfunction_id_61(item, user, empty): return ""

    # Use functions are guaranteed valid parameters (? TODO)S

    item = {
        # Effect: "Randomises the user's bitfield for major statuses. If we assign an int value from 0-7 for a major status, we have 0 (no status), 1-6 (the 6 statuses) and 7 (a glitch status). This glitch status is called ""--"" and does nothing (but also protects the cybermon from other major statuses!)"
        # Obtain: Gained through the NULLITEM glitch.
        ItemId.NULL_BERRY: {
            "name": "00 Berry", "type": ItemType.BERRY,
            "description": "A a",
            "emoji": "<:00_berry:918636753529749544>", "use_function": null_berry_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM], "shop_cost": -1
        },

        # Effect: Removes a burn and heals by 10% max HP.
        # Obtain: Found throughout the world.
        ItemId.APPA_BERRY: {
            "name": "Appa Berry", "type": ItemType.BERRY,
            "description": "A hard berry with a sour taste. Used to cure burns, its medicinal properties also slightly heal the Cybermon that consumes it.",
            "emoji": "<:appa_berry:918636753777221692>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.BURN, "heal": 0.1},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Removes poison.
        # Obtain: Found throughout the world.
        ItemId.MELO_BERRY: {
            "name": "Melo Berry", "type": ItemType.BERRY,
            "description": "Soft and round, this berry contains compounds able to cure poisoning.",
            "emoji": "<:melo_berry:918636774442565683>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.POISON},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 100
        },

        # Effect: Stuns target.
        # Obtain: Found throughout the world.
        ItemId.PIU_BERRY: {
            "name": "Piu Berry", "type": ItemType.BERRY,
            "description": "A large, squishy berry that bursts easily. Can be thrown to stun a target.",
            "emoji": "<:piu_berry:918636774518034462>", "use_function": piu_berry_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM], "shop_cost": 100
        },

        # Effect: Removes stun
        # Obtain: Found throughout the world.
        ItemId.DURAN_BERRY: {
            "name": "Duran Berry", "type": ItemType.BERRY,
            "description": "A foul-tasting berry that cures all mind-altering effects, such as stunning.",
            "emoji": "<:duran_berry:918636774195101697>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.STUN},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 100
        },

        # Effect: Removes freeze. Raises SPD and ATK by one stage.
        # Obtain: Found throughout the world.
        ItemId.CHARIN_BERRY: {
            "name": "Charin Berry", "type": ItemType.BERRY,
            "description": "A long gourd-like berry that can cure freezing. Eating it focuses one's mind, raising SPD and ATK if in battle.",
            "emoji": "<:charin_berry:918636753588478003>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.FREEZE, "raise": [0, 1, 0, 0, 0, 1]},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 200
        },

        # Effect: Removes paralysis.
        # Obtain: Found throughout the world.
        ItemId.GRAM_BERRY: {
            "name": "Gram Berry", "type": ItemType.BERRY,
            "description": "A tough, sinewy berry that cures paralysis.",
            "emoji": "<:gram_berry:918636774455148575>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.PARALYSIS},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Removes sleep.
        # Obtain: Found throughout the world.
        ItemId.FLAK_BERRY: {
            "name": "Flak Berry", "type": ItemType.BERRY,
            "description": "Infused with vitamins, this berry wakes up a sleeping Cybermon.",
            "emoji": "<:flak_berry:918636774383833108>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.SLEEP},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Removes all major status conditions. (Sets the cybermon's major status ID to 0)
        # Obtain: Found throughout the world. Rare.
        ItemId.WAK_BERRY: {
            "name": "Wak Berry", "type": ItemType.BERRY,
            "description": "This berry can cure any major status condition.",
            "emoji": "<:wak_berry:918640401060089936>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.NONE},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Raise ATK and SpATK by 3 stages.
        # Obtain: Found only in the Berry Field or as challenge completion rewards.
        ItemId.ONON_BERRY: {
            "name": "Onon Berry", "type": ItemType.BERRY,
            "description": "The sour taste of this berry induces a feral anger in those who consume it, greatly raising ATK and SpATK while in battle.",
            "emoji": "<:onon_berry:918636774450925608>", "use_function": generic_item_handler, "use_options": {"raise": [0, 3, 0, 3, 0, 0]},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM], "shop_cost": -1
        },

        # Effect: Removes all major and minor status conditions (including positive ones).
        # Obtain: Found only in the Berry Field or as challenge completion rewards.
        ItemId.BLANC_BERRY: {
            "name": "Blanc Berry", "type": ItemType.BERRY,
            "description": "This berry's powerful syrup will remove all effects, including stat changes or major status conditions.",
            "emoji": "<:blanc_berry:918636753642991636>", "use_function": blanc_berry_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Heals by 40% max HP (60% if cybermon is Grass-type)
        # Obtain: Found only in the Berry Field or as challenge completion rewards.
        ItemId.MED_BERRY: {
            "name": "Med Berry", "type": ItemType.BERRY,
            "description": "Containing very powerful medicinal agents, this berry can heal a Cybermon greatly. Grass-types can resonate further for even more healing.",
            "emoji": "<:med_berry:918636774564179978>", "use_function": med_berry_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Knocks out a Cybermon instantly if their is_ohkoable flag is set to 1. (This is part of the Cybermon's data bitfield!)
        # Obtain: Found only in the Berry Field or as challenge completion rewards. Very, very rare.
        ItemId.PULPA_BERRY: {
            "name": "Pulpa Berry", "type": ItemType.BERRY,
            "description": "This berry contains powerful stunning compounds. Any weak, wild Cybermon that consumes this berry will be immediately knocked out.",
            "emoji": "<:pulpa_berry:918636774404792400>", "use_function": pulpa_berry_handler,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": -1
        },

        # Effect: Raises DEF, SpDEF, HP EVs by 4 each. Lowers ATK, SpATK EVs by 4 each. These values can under/overflow.
        # Obtain: Found as challenge completion rewards.
        ItemId.GREEN_CYBERDRUG: {
            "name": "Green Cyberdrug", "type": ItemType.DEVICE,
            "description": "This solution, when injected, will permanently increase the Cybermon's base DEF, SpDEF and HP while reducing their base ATK and SpATK,",
            "emoji": "<:green_cyberdrug:918636774362865664>", "use_function": green_cyberdrug_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Raises ATK, SpATK, SPD EVs by 4 each. Lowers HP EV by 6. This is a pathway to the Pomeg glitch.
        # Obtain: Found as challenge completion rewards.
        ItemId.PURPLE_CYBERDRUG: {
            "name": "Purple Cyberdrug", "type": ItemType.DEVICE,
            "description": "This solution, when injected, will permanently increase the Cybermon's base ATK, SpATK and SPD while reducing their base HP.",
            "emoji": "<:purple_cyberdrug:918640400779067392>", "use_function": purple_cyberdrug_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Raises ATK, SpATK, SPD by 6 stages. Confuses the user.
        # Obtain: Found as challenge completion rewards.
        ItemId.BLUE_CYBERDRUG: {
            "name": "Blue Cyberdrug", "type": ItemType.DEVICE,
            "description": "Injecting a Cybermon with this drug causes an intense fury, confusing them but causing their ATK, SpATK and SPD to be raised to the maximum.",
            "emoji": "<:blue_cyberdrug:918636753697538138>", "use_function": generic_item_handler, "use_options": {"raise": [12, 12, 12, 12, 12, 12], "apply": CybermonStatus.STUN},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM], "shop_cost": 1800
        },

        # Effect: Affected Cybermon gains a 4-turn effect that makes them dodge all non-guaranteed attacks and ends with them being knocked out (bypassing damage; just setting their HP to 0)
        # Obtain: Found as challenge completion rewards.
        ItemId.WHITE_CYBERDRUG: {
            "name": "White Cyberdrug", "type": ItemType.DEVICE,
            "description": "A Cybermon injected with this drug will temporarily evade all dodgeable attacks but be knocked out immediately 4 turns later.",
            "emoji": "<:white_cyberdrug:918640400867143770>", "use_function": white_cyberdrug_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM], "shop_cost": 1800
        },

        # Effect: Heals 150 HP. Stuns user.
        # Obtain: Found throughout the world. Rare.
        ItemId.HERBAL_MEDICINE: {
            "name": "Herbal Medicine", "type": ItemType.DEVICE,
            "description": "This powerful remedy heals a Cybermon by 150 HP but stuns them.",
            "emoji": "<:herbal_medicine:918636774400618496>", "use_function": generic_item_handler, "use_options": {"heal_fixed": 150, "apply": CybermonStatus.STUN},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 1600
        },

        # Effect: Heals 100% HP. Applies sleep.
        # Obtain: Found throughout the world. Rare.
        ItemId.HERBAL_REMEDY: {
            "name": "Herbal Remedy", "type": ItemType.DEVICE,
            "description": "Concentrated herbs inside this capsule fully heal a Cybermon but also send them to sleep.",
            "emoji": "<:herbal_remedy:918636774438346792>", "use_function": generic_item_handler, "use_options": {"heal": 1, "apply": CybermonStatus.SLEEP},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 2400
        },

        # Effect: Heals 100% HP.
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.RED_POWDERBAG: {
            "name": "Red Powderbag", "type": ItemType.DEVICE,
            "description": "The powder in this bag fully heals the Cybermon who ingests it. However, the stress from the application makes it impossible to apply in battle.",
            "emoji": "<:red_powderbag:918640400804241448>", "use_function": generic_item_handler, "use_options": {"heal": 1},
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Sets XP to xp_next and triggers a levelup.
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.GREEN_POWDERBAG: {
            "name": "Green Powderbag", "type": ItemType.DEVICE,
            "description": "The powder in this bag causes its consumer to increase in level by 1 immediately. The potency of this powder makes it unfeasible to apply during battle.",
            "emoji": "<:green_powderbag:918636774484508672>", "use_function": green_powderbag_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Drops 2-3 items, usually of the Device or Medicinal category.
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.CURIOSITY_CHEST: {
            "name": "Curiosity Chest", "type": ItemType.CONTAINER,
            "description": "Contains a few interesting items.",
            "emoji": "<:curiosity_chest:918636774425759744>", "use_function": item_chest_handler,
            "drops": [
                ItemId.POTION, ItemId.MEGA_POTION, 
                ItemId.MEGA_POTION, ItemId.BURN_B_GONE,
                ItemId.WARMING_FLASK, ItemId.LIMBERING_SOLUTION,
                ItemId.GREEN_CYBERDRUG, ItemId.BLUE_CYBERDRUG,
                ItemId.WHITE_CYBERDRUG, ItemId.PURPLE_CYBERDRUG,
                ItemId.RED_POWDERBAG, ItemId.GREEN_POWDERBAG
            ], "drop_chance": (2, 3),
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Drops 5-7 items from the Berry category; much more likely to be common (world drop) berries.
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.BERRY_BASKET: {
            "name": "Berry Basket", "type": ItemType.CONTAINER,
            "description": "Contains many berries, all packed up in a little basket.",
            "emoji": "<:berry_basket:918636753634623528>", "use_function": item_chest_handler,
            "drops": [
                ItemId.APPA_BERRY, ItemId.MELO_BERRY, ItemId.PIU_BERRY,
                ItemId.DURAN_BERRY, ItemId.CHARIN_BERRY, ItemId.GRAM_BERRY,
                ItemId.FLAK_BERRY,
                
                ItemId.APPA_BERRY, ItemId.MELO_BERRY, ItemId.PIU_BERRY,
                ItemId.DURAN_BERRY, ItemId.CHARIN_BERRY, ItemId.GRAM_BERRY,
                ItemId.FLAK_BERRY,
                
                ItemId.APPA_BERRY, ItemId.MELO_BERRY, ItemId.PIU_BERRY,
                ItemId.DURAN_BERRY, ItemId.CHARIN_BERRY, ItemId.GRAM_BERRY,
                ItemId.FLAK_BERRY,
                
                ItemId.APPA_BERRY, ItemId.MELO_BERRY, ItemId.PIU_BERRY,
                ItemId.DURAN_BERRY, ItemId.CHARIN_BERRY, ItemId.GRAM_BERRY,
                ItemId.FLAK_BERRY,
                
                ItemId.WAK_BERRY, ItemId.ONON_BERRY, ItemId.BLANC_BERRY,
                ItemId.WAK_BERRY, ItemId.ONON_BERRY, ItemId.BLANC_BERRY,
                
                ItemId.MED_BERRY, ItemId.MED_BERRY,
                
                ItemId.PULPA_BERRY
            ], "drop_chance": (5, 7),
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Drops 2-3 "Very Rare / Rare" items. 
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.RARITY_CHEST: {
            "name": "Rarity Chest", "type": ItemType.CONTAINER,
            "description": "Contains a few very rare items!",
            "emoji": "<:rarity_chest:918640400833577060>", "use_function": item_chest_handler,
            "drops": [
                ItemId.WAK_BERRY, ItemId.ONON_BERRY, ItemId.BLANC_BERRY,
                ItemId.MED_BERRY, ItemId.PULPA_BERRY,
                ItemId.RED_POWDERBAG, ItemId.GREEN_POWDERBAG,

                ItemId.WAK_BERRY, ItemId.ONON_BERRY, ItemId.BLANC_BERRY,
                ItemId.MED_BERRY, ItemId.PULPA_BERRY,
                ItemId.RED_POWDERBAG, ItemId.GREEN_POWDERBAG,

                ItemId.FLAWLESS_CYBERBOX,

                ItemId.STRANGE_CYBERBOX, ItemId.STRANGE_CYBERBOX,

                ItemId.GREEN_CYBERDRUG, ItemId.PURPLE_CYBERDRUG,
                ItemId.GREEN_CYBERDRUG, ItemId.PURPLE_CYBERDRUG
            ], "drop_chance": (2, 3),
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Drops 4-6 items from the Medicinal category, or {Herbal Medicine, Herbal Remedy}.
        # Obtain: Found as challenge completion rewards or throughout the world. Rare.
        ItemId.SUPPLY_BACKPACK: {
            "name": "Supply Backpack", "type": ItemType.CONTAINER,
            "description": "Contains a number of healing items and medicines within.",
            "emoji": "<:supply_backpack:918640400808431667>", "use_function": item_chest_handler,
            "drops": [
                ItemId.MINI_POTION, ItemId.POTION, ItemId.MEGA_POTION,
                ItemId.BURN_B_GONE, ItemId.WARMING_FLASK,
                ItemId.LIMBERING_SOLUTION,

                ItemId.HERBAL_MEDICINE, ItemId.HERBAL_REMEDY
            ], "drop_chance": (4, 6),
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Heal by 25
        # Obtain: Found throughout the world.
        ItemId.MINI_POTION: {
            "name": "Mini-Potion", "type": ItemType.MEDICINAL,
            "description": "A medicine that heals the HP of one Cybermon by 25 points.",
            "emoji": "<:minipotion:918636774501269545>", "use_function": generic_item_handler, "use_options": {"heal_fixed": 25},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 50
        },

        # Effect: Heal by 80
        # Obtain: Found throughout the world.
        ItemId.POTION: {
            "name": "Potion", "type": ItemType.MEDICINAL,
            "description": "A medicine that heals the HP of one Cybermon by 80 points.",
            "emoji": "<:potion:918636774383812639>", "use_function": generic_item_handler, "use_options": {"heal_fixed": 80},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 250
        },

        # Effect: Heal by 200
        # Obtain: Found throughout the world. Rare.
        ItemId.MEGA_POTION: {
            "name": "Mega-Potion", "type": ItemType.MEDICINAL,
            "description": "A medicine that heals the HP of one Cybermon by 200 points.",
            "emoji": "<:megapotion:918636774383837254>", "use_function": generic_item_handler, "use_options": {"heal_fixed": 200},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 500
        },

        # Effect: Removes a burn.
        # Obtain: Found throughout the world.
        ItemId.BURN_B_GONE: {
            "name": "Burn-B-Gone", "type": ItemType.MEDICINAL,
            "description": "A medicine that removes a burn.",
            "emoji": "<:burnbgone:918636753525551144>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.BURN},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 150
        },

        # Effect: Removes freezing and sleep.
        # Obtain: Found throughout the world.
        ItemId.WARMING_FLASK: {
            "name": "Warming Flask", "type": ItemType.MEDICINAL,
            "description": "A medicine that removes frost and sleep.",
            "emoji": "<:warming_flask:918640400741322793>", "use_function": generic_item_handler, "use_options": {"remove": [CybermonStatus.FREEZE, CybermonStatus.SLEEP]},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 250
        },

        # Effect: Removes paralysis.
        # Obtain: Found throughout the world.
        ItemId.LIMBERING_SOLUTION: {
            "name": "Limbering Solution", "type": ItemType.MEDICINAL,
            "description": "A medicine that cures paralysis.",
            "emoji": "<:limbering_solution:918636774606114827>", "use_function": generic_item_handler, "use_options": {"remove": CybermonStatus.PARALYSIS},
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": 150
        },

        # Effect: Teaches Puddles.
        # Obtain: Found as a Crypto completion reward.
        ItemId.CM01: {
            "name": "CM01", "type": ItemType.CM,
            "description": "Consume this device to teach a Water-type Cybermon \"Puddles\".",
            "emoji": "<:cm01:918636753626206228>", "use_function": generic_teach_move, "teach_move": MoveId.PUDDLES,
            "restriction": CybermonType.WATER,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Magma.
        # Obtain: Found as a Pwn completion reward.
        ItemId.CM02: {
            "name": "CM02", "type": ItemType.CM,
            "description": "Consume this device to teach a Fire-type Cybermon \"Magma\".",
            "emoji": "<:cm02:918636753663954944>", "use_function": generic_teach_move, "teach_move": MoveId.MAGMA,
            "restriction": CybermonType.FIRE,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Blazing Strike.
        # Obtain: Found as a Pwn completion reward.
        ItemId.CM03: {
            "name": "CM03", "type": ItemType.CM,
            "description": "Consume this device to teach a Fighting-type Cybermon \"Blazing Strike\".",
            "emoji": "<:cm03:918636774367039518>", "use_function": generic_teach_move, "teach_move": MoveId.BLAZING_STRIKE, 
            "restriction": CybermonType.FIGHTING,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Burning Cold.
        # Obtain: Found as a Web completion reward.
        ItemId.CM04: {
            "name": "CM04", "type": ItemType.CM,
            "description": "Consume this device to teach a Water-type Cybermon \"Burning Cold\".",
            "emoji": "<:cm04:918636774203465739>", "use_function": generic_teach_move, "teach_move": MoveId.BURNING_COLD,
            "restriction": CybermonType.WATER,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Overgrowth.
        # Obtain: Found as a Web completion reward.
        ItemId.CM05: {
            "name": "CM05", "type": ItemType.CM,
            "description": "Consume this device to teach a Grass-type Cybermon \"Overgrowth\".",
            "emoji": "<:cm05:918636774048280608>", "use_function": generic_teach_move, "teach_move": MoveId.OVERGROWTH,
            "restriction": CybermonType.GRASS,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Soul Touch.
        # Obtain: Found as a Web completion reward.
        ItemId.CM06: {
            "name": "CM06", "type": ItemType.CM,
            "description": "Consume this device to teach a Ghost-type Cybermon \"Soul Touch\".",
            "emoji": "<:cm06:918636774312542248>", "use_function": generic_teach_move, "teach_move": MoveId.SOUL_TOUCH,
            "restriction": CybermonType.GHOST,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Spirit Siphon.
        # Obtain: Found as a Rev completion reward.
        ItemId.CM07: {
            "name": "CM07", "type": ItemType.CM,
            "description": "Consume this device to teach a Ghost-type Cybermon \"Spirit Siphon\".",
            "emoji": "<:cm07:918636774484475924>", "use_function": generic_teach_move, "teach_move": MoveId.SPIRIT_SIPHON,
            "restriction": CybermonType.GHOST,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Sp▲ïÖ« Si.
        # Obtain: Found as a Rev completion reward.
        ItemId.CM08: {
            "name": "CM08", "type": ItemType.CM,
            "description": "Consume this device to teach a Ghost-type Cybermon \"Sp▲ïÖ« Si",
            "emoji": "<:cm08:918636774400593940>", "use_function": generic_teach_move, "teach_move": MoveId.SP_____SI, 
            "restriction": CybermonType.GHOST,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Sure Hit.
        # Obtain: Found as a Forensics completion reward.
        ItemId.CM09: {
            "name": "CM09", "type": ItemType.CM,
            "description": "Consume this device to teach any Cybermon \"Sure Hit\".",
            "emoji": "<:cm09:918636774320914552>", "use_function": generic_teach_move, "teach_move": MoveId.SURE_HIT,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Vital Pulse.
        # Obtain: Found as a Forensics completion reward.
        ItemId.CM10: {
            "name": "CM10", "type": ItemType.CM,
            "description": "Consume this device to teach an Electric-type Cybermon \"Vital Pulse\".",
            "emoji": "<:cm10:918636774488698880>", "use_function": generic_teach_move, "teach_move": MoveId.VITAL_PULSE,
            "restriction": CybermonType.ELECTRIC,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Super-Reversal.
        # Obtain: Found as a Misc completion reward.
        ItemId.CM11: {
            "name": "CM11", "type": ItemType.CM,
            "description": "Consume this device to teach any Cybermon \"Super-Reversal\".",
            "emoji": "<:cm11:918636774346096711>", "use_function": generic_teach_move, "teach_move": MoveId.SUPER_REVERSAL,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Dark Thoughts.
        # Obtain: Found as a Misc completion reward.
        ItemId.CM12: {
            "name": "CM12", "type": ItemType.CM,
            "description": "Consume this device to teach a Dark-type Cybermon \"Dark Thoughts\".",
            "emoji": "<:cm12:918636774086029323>", "use_function": generic_teach_move, "teach_move": MoveId.DARK_THOUGHTS,
            "restriction": CybermonType.DARK,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Powerblast.
        # Obtain: Found as a Emulation completion reward.
        ItemId.CM13: {
            "name": "CM13", "type": ItemType.CM,
            "description": "Consume this device to teach any Cybermon \"Powerblast\".",
            "emoji": "<:cm13:918636774341873694>", "use_function": generic_teach_move, "teach_move": MoveId.POWERBLAST,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Normal.
        # Obtain: Found as a Emulation completion reward.
        ItemId.CM14: {
            "name": "CM14", "type": ItemType.CM,
            "description": "Consume this device to teach any Cybermon \"Normal\".",
            "emoji": "<:cm14:918636774044098632>", "use_function": generic_teach_move, "teach_move": MoveId.NORMAL,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches @.
        # Obtain: Found as a Hardware completion reward.
        ItemId.CM15: {
            "name": "CM15", "type": ItemType.CM,
            "description": "Consume this device to teach any Cybermon \"@\".",
            "emoji": "<:cm15:918636774362845224>", "use_function": generic_teach_move, "teach_move": MoveId._,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Teaches Bubble.
        # Obtain: Found as a Blockchain completion reward.
        ItemId.CM16: {
            "name": "CM16", "type": ItemType.CM,
            "description": "Consume this device to teach a Water-type Cybermon \"Bubble\".",
            "emoji": "<:cm16:918636774413185054>", "use_function": generic_teach_move, "teach_move": MoveId.BUBBLE,
            "restriction": CybermonType.WATER,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Run capture -> weight 1
        # Obtain: Found throughout the world.
        ItemId.CYBERBOX: {
            "name": "Cyberbox", "type": ItemType.CYBERBOX,
            "description": "A device used for capturing Cybermon. It is used in battle. Weaker Cybermon are more susceptible to capture.",
            "emoji": "<:cyberbox:918636774677426246>", "use_function": cyberbox_handler, "chance_weight": 1,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": 150
        },

        # Effect: Run capture -> weight 1.6
        # Obtain: Found throughout the world.
        ItemId.SUPER_CYBERBOX: {
            "name": "Super Cyberbox", "type": ItemType.CYBERBOX,
            "description": "A device used for capturing Cybermon. It is more effective than a regular Cyberbox.",
            "emoji": "<:super_cyberbox:918640400665821215>", "use_function": cyberbox_handler, "chance_weight": 1.6,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": 350
        },

        # Effect: Run capture -> weight 2.2
        # Obtain: Found throughout the world. Rare.
        ItemId.ULTRA_CYBERBOX: {
            "name": "Ultra Cyberbox", "type": ItemType.CYBERBOX,
            "description": "A device used for capturing Cybermon. It is more effective than a Super Cyberbox.",
            "emoji": "<:ultra_cyberbox:918640400795852820>", "use_function": cyberbox_handler, "chance_weight": 2.2,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": 600
        },

        # Effect: Run capture -> weight 1 if cybermon's ID is even else 3
        # Obtain: Found throughout the world. Rare.
        ItemId.STRANGE_CYBERBOX: {
            "name": "Strange Cyberbox", "type": ItemType.CYBERBOX,
            "description": "A device used for capturing Cybermon. It seems to be more effective against certain Cybermon, but the criteria are uncertain.",
            "emoji": "<:strange_cyberbox:918640400972013588>", "use_function": cyberbox_handler, "chance_weight": 1, 'chance_custom': 3,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": -1
        },

        # Effect: Run capture -> weight 65536
        # Obtain: Found throughout the world. Very rare.
        ItemId.FLAWLESS_CYBERBOX: {
            "name": "Flawless Cyberbox", "type": ItemType.CYBERBOX,
            "description": "A device used for capturing Cybermon. It will capture a target without fail.",
            "emoji": "<:flawless_cyberbox:918636774383824946>", "use_function": cyberbox_handler, "chance_weight": 65536,
            "usability": [ItemUsability.BATTLE_WILD], "shop_cost": -1
        },

        # Effect: "Teaches" a Cybermon an empty move, essentially removing it. Type restriction removed.
        # Obtain: Gained through the CM incrementation glitch. (Trying to increment this CM again will cause it to turn into CM-10)
        ItemId.CM17: {
            "name": "CM17", "type": ItemType.CM,
            "description": "Consume this device to teach a  -typed Cybermon \"\".",
            "emoji": "<:cm17:918636774031491093>", "use_function": generic_teach_move, "teach_move": MoveId.DELETER_MOVE,
            "restriction": None,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: A reusable heal of 76 HP. Increments its own quantity by 2 every time it is used.
        # Obtain: Gained through the item merge glitch on items between ID 22 (Berry Basket) and 30 (Limbering Solution). Other merge candidates will cause the creation of "coin".
        ItemId.QUEQUEQUEQUEQUE: {
            "name": "?????", "type": ItemType.MEDICINAL,
            "description": "f one Cybermon by 80 points.A medicine that heals the HP",
            "emoji": "<:quequequequeque:918640400942653470>", "use_function": queque_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Change the character's sprite to this coin sprite until they move again.
        # Obtain: Gained through the item merge glitch when it does not create ?????.
        ItemId.COIN: {
            "name": "coin", "type": ItemType.DEVICE,
            "description": "Contains a few interesting items.[EOF]////Consume this device to teach a same-typed Cybe",
            "emoji": "<:coin:918636774346088448>", "use_function": coin_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: In battle, immediately abort the battle and return to the overworld. If against another player, instead print "Using this item failed!". If not in battle, immediately teleport to map spawn point and abort whatever action was going on.
        # Obtain: Allow ????? to reach a quantity of 255, then use it again. It transforms into this item.
        ItemId.BACK: {
            "name": "Back", "type": ItemType.DEVICE,
            "description": "Back to menu",
            "emoji": "<:back:918636753647190136>", "use_function": back_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Remove the player's "in battle" state. The battle continues. If not in battle, the player is set to be in battle against Cybermon ID 0 with no prompt (so, they have to first work out they are in battle, lol)
        # Obtain: Use the item ID incrementation glitch. (Note that above ID 58, items will disappear instead of being incremented)
        ItemId.MENU: {
            "name": "Menu", "type": ItemType.DEVICE,
            "description": "Menu",
            "emoji": "<:menu:918636774438371389>", "use_function": menu_handler,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Using this item will enable walk through walls
        # Obtain: Use the item ID incrementation glitch. (Note that above ID 58, items will disappear instead of being incremented)
        ItemId.EXIT: {
            "name": "Exit", "type": ItemType.DEVICE,
            "description": "Exit",
            "emoji": "<:exit:918636774388015104>", "use_function": exit_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Fully heals all Cybermon. Increments its own quantity by 1.
        # Obtain: Use the "Back" item while healing. It will transform into this item permanently.
        ItemId.QZ_: {
            "name": "qz-", "type": ItemType.MEDICINAL,
            "description": "hank you for visiting! We hope you come again soon!",
            "emoji": "<:qz:918640400816816248>", "use_function": qz_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Sets a Cybermon's level to 100. (Does not trigger evolution or move learning.)
        # Obtain: Use the "Back" item while learning a move on levelup. It will transform into this item permanently.
        ItemId.T: {
            "name": "t", "type": ItemType.MEDICINAL,
            "description": "treat treat treat treat treat",
            "emoji": "<:t_:918640400883916800>", "use_function": t_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: A reference to the Dokokashira door glitch (https://glitchcity.wiki/Dokokashira_door_glitch), this will warp the player chaotically to a random non-blocked location every single time they move (change position) for as long as the item is in their inventory.
        # Obtain: Use the Cybermon-to-item transform glitch with Stickibat.
        ItemId.DOKOKASHIRA: {
            "name": "ありつなまぁおこいの", "type": ItemType.MEDICINAL,
            "description": "Dokokashira...?",
            "emoji": "<:dokokashira:918640400506454017>", "use_function": dokokashira_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Does nothing at all and increments its own quantity by 1 (causing it to remain the same). (This could also grant a flag when used.)
        # Obtain: Gained through the elusive arbitrary item glitch. (This should be the final glitch in a big chain.)
        ItemId.COMPLETION_MEDAL: {
            "name": "Completion Medal", "type": ItemType.DEVICE,
            "description": "Does nothing. Congratulations!",
            "emoji": "<:completion_medal:918636774400610384>", "use_function": itemfunction_id_61,
            "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Transforms any Cybermon into Malraja, keeping all persistent data like moves.
        # Obtain: Gained through the CM incrementation glitch.
        ItemId.CM00: {
            "name": "CM00", "type": ItemType.CM,
            "description": "Consume this device to teach a same-typed Cybermon \"Malraja\".",
            "emoji": "<:cm00:918636753525571644>", "use_function": cm00_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        },

        # Effect: Reduces the ID of any Cybermon by 1, keeping all persistent data like moves.
        # Obtain: Gained through the CM incrementation glitch.
        ItemId.CM_10: {
            "name": "CM-10", "type": ItemType.CM,
            "description": "Consume this device to   Cybermon \"[EOF]",
            "emoji": "<:cm10:918636774656446464>", "use_function": cm_10_handler,
            "usability": [ItemUsability.OUTSIDE_BATTLE], "shop_cost": -1
        }
    }
