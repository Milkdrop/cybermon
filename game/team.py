from .enums import *
from .item import ItemData
from .map import MapData
import traceback

class Team:
    class Coords:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def __eq__(self, second):
            return self.x == second.x and self.y == second.y

        def __add__(self, other):
            if isinstance(other, tuple):
                return Team.Coords(self.x + other[0], self.y + other[1])
            elif isinstance(other, Team.Coords):
                return Team.Coords(self.x + other.x, self.y + other.y)
            else:
                raise ValueError(
                    "+ operation on Team.Coords works only with Team.Coords or tuple. other was type {}".format(
                        type(other)
                    )
                )
        
    def __init__(self, team_id, name, coords, country_code = "", avatar_url = ""):
        self.id = team_id
        self.name = name
        self.coords = Team.Coords(coords[0], coords[1])
        self.country_code = country_code
        self.avatar_url = avatar_url

        self.movement_path = []
        self.members = []
        self.cybermon = []
        self.pc_cybermon = []
        self.rewards_taken = []
        self.orientation = Orientation.SOUTH

        self.states = []

        self.battle = None
        self.challenged_team = None
        self.accepted_challenge = False
        self.blocked_team_ids = []

        self.revival_timestamp = 0

        self.items = []
        self.money = 0
        self.elo = 1500
        self.games = 0
        
        self.npcs_encountered = {}
        self.notify_callback = None
        self.got_glitch = []

    async def notify(self, message):
        if self.notify_callback is not None:
            try:
                await self.notify_callback(self, message)
            except Exception as e:
                traceback.print_exc()
        else:
            print(f"{self.name}'s notify callback is empty!")

    async def notify_glitch(self, glitch_id, message):
        if glitch_id not in self.got_glitch:
            await self.notify(message)
            self.got_glitch.append(glitch_id)

    def add_state(self, state):
        if (state not in self.states):
            self.states.append(state)
        else:
            print(f"Attempted to add state {state} twice to team {self.id}")


    def remove_state(self, state):
        if (state in self.states):
            print(self.states)
            self.states.remove(state)
            print(self.states)
        elif (self.id is not None):
            print(f"Attempted to remove nonexistent {state} from team {self.id}")

    def add_item(self, item_id, quantity):
        item = ItemData.get_item(item_id)

        item_already_exists = False

        for i, team_item in enumerate(self.items):
            if (team_item["id"] == item["id"]):
                team_item["quantity"] += quantity

                if (team_item["quantity"] <= 0):
                    del self.items[i]
                else:
                    self.items[i] = team_item

                item_already_exists = True
                break
                
        if (not item_already_exists):
            if (quantity > 0):
                item["quantity"] = quantity
                self.items.append(item)

    def is_at_object(self, object_type):
        objects = MapData.get_all_objects_of_type(object_type)

        for o in objects:
            if self.coords.x == o["x"] and self.coords.y == o["y"]:
                return True
        
        return False

    def get_near_object(self, object_type):
        objects = MapData.get_all_objects_of_type(object_type)

        for o in objects:
            if (self.coords.x, self.coords.y) in [(o["x"], o["y"]), (o["x"] - 1, o["y"]), (o["x"] + 1, o["y"]), (o["x"], o["y"] - 1), (o["x"], o["y"] + 1)]:
                return o
        
        return None

    def is_in_state(self, state):
        return state in self.states
    
    def can_use_map_objects(self):
        return (
                TeamState.BATTLE not in self.states
            and TeamState.MOVING not in self.states
            and TeamState.DEAD not in self.states
            and TeamState.CHALLENGED not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )

    def can_move(self):
        return TeamState.GLITCH_CAN_MOVE_FOREVER in self.states or (
                TeamState.BATTLE not in self.states
            and TeamState.DEAD not in self.states
            and TeamState.CHALLENGED not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )

    def can_challenge(self):
        return (
                TeamState.BATTLE not in self.states
            and TeamState.MOVING not in self.states
            and TeamState.DEAD not in self.states
            and TeamState.CHALLENGED not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )
    
    def can_battle(self):
        return (
                TeamState.BATTLE not in self.states
            and TeamState.MOVING not in self.states
            and TeamState.DEAD not in self.states
            and TeamState.CHALLENGED not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )

    def can_swap(self):
        return (
            TeamState.BATTLE not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )
    
    def can_learn_moves(self):
        return (
            TeamState.BATTLE not in self.states
            and TeamState.NO_CYBERMON not in self.states
        )

    def can_use_bag(self):
        return TeamState.BATTLE not in self.states

    def get_active_cybermon(self):
        for cybermon in self.cybermon:
            if (cybermon.hp > 0):
                return cybermon
        
        return None


    def set_challenged(self, by_team):
        self.add_state(TeamState.CHALLENGED)
        self.challenged_team = by_team
        self.accepted_challenge = False
    
    
    def clear_challenged(self):
        self.remove_state(TeamState.CHALLENGED)
        self.challenged_team = None
        self.accepted_challenge = False
