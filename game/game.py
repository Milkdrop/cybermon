import os
import sqlite3
import time
import asyncio
import random

from .enums import *
from utils import Utils
from .battle import Battle
from .team import Team
from .cybermon import Cybermon
from .map import MapData
from .pathfind import find_path
from .item import ItemData
from .move import MoveData

class Game:
    def __init__(self, db_path):
        self.db_conn = sqlite3.connect(db_path)
        self.db_conn.row_factory = sqlite3.Row
        self.db_cur = self.db_conn.cursor()
        self.verify_db()

        self.movement_callbacks = {}
        self.timed_actions = []
        self.teams = {}

        self.starters = {
            10: {"id": CybermonId.GUILGILE},
            14: {"id": CybermonId.CROCOAL},
            19: {"id": CybermonId.LIZEROON},
            35: {"id": CybermonId.FINNIAL},
            32: {"id": CybermonId.GRAVENDOU},
            46: {"id": CybermonId.MELANYAN}
        }

        self.rewards = {
            1: [{"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}, {"id": ItemId.BLANC_BERRY, "quantity": 3}],
            2: [{"id": ItemId.RARITY_CHEST, "quantity": 1}],
            3: [{"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 4}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}],
            4: [{"id": ItemId.CM11, "quantity": 1}, {"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}],
            5: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.MED_BERRY, "quantity": 2}, {"id": ItemId.HERBAL_REMEDY, "quantity": 4}],
            6: [{"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.WHITE_CYBERDRUG, "quantity": 1}, {"id": ItemId.WAK_BERRY, "quantity": 3}],
            7: [{"id": ItemId.CM16, "quantity": 1}, {"id": ItemId.BERRY_BASKET, "quantity": 1}],
            8: [{"id": ItemId.RARITY_CHEST, "quantity": 1}],
            9: [{"id": ItemId.CM01, "quantity": 1}, {"id": ItemId.ONON_BERRY, "quantity": 3}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            11: [{"id": ItemId.RARITY_CHEST, "quantity": 1}],
            12: [{"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.WAK_BERRY, "quantity": 3}],
            13: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.WAK_BERRY, "quantity": 3}, {"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}],
            14: [{"id": ItemId.CURIOSITY_CHEST, "quantity": 1}],
            15: [{"id": ItemId.CM02, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 4}, {"id": ItemId.MED_BERRY, "quantity": 2}],
            16: [{"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}, {"id": ItemId.PURPLE_CYBERDRUG, "quantity": 1}, {"id": ItemId.MED_BERRY, "quantity": 2}],
            17: [{"id": ItemId.CM03, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 4}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}, {"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}, {"id": ItemId.WAK_BERRY, "quantity": 3}],
            18: [{"id": ItemId.BERRY_BASKET, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            20: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 6}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            21: [{"id": ItemId.CM12, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}],
            22: [{"id": ItemId.CM04, "quantity": 1}, {"id": ItemId.BERRY_BASKET, "quantity": 1}],
            23: [{"id": ItemId.HERBAL_REMEDY, "quantity": 6}, {"id": ItemId.BERRY_BASKET, "quantity": 1}, {"id": ItemId.PURPLE_CYBERDRUG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            24: [{"id": ItemId.GREEN_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            25: [{"id": ItemId.CM05, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}, {"id": ItemId.BLUE_CYBERDRUG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            26: [{"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.BLUE_CYBERDRUG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            28: [{"id": ItemId.CM06, "quantity": 1}, {"id": ItemId.BERRY_BASKET, "quantity": 1}],
            29: [{"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 4}, {"id": ItemId.BLANC_BERRY, "quantity": 3}],
            30: [{"id": ItemId.CM10, "quantity": 1}, {"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.MED_BERRY, "quantity": 2}],
            31: [{"id": ItemId.GREEN_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            33: [{"id": ItemId.CM09, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 4}, {"id": ItemId.ONON_BERRY, "quantity": 3}],
            34: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            36: [{"id": ItemId.CM07, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}],
            37: [{"id": ItemId.CM08, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.WAK_BERRY, "quantity": 3}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            38: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            39: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 6}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            40: [{"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}, {"id": ItemId.WAK_BERRY, "quantity": 3}],
            41: [{"id": ItemId.WHITE_CYBERDRUG, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.BLUE_CYBERDRUG, "quantity": 1}],
            43: [{"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.WAK_BERRY, "quantity": 6}],
            44: [{"id": ItemId.RARITY_CHEST, "quantity": 1}],
            47: [{"id": ItemId.WHITE_CYBERDRUG, "quantity": 1}, {"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            48: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}],
            49: [{"id": ItemId.CM15, "quantity": 1}, {"id": ItemId.PURPLE_CYBERDRUG, "quantity": 1}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}],
            50: [{"id": ItemId.BERRY_BASKET, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            51: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}, {"id": ItemId.WAK_BERRY, "quantity": 3}],
            52: [{"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.GREEN_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 4}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}],
            53: [{"id": ItemId.BERRY_BASKET, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            55: [{"id": ItemId.HERBAL_REMEDY, "quantity": 6}, {"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}],
            56: [{"id": ItemId.CM13, "quantity": 1}, {"id": ItemId.BERRY_BASKET, "quantity": 1}],
            57: [{"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 4}],
            58: [{"id": ItemId.GREEN_CYBERDRUG, "quantity": 1}, {"id": ItemId.WHITE_CYBERDRUG, "quantity": 1}],
            59: [{"id": ItemId.CM14, "quantity": 1}, {"id": ItemId.GREEN_POWDERBAG, "quantity": 1}],
            60: [{"id": ItemId.SUPPLY_BACKPACK, "quantity": 1}, {"id": ItemId.CURIOSITY_CHEST, "quantity": 1}, {"id": ItemId.MED_BERRY, "quantity": 2}],
            61: [{"id": ItemId.RED_POWDERBAG, "quantity": 1}, {"id": ItemId.RARITY_CHEST, "quantity": 1}, {"id": ItemId.HERBAL_REMEDY, "quantity": 2}, {"id": ItemId.HERBAL_MEDICINE, "quantity": 2}]
        }

        self.challenge_names = {}
        self.notify_team_callback = None
        self.notify_callback = None

    def verify_db(self):
        tables = [
            table[0] for table in self.db_cur.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall()
        ]

        if "discord_users" not in tables:
            self.db_cur.execute("CREATE TABLE discord_users(id INTEGER PRIMARY KEY, team INTEGER)")

        if "teams" not in tables:
            self.db_cur.execute("CREATE TABLE teams("
                                "id INTEGER PRIMARY KEY, name TEXT, avatar_url TEXT, country_code TEXT, x INTEGER, y INTEGER, money INTEGER, elo INTEGER, games INTEGER)")

        if "team_blocks" not in tables:
            self.db_cur.execute("CREATE TABLE team_blocks(id INTEGER, team INTEGER, PRIMARY KEY(id, team))")

        if "rewards_taken" not in tables:
            self.db_cur.execute("CREATE TABLE rewards_taken(team INTEGER, challenge INTEGER)")

        if "items" not in tables:
            self.db_cur.execute("CREATE TABLE items(team INTEGER, id INTEGER, quantity INTEGER, PRIMARY KEY(team, id))")

        if "cybermon" not in tables:
            self.db_cur.execute("CREATE TABLE cybermon("
                                "id INTEGER PRIMARY KEY AUTOINCREMENT, team INTEGER, cybermon_id INTEGER, "
                                "in_use INTEGER, nickname text, level INTEGER, xp INTEGER, hp INTEGER, position INTEGER)")

        if "evs" not in tables:
            self.db_cur.execute("CREATE TABLE evs(cybermon INTEGER PRIMARY KEY, ev_hp INTEGER, ev_patk INTEGER, ev_pdef INTEGER, ev_satk INTEGER, ev_sdef INTEGER, ev_spd INTEGER)")

        if "ivs" not in tables:
            self.db_cur.execute("CREATE TABLE ivs(cybermon INTEGER PRIMARY KEY, iv_hp INTEGER, iv_patk INTEGER, iv_pdef INTEGER, iv_satk INTEGER, iv_sdef INTEGER, iv_spd INTEGER)")

        if "moves" not in tables:
            self.db_cur.execute("CREATE TABLE moves(cybermon_id INTEGER, move_id INTEGER, pp INTEGER, pending INTEGER)")

        self.db_cur.execute("PRAGMA case_sensitive_like = true;")

    async def periodic_db_commit(self):
        while True:
            print("DB Commit!")
            self.db_conn.commit()
            await asyncio.sleep(10)

    async def periodic_move_players(self):
        while True:
            teams = self.get_all_teams_data()
            for team in teams:
                iterations = 1
                if len(team.movement_path) <= 6:
                    iterations = len(team.movement_path)

                for i in range(iterations):
                    if team.is_in_state(TeamState.MOVING) and team.can_move():
                        actually_moved = False

                        if len(team.movement_path) > 0:
                            actually_moved = True

                            old_coords = team.coords
                            team.coords = Team.Coords(team.movement_path[0][0], team.movement_path[0][1])
                            team.movement_path = team.movement_path[1:]

                            coords_diff = (team.coords.x - old_coords.x, team.coords.y - old_coords.y)
                            if (coords_diff[0] == 1): team.orientation = Orientation.EAST
                            if (coords_diff[0] == -1): team.orientation = Orientation.WEST
                            if (coords_diff[1] == 1): team.orientation = Orientation.SOUTH
                            if (coords_diff[1] == -1): team.orientation = Orientation.NORTH

                        encounter_zone = MapData.get_encounter_zone(team.coords.x, team.coords.y)

                        if actually_moved:
                            npc = team.get_near_object(MapObject.NPC)
                            if npc is not None:
                                if npc["id"] in team.npcs_encountered:
                                    if int(time.time()) > team.npcs_encountered[npc["id"]] + 60 * 15:
                                        del team.npcs_encountered[npc["id"]]
                                
                                if npc["id"] not in team.npcs_encountered:
                                    team.npcs_encountered[npc["id"]] = int(time.time())
                                    team.add_state(TeamState.BATTLE)

                                    encounter_team = Team(None, npc["name"], (team.coords.x, team.coords.y))

                                    money_range = npc["money"] * 0.1
                                    encounter_team.money = random.randint(int(npc["money"] - money_range), int(npc["money"] + money_range))

                                    for cybermon_config in npc["team"]:
                                        cybermon = Cybermon(cybermon_config[0])
                                        cybermon.configure(encounter_team, level=cybermon_config[1])
                                        encounter_team.cybermon.append(cybermon)

                                    battle = Battle(self, team, encounter_team)
                                    team.battle = battle
                                    encounter_team.battle = battle
                                    
                                    message = f"An NPC approaches you...\n**{npc['name']}**: {npc['message']}"
                                    await self.movement_callbacks[team.id]["encounter_callback"](custom_message = message)

                            elif random.random() < encounter_zone["chance"]:
                                encounter_chance = random.random()

                                for encounter in encounter_zone["encounters"]:
                                    if encounter_chance < encounter["chance"]:
                                        team.add_state(TeamState.BATTLE)

                                        encounter_team = Team(None, '', (team.coords.x, team.coords.y))
                                        encounter_team.money = random.randint(100, 200)

                                        # GLITCH - Encounter yourself
                                        if encounter["cybermon"] == CybermonId.GLITCH_YOURSELF:
                                            attacker = team.get_active_cybermon()
                                            cybermon = Cybermon(attacker.id)

                                            moves = [MoveData.get_move(move["id"]) for move in attacker.moves]
                                            pending_moves = [MoveData.get_move(move["id"]) for move in attacker.pending_moves]
                                            cybermon.configure(encounter_team,
                                                level=attacker.level, nickname = attacker.name, moves = moves, pending_moves = pending_moves,
                                                ivs = attacker.ivs, evs = attacker.evs
                                            )
                                            
                                            cybermon.hp = attacker.hp
                                            cybermon.xp = attacker.xp

                                            cybermon.glitch_states.append(GlitchId.DOLLY)
                                            encounter_team.money = team.money
                                        else:
                                            level_range = encounter["level_range"]
                                            cybermon = Cybermon(encounter["cybermon"])
                                            cybermon.configure(encounter_team, level=random.randint(level_range[0], level_range[1]))
                                        
                                        encounter_team.cybermon.append(cybermon)

                                        battle = Battle(self, team, encounter_team)
                                        team.battle = battle
                                        encounter_team.battle = battle
                                        
                                        await self.movement_callbacks[team.id]["encounter_callback"]()
                                        break
                        
                        if len(team.movement_path) == 0:
                            team.remove_state(TeamState.MOVING)
                            self.db_save_team_coords(team.id)

                            await self.movement_callbacks[team.id]["move_callback"]()

            # TODO - change this back to 2
            await asyncio.sleep(2)

    async def periodic_do_timed_actions(self):
        while True:
            timestamp = int(time.time())

            # execute any actions that we need to do in the future (moving, pissing, etc.)
            for action in self.timed_actions:
                if timestamp >= action["timestamp"]:
                    team = None
                    if "team" in action:
                        team = self.get_team_data(action["team"])
                        if type(team) != Team:
                            print(f"Invalid team in future action: {action['team']}")
                            continue
                    
                    if action["type"] == "battle_timeout":
                        battle = action["battle"]
                        if len(battle.queued_moves) != len(battle.teams):
                            # hardcoded 2 teams
                            losing_team = None
                            winning_team = None

                            if battle.teams[0].id in battle.queued_moves:
                                winning_team = battle.teams[0]
                                losing_team = battle.teams[1]
                            else:
                                winning_team = battle.teams[1]
                                losing_team = battle.teams[0]
                            
                            await battle.battle_timeout_ui_update()
                            await self.battle_end_callback(winning_team, losing_team)
                    
                    elif action["type"] == "revive_team":
                        team.remove_state(TeamState.DEAD)

                    else:
                        print(f"Unknown timed action to remove: {action['type']}")
                    
                    if "async_callback" in action and action["async_callback"] is not None:
                        await action["async_callback"]

                    self.timed_actions.remove(action)

            await asyncio.sleep(1)

    def load_challenge_names(self, challenge_data):
        self.challenge_names = {}
        for c in challenge_data:
            self.challenge_names[c["id"]] = c["title"]

    def create_team_from_ctfx(self, team_id, team_data):
        team = self.get_team_data(team_id)
        
        new_team = False

        if type(team) == GameError:
            new_team = True

        spawn_location = random.choice(MapData.get_all_objects_of_type(MapObject.SPAWN_LOCATION))
        
        self.db_cur.execute("INSERT INTO teams(id, name, avatar_url, country_code, x, y, money, elo, games) VALUES(?, ?, ?, ?, ?, ?, 0, 1500, 0) "
                            + "ON CONFLICT(id) DO NOTHING",
                            (team_id, team_data["team_name"], team_data["avatar_url"], team_data["country_code"], spawn_location["x"], spawn_location["y"]))

        if new_team:
            self.db_cur.execute("INSERT INTO items(team, id, quantity) VALUES (?, ?, ?)", (team_id, ItemId.CYBERBOX.value, 10))
            self.db_cur.execute("INSERT INTO items(team, id, quantity) VALUES (?, ?, ?)", (team_id, ItemId.MINI_POTION.value, 10))

    def assign_user_to_team(self, user_id, team_id):
        self.db_cur.execute("INSERT INTO discord_users VALUES(?, ?) ON CONFLICT(id) DO UPDATE SET team=?",
                            (user_id, team_id, team_id))

        if (team_id in self.teams):
            self.teams[team_id].members.append(user_id)

        self.db_conn.commit()
    
    def swap_cybermon(self, team_id, cybermon_1_index, cybermon_2_index):
        team = self.get_team_data(team_id)
        
        # GLITCH: Maybe glitch something with the swaps?
        # indexed from 0

        if (cybermon_1_index < 0 or (cybermon_1_index >= len(team.cybermon) and cybermon_1_index < 6)
            or cybermon_2_index < 0 or (cybermon_2_index >= len(team.cybermon) and cybermon_2_index < 6)):
            return GameError.CYBERMON_INVALID_INDEX
        
        if ((cybermon_1_index >= 6 and cybermon_1_index >= 6 + len(team.pc_cybermon))
            or (cybermon_2_index >= 6 and cybermon_2_index >= 6 + len(team.pc_cybermon))):
            return GameError.CYBERMON_INVALID_INDEX

        # GLITCH: Removing this can make people be able to swap while in a battle can_swap
        if not team.can_swap():
            return GameError.CYBERMON_CANT_EXECUTE

        cybermon_arr_1 = team.cybermon
        cybermon_arr_2 = team.cybermon
        
        if cybermon_1_index >= 6:
            cybermon_1_index -= 6
            cybermon_arr_1 = team.pc_cybermon

        if cybermon_2_index >= 6:
            cybermon_2_index -= 6
            cybermon_arr_2 = team.pc_cybermon
            
        swp = cybermon_arr_1[cybermon_1_index]
        cybermon_arr_1[cybermon_1_index] = cybermon_arr_2[cybermon_2_index]
        cybermon_arr_2[cybermon_2_index] = swp
        
        self.db_save_team_cybermon(team)
        return True

    def teach_move(self, team_id, cybermon_index, move_index, learn_move_index):
        team = self.get_team_data(team_id)

        if not team.can_learn_moves():
            return GameError.CYBERMON_CANT_EXECUTE

        if cybermon_index < 0 or cybermon_index >= len(team.cybermon):
            return GameError.CYBERMON_INVALID_INDEX

        cybermon = team.cybermon[cybermon_index]

        if (move_index < 0 or learn_move_index < 0 or
        move_index >= len(cybermon.moves) or learn_move_index >= len(cybermon.pending_moves)):
            return GameError.CYBERMON_INVALID_MOVE_INDEX
        
        move = cybermon.pending_moves[learn_move_index]
        if len(cybermon.moves) < 4:
            cybermon.moves.append(move)
            move_index = len(cybermon.moves) - 1
        else:
            cybermon.moves[move_index] = move
        
        cybermon.pending_moves.remove(move)

        if move["id"] == MoveId.DELETER_MOVE:
            del cybermon.moves[move_index]
            new_moves = []

            for move in cybermon.moves:
                new_moves.append(move)

            cybermon.moves = new_moves

            if len(cybermon.moves) == 0:
                team.notify_glitch(GlitchId.POWERLESS, "This cybermon is... powerless??\n**X-MAS{POWERLESS_su9c8n198fc138n38f19c1}**")
        
        self.db_save_team_cybermon(team)
        return True

    def nickname_cybermon(self, team_id, cybermon_index, nickname):
        team = self.get_team_data(team_id)

        if cybermon_index < 0 or cybermon_index >= len(team.cybermon):
            return GameError.CYBERMON_INVALID_INDEX

        team.cybermon[cybermon_index].name = nickname[:16]

        self.db_save_team_cybermon(team)
        return True

    def set_team_starter(self, team_id, starter):
        team = self.get_team_data(team_id)
        
        starter.configure(team, level=5)
        team.cybermon.append(starter)

        self.db_save_team_cybermon(team)
        team.remove_state(TeamState.NO_CYBERMON)

    def team_claim_reward(self, team_id, reward_id):
        team = self.get_team_data(team_id)
        team.rewards_taken.append(reward_id)
        
        for reward in self.rewards[reward_id]:
            team.add_item(reward["id"], reward["quantity"])

        self.db_save_team_rewards(team_id)
        self.db_save_team_items(team_id)

    def get_user_team_data(self, user_id):
        team_id = self.db_cur.execute("SELECT team FROM discord_users WHERE id=?", (user_id,)).fetchone()
        if team_id is None:
            return GameError.USER_NOT_IN_TEAM

        team_id = team_id["team"]

        return self.get_team_data(team_id)

    def get_team_data_from_name_search(self, team_name):
        team_id = self.db_cur.execute("SELECT id FROM teams WHERE name LIKE '%' || ? || '%'", (team_name,)).fetchone()
        if team_id is None:
            return GameError.TEAM_DATA_NULL
        
        return self.get_team_data(team_id["id"])

    def get_team_data(self, team_id):
        if team_id in self.teams:
            return self.teams[team_id]

        team_data_sql = self.db_cur.execute("SELECT * FROM teams WHERE id=?", (team_id,)).fetchone()
        if team_data_sql is None:
            return GameError.TEAM_DATA_NULL

        team_data = Team(team_data_sql["id"], team_data_sql["name"], (team_data_sql["x"], team_data_sql["y"]),
                        country_code = team_data_sql["country_code"], avatar_url = team_data_sql["avatar_url"])
        
        team_data.money = team_data_sql["money"]
        team_data.elo = team_data_sql["elo"]
        team_data.games = team_data_sql["games"]

        for row in self.db_cur.execute("SELECT id FROM discord_users WHERE team=?", (team_data.id,)).fetchall():
            team_data.members.append(row["id"])
        
        active_cybermon = [None] * 6

        for row in self.db_cur.execute("SELECT * FROM cybermon WHERE team=?", (team_data.id,)).fetchall():
            print(f"load {row['cybermon_id']} {row['hp']}")
            
            moves = []
            pending_moves = []

            for row_move in self.db_cur.execute("SELECT * FROM moves WHERE cybermon_id = ?", (row["id"],)).fetchall():
                move = MoveData.get_move(MoveId(row_move["move_id"]))
                move["pp"] = row_move["pp"]

                if row_move["pending"] == 0:
                    moves.append(move)
                else:
                    pending_moves.append(move)

            evs_data = self.db_cur.execute("SELECT * FROM evs WHERE cybermon = ?", (row["id"],)).fetchone()
            if evs_data is None:
                print(f"Team {team_id} had corrupted EVs!")
                evs = [0, 0, 0, 0, 0, 0]
            else:
                evs_data = dict(evs_data)
                evs = list(evs_data.values())[1:]

            ivs_data = self.db_cur.execute("SELECT * FROM ivs WHERE cybermon = ?", (row["id"],)).fetchone()
            if ivs_data is None:
                print(f"Team {team_id} had corrupted IVs!")
                ivs = [0, 0, 0, 0, 0, 0]
            else:
                ivs_data = dict(ivs_data)
                ivs = list(ivs_data.values())[1:]

            cybermon = Cybermon(row["cybermon_id"])
            cybermon.configure(team_data, level=row["level"], nickname=row["nickname"], moves = moves, pending_moves = pending_moves, evs = evs, ivs = ivs)
            cybermon.db_id = row["id"]
            cybermon.hp = row["hp"]
            cybermon.xp = row["xp"]

            if row["in_use"] == 1:
                if active_cybermon[row["position"]] is None:
                    active_cybermon[row["position"]] = cybermon
                else:
                    team_data.pc_cybermon.append(cybermon)
            else:
                team_data.pc_cybermon.append(cybermon)
        
        for cybermon in active_cybermon:
            if (cybermon is not None):
                team_data.cybermon.append(cybermon)

        if len(team_data.cybermon) == 0:
            team_data.add_state(TeamState.NO_CYBERMON)

        for row in self.db_cur.execute("SELECT * FROM rewards_taken WHERE team=?", (team_data.id,)).fetchall():
            team_data.rewards_taken.append(row["challenge"])

        for row in self.db_cur.execute("SELECT * FROM team_blocks WHERE id=?", (team_data.id,)).fetchall():
            team_data.blocked_team_ids.append(row["team"])

        for row in self.db_cur.execute("SELECT * FROM items WHERE team=?", (team_data.id,)).fetchall():
            item = ItemData.get_item(ItemId(row["id"]))
            item["quantity"] = row["quantity"]
            
            team_data.items.append(item)

        team_data.notify_callback = self.notify_team_callback
        self.teams[team_id] = team_data
        return team_data

    def battle_timeout_clock_callback(self, battle):
        if battle.teams[0].id != None and battle.teams[1].id != None:
            for action in self.timed_actions:
                if action["type"] == "battle_timeout" and action["battle"] == battle:
                    self.timed_actions.remove(action)

            self.timed_actions.append({
                "timestamp": int(time.time()) + 5 * 60, "type": "battle_timeout", "battle": battle
            })

    async def battle_end_callback(self, winning_team, losing_team):
        winning_team.remove_state(TeamState.BATTLE)
        losing_team.remove_state(TeamState.BATTLE)

        # After a battle
        for cybermon in winning_team.cybermon + losing_team.cybermon:
            cybermon.stat_stages = [0, 0, 0, 0, 0, 0]
            cybermon.states = {}

        if winning_team.id is not None:
            self.db_save_team_cybermon(winning_team)
            self.db_save_team_items(winning_team.id)
            self.db_save_team_money(winning_team.id)

        if losing_team.id is not None:
            for cybermon in losing_team.cybermon:
                cybermon.revive()

            self.db_save_team_cybermon(losing_team)
            self.db_save_team_items(losing_team.id)
            self.db_save_team_money(losing_team.id)

            if winning_team.id is not None:
                revival_time = 60 # TODO: set to 30 * 60
            else:
                revival_time = 30
            
            losing_team.revival_timestamp = int(time.time()) + revival_time
            losing_team.add_state(TeamState.DEAD)
            
            def get_cybercenter_distance(coords, center_coords):
                return abs(center_coords["x"] - coords.x) + abs(center_coords["y"] - coords.y)

            cyber_centers = MapData.get_all_objects_of_type(MapObject.CYBER_CENTER)
            closest_center = cyber_centers[0]
            closest_center_distance = get_cybercenter_distance(losing_team.coords, closest_center)

            for center in cyber_centers:
                distance = get_cybercenter_distance(losing_team.coords, closest_center)
                if (distance <= closest_center_distance):
                    losing_team.coords.x = closest_center["x"]
                    losing_team.coords.y = closest_center["y"]
            
            losing_team.orientation = Orientation.SOUTH
            losing_team.remove_state(TeamState.MOVING)
            losing_team.movement_path = []

            self.db_save_team_coords(losing_team.id)

            self.timed_actions.append({
                "timestamp": int(time.time()) + revival_time, "type": "revive_team", "team": losing_team.id
            })

        winning_team.battle.teams = []
        winning_team.battle = None
        losing_team.battle = None

        if winning_team.id is not None and losing_team.id is not None:
            e_winning = 1 / (1 + 10**((losing_team.elo - winning_team.elo) / 400))
            e_losing = 1 / (1 + 10**((winning_team.elo - losing_team.elo) / 400))

            old_win_elo = winning_team.elo
            old_lose_elo = losing_team.elo

            winning_team.elo += 32 * (1 - e_winning)
            losing_team.elo += 32 * (0 - e_losing)

            winning_team.elo = int(round(winning_team.elo))
            losing_team.elo = int(round(losing_team.elo))

            winning_team.games += 1
            losing_team.games += 1
            
            self.db_save_team_elo(winning_team.id)
            self.db_save_team_elo(losing_team.id)

            if (self.notify_callback is not None):
                msg = f"**{winning_team.name}** won against **{losing_team.name}**!\n"
                msg += f"{winning_team.name}: {old_win_elo} -> {winning_team.elo} ({'+' if (winning_team.elo > old_win_elo) else '-'}{abs(winning_team.elo - old_win_elo)}) ELO!\n"
                msg += f"{losing_team.name}: {old_lose_elo} -> {losing_team.elo} ({'+' if (losing_team.elo > old_lose_elo) else '-'}{abs(losing_team.elo - old_lose_elo)}) ELO!\n"
                await self.notify_callback(msg)
        
        
    def db_save_team_cybermon(self, team):
        index = 0
        for cybermon in team.cybermon + team.pc_cybermon:
            print("Update in db:" + cybermon.name + " " + str(cybermon.level))
            in_use = cybermon in team.cybermon

            if cybermon.db_id == -1:
                self.db_cur.execute(
                    "INSERT INTO cybermon(team, cybermon_id, in_use, nickname, level, hp, xp, position) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                    (team.id, cybermon.id.value, in_use, cybermon.name, cybermon.level, cybermon.hp, cybermon.xp, index)
                )
                
                cybermon.db_id = self.db_cur.lastrowid
                
            else:
                self.db_cur.execute(
                    "UPDATE cybermon SET team = ?, cybermon_id = ?, nickname = ?, in_use = ?, level = ?, hp = ?, xp = ?, position = ? WHERE id = ?",
                    (team.id, cybermon.id.value, cybermon.name, in_use, cybermon.level, cybermon.hp, cybermon.xp, index, cybermon.db_id)
                )

            self.db_cur.execute("DELETE FROM moves WHERE cybermon_id = ?", (cybermon.db_id,))

            for move in cybermon.moves + cybermon.pending_moves:
                pending = 0
                if move in cybermon.pending_moves:
                    pending = 1

                self.db_cur.execute("INSERT INTO moves(cybermon_id, move_id, pp, pending) VALUES (?, ?, ?, ?)", (cybermon.db_id, move["id"].value, move["pp"], pending))
            
            self.db_cur.execute("DELETE FROM evs WHERE cybermon = ?", (cybermon.db_id,))
            self.db_cur.execute("DELETE FROM ivs WHERE cybermon = ?", (cybermon.db_id,))
            
            self.db_cur.execute("INSERT INTO evs(cybermon, ev_hp, ev_patk, ev_pdef, ev_satk, ev_sdef, ev_spd) VALUES (?, ?, ?, ?, ?, ?, ?)",
                (cybermon.db_id, cybermon.evs[0], cybermon.evs[1], cybermon.evs[2], cybermon.evs[3], cybermon.evs[4], cybermon.evs[5]))
            
            self.db_cur.execute("INSERT INTO ivs(cybermon, iv_hp, iv_patk, iv_pdef, iv_satk, iv_sdef, iv_spd) VALUES (?, ?, ?, ?, ?, ?, ?)",
                (cybermon.db_id, cybermon.ivs[0], cybermon.ivs[1], cybermon.ivs[2], cybermon.ivs[3], cybermon.ivs[4], cybermon.ivs[5]))
            
            index += 1
    

    def db_save_team_coords(self, team_id):
        team = self.get_team_data(team_id)

        self.db_cur.execute(
            "UPDATE teams SET x = ?, y = ? WHERE id = ?",
            (team.coords.x, team.coords.y, team.id)
        )

    def db_save_team_money(self, team_id):
        team = self.get_team_data(team_id)

        self.db_cur.execute(
            "UPDATE teams SET money = ? WHERE id = ?",
            (team.money, team.id)
        )

    def db_save_team_elo(self, team_id):
        team = self.get_team_data(team_id)

        self.db_cur.execute(
            "UPDATE teams SET elo = ?, games = ? WHERE id = ?",
            (team.elo, team.games, team.id)
        )

    def db_save_team_rewards(self, team_id):
        team = self.get_team_data(team_id)

        self.db_cur.execute("DELETE FROM rewards_taken WHERE team = ?", (team_id,))

        for challenge in team.rewards_taken:
            self.db_cur.execute("INSERT INTO rewards_taken VALUES(?, ?)", (team_id, challenge))

    def db_save_team_items(self, team_id):
        team = self.get_team_data(team_id)
        
        self.db_cur.execute("DELETE FROM items WHERE team = ?", (team_id,))
        
        for item in team.items:
            self.db_cur.execute("INSERT INTO items(team, id, quantity) VALUES(?, ?, ?)", (team_id, item["id"].value, item["quantity"]))

    def db_save_team_blocks(self, team_id):
        team = self.get_team_data(team_id)

        self.db_cur.execute("DELETE FROM team_blocks WHERE id = ?", (team_id,))

        for blocked_team in team.blocked_team_ids:
            self.db_cur.execute("INSERT INTO team_blocks VALUES(?, ?) ", (team_id, blocked_team))


    def start_moving_team(self, team_id, offset_x, offset_y, move_callback, encounter_callback):
        # offset_x = 0 and offset_y = 0 mean stop moving

        team = self.get_team_data(team_id)

        if type(team) != Team or not team.can_move():
            return GameError.TEAM_CANNOT_MOVE

        x = team.coords.x
        y = team.coords.y
        new_x = x + offset_x
        new_y = y + offset_y

        path = find_path(
            (x, y), (new_x, new_y), MapData.collision_zones, bypass_collisions = team.is_in_state(TeamState.NOCLIP)
        )

        if path == -1:
            return GameError.TEAM_MOVE_OUT_OF_MAP

        path = path[1:]
        mov_distance = len(path)

        if mov_distance < 6:
            mov_time = 2
        else:
            mov_time = 2 * (mov_distance - 5)

        team.movement_path = path
        team.add_state(TeamState.MOVING)
        self.movement_callbacks[team_id] = {"move_callback": move_callback, "encounter_callback": encounter_callback}

        return mov_time
    
    def challenge_team_to_battle(self, attacker_team_id, target_team_id):
        if (attacker_team_id == target_team_id):
            return GameError.TEAM_SELF_BATTLING

        attacker_team = self.get_team_data(attacker_team_id)
        target_team = self.get_team_data(target_team_id)

        if type(target_team) != Team or type(attacker_team) != Team:
            return GameError.TEAM_BATTLE_INVALID_TARGET

        if (not attacker_team.can_battle()) or (not target_team.can_battle()):
            return GameError.TEAM_CANNOT_BATTLE

        if attacker_team.coords != target_team.coords:
            return GameError.TEAM_NOT_SAME_COORDS

        if (attacker_team.id in target_team.blocked_team_ids):
            return GameError.TEAM_BLOCKED

        attacker_team.set_challenged(target_team)
        target_team.set_challenged(attacker_team)
        attacker_team.accepted_challenge = True

    def start_team_battle(self, attacker_team_id, target_team_id):
        attacker_team = self.get_team_data(attacker_team_id)
        target_team = self.get_team_data(target_team_id)
        
        attacker_team.clear_challenged()
        target_team.clear_challenged()

        attacker_team.add_state(TeamState.BATTLE)
        target_team.add_state(TeamState.BATTLE)

        battle = Battle(self, attacker_team, target_team)
        attacker_team.battle = battle
        target_team.battle = battle

    def get_teams_at_coords(self, x, y):
        teams_sql = self.db_cur.execute("SELECT * FROM teams WHERE x = ? AND y = ?", (x, y)).fetchall()
        teams = []
        for team in teams_sql:
            teams.append(self.get_team_data(team["id"]))
        
        return teams

    def get_all_teams_data(self):
        result = self.db_cur.execute("SELECT id FROM teams").fetchall()
        result_constructed = []
        for team_id in result:
            result_constructed.append(self.get_team_data(team_id[0]))

        return result_constructed
