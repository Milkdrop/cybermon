def reconstruct(came_from, current):
    c = current
    total_path = [c]
    while c in came_from.keys():
        c = came_from[c]
        total_path.append(c)

    return list(reversed(total_path))


def get_offsets(p, collision_map, bypass_collisions = False):
    new = []
    for offset in ((0, 1), (1, 0), (0, -1), (-1, 0)):
        new_offset = (p[0] + offset[0], p[1] + offset[1])
        if 0 <= new_offset[0] < collision_map[1][0]:
            if 0 <= new_offset[1] < collision_map[1][1]:
                if bypass_collisions or collision_map[0][new_offset[0], new_offset[1]][0] == 0:
                    new.append(new_offset)

    return new


def find_path(a, b, collision_map, bypass_collisions = False):
    # This is mostly taken from the Wikipedia A* article.
    okay = False
    if 0 <= b[0] < collision_map[1][0]:
        if 0 <= b[1] < collision_map[1][1]:
            okay = True

    if not okay or (collision_map[0][b[0], b[1]][0] != 0 and not bypass_collisions):
        return -1

    get_dist = lambda p: abs(b[0] - p[0]) + abs(b[1] - p[1])

    discovered_nodes = {a}
    came_from = {}
    g_score = {a: 0}
    f_score = {a: get_dist(a)}

    while len(discovered_nodes) > 0:
        current = min(discovered_nodes, key=get_dist)

        if current == b:
            return reconstruct(came_from, current)

        discovered_nodes.remove(current)
        for neighbour in get_offsets(current, collision_map, bypass_collisions):
            tentative_gscore = g_score[current] + 1
            if tentative_gscore < (g_score[neighbour] if neighbour in g_score else 9e99):
                came_from[neighbour] = current
                g_score[neighbour] = tentative_gscore
                f_score[neighbour] = g_score[neighbour] + get_dist(neighbour)
                if neighbour not in discovered_nodes:
                    discovered_nodes.add(neighbour)

    return -1
