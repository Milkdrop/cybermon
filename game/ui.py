import disnake
import os, time, random

from typing import List
from .cybermon import Cybermon
from .move import MoveData
from .map import MapData
from .enums import *
from .team import Team
from utils import Utils
import traceback

from PIL import Image, ImageDraw, ImageFont

class Ui:
    view_timeout = 60
    sprite_cache = {}
    image_server = "http://challs.xmas.htsp.ro:9000"

    @staticmethod
    def get_served_image(path):
        return Ui.image_server + "/" + path

    @staticmethod
    def render_map_for_team(all_teams: List[Team], viewer_team_id):
        viewer_x = 0
        viewer_y = 0

        viewer_team = None
        for team_data in all_teams:
            if team_data.id == viewer_team_id:
                viewer_x = team_data.coords.x
                viewer_y = team_data.coords.y

            if team_data.id == viewer_team_id:
                viewer_team = team_data

        map_window_size = (14, 14) # Will get cropped to center the image
        tile_size = (16, 16)

        map_bg_atlas = MapData.bg_atlas.crop(
            (tile_size[0] * (viewer_x - map_window_size[0] // 2), tile_size[1] * (viewer_y - map_window_size[1] // 2),
            tile_size[0] * (viewer_x + map_window_size[0] // 2), tile_size[1] * (viewer_y + map_window_size[1] // 2)
            )
        )

        map_fg_atlas = MapData.fg_atlas.crop(
            (tile_size[0] * (viewer_x - map_window_size[0] // 2), tile_size[1] * (viewer_y - map_window_size[1] // 2),
            tile_size[0] * (viewer_x + map_window_size[0] // 2), tile_size[1] * (viewer_y + map_window_size[1] // 2)
            )
        )

        map_image = Image.new('RGB', (tile_size[0] * map_window_size[0], tile_size[1] * map_window_size[1]))
        map_image.paste(map_bg_atlas, (0, 0))

        # path!
        if viewer_team.movement_path:
            draw = ImageDraw.Draw(map_image)
            line_points = [
                              (
                                  ((viewer_team.coords.x - viewer_x + map_window_size[0] // 2) * tile_size[0]) + (
                                              tile_size[0] // 2),
                                  ((viewer_team.coords.y - viewer_y + map_window_size[1] // 2) * tile_size[1]) + (
                                              tile_size[1] // 2)
                              )
                          ] + [
                              (
                                  ((t[0] - viewer_x + map_window_size[0] // 2) * tile_size[0]) + (tile_size[0] // 2),
                                  ((t[1] - viewer_y + map_window_size[1] // 2) * tile_size[1]) + (tile_size[1] // 2)
                              ) for t in viewer_team.movement_path
                          ]

            draw.line(
                line_points,
                fill="#000000", width=3
            )

            draw.line(
                line_points,
                fill="#00ff00", width=1
            )

            end = (viewer_team.movement_path[-1][0], viewer_team.movement_path[-1][1])
            draw.rectangle([
                ((end[0] - viewer_x + map_window_size[0] // 2) * tile_size[0]) + (tile_size[0] // 2) - 2,
                ((end[1] - viewer_y + map_window_size[1] // 2) * tile_size[1]) + (tile_size[1] // 2) - 2,
                ((end[0] - viewer_x + map_window_size[0] // 2) * tile_size[0]) + (tile_size[0] // 2) + 2,
                ((end[1] - viewer_y + map_window_size[1] // 2) * tile_size[1]) + (tile_size[1] // 2) + 2
            ], fill="#00ff00", outline="#000000", width=1)

        # player sprites
        visible_teams: List[Team] = []
        
        def paste_map_object(sprite, coords):
            map_image.paste(
                sprite, ((coords[0] - viewer_x + map_window_size[0] // 2) * tile_size[0] + 1,
                        (coords[1] - viewer_y + map_window_size[1] // 2) * tile_size[1] - 16),
                sprite  # sprite transparency mask is same as sprite
            )

        def is_map_object_visible(coords):
            if (coords[0] in range(viewer_x - map_window_size[0] // 2, viewer_x + map_window_size[0] // 2 + 1)
                and coords[1] in range(viewer_y - map_window_size[1] // 2, viewer_y + map_window_size[1] // 2 + 1)):
                return True
            else:
                return False

        for team in all_teams:
            if is_map_object_visible((team.coords.x, team.coords.y)):
                player_sprite_sheet_x = 0

                if (team.orientation == Orientation.WEST): player_sprite_sheet_x = 16
                if (team.orientation == Orientation.EAST): player_sprite_sheet_x = 32
                if (team.orientation == Orientation.NORTH): player_sprite_sheet_x = 48

                player_sprite = Ui.get_sprite("people/player.png", (player_sprite_sheet_x, 0), (16, 32))
                paste_map_object(player_sprite, (team.coords.x, team.coords.y))

                if team.id == viewer_team_id:
                    pass
                else:
                    visible_teams.append(team)

        visible_teams.append(viewer_team)
        
        # npcs
        for o in MapData.objects:
            if is_map_object_visible((o["x"], o["y"])):
                if o["type"] == MapObject.NPC:
                    sprite = Ui.get_sprite(f"people/{o['sprite']}.png", (random.choice([0, 16, 32, 48]), 0), (16, 32))
                    paste_map_object(sprite, (o["x"], o["y"]))

        map_image.paste(map_fg_atlas, (0, 0), map_fg_atlas)

        resize_factor = 3
        map_image = map_image.resize(
            (map_image.size[0] * resize_factor, map_image.size[1] * resize_factor), Image.NEAREST
        )

        # player names
        name_coords = {}
        font = Utils.get_path("assets/fonts/MS-Gothic-01.ttf")
        draw = ImageDraw.Draw(map_image)
        draw.fontmode = "1"  # No aliasing, pixelated font

        font = ImageFont.truetype(font, 22)

        for team in visible_teams:
            x = (team.coords.x - viewer_x + map_window_size[0] // 2) * tile_size[0] * resize_factor
            y = (team.coords.y - viewer_y + map_window_size[1] // 2) * tile_size[1] * resize_factor

            if (x, y) in name_coords:
                name_coords[(x, y)] += 1
            else:
                name_coords[(x, y)] = 0

            if team.coords.x == viewer_x and team.coords.y == viewer_y:
                viewer_offset = name_coords[(x, y)] + 1
                if team.id == viewer_team_id:
                    viewer_offset = 0
            else:
                viewer_offset = name_coords[(x, y)]

            y -= viewer_offset * 24

            text = team.name if len(team.name) < 16 else team.name[:13] + "..."

            text_colour = "white" if team.id != viewer_team_id else "#00FF00"
            if not team.can_battle():
                text_colour = "#aaaaaa" if team.id != viewer_team_id else "#00cc00"

            text_w, text_h = draw.textsize(text, font)

            x -= int(text_w / 2) - ((tile_size[0] // 2) * resize_factor)
            y -= int(text_h / 2) - ((tile_size[1] // 2) * resize_factor) + (20 * resize_factor)

            draw.text((x, y), text, font=font, fill=text_colour, stroke_width=2, stroke_fill="black")
        
        print(viewer_team.states)
        if viewer_team.is_in_state(TeamState.NOCLIP):
            print("noclip")
            draw.text((64, 64), "OUTSIDE", font=font, fill="#FF0000", stroke_width=2, stroke_fill="black")

        # center image on player if we have even coordinates
        if map_window_size[0] % 2 == 0 and map_window_size[1] % 2 == 0:
            map_image = map_image.crop(
                (
                    tile_size[0] * resize_factor, tile_size[1] * resize_factor,
                    (tile_size[0] * map_window_size[0]) * resize_factor,
                    (tile_size[1] * map_window_size[1]) * resize_factor
                )
            )
        
        image_file = Utils.get_path(f"served_images/map/{viewer_team_id}.png")
        map_image.save(image_file)

        return (Ui.image_server + f"/map/{viewer_team_id}.png?t=" + str(int(time.time()*1000)))

    @staticmethod
    def render_battle(attacker_team):
        target_team = attacker_team.battle.get_opponent_team(attacker_team.id)
        
        bg = Ui.get_sprite("battle/bg/1.png")
        bg_shadows = Ui.get_sprite("battle/bg_shadows.png")

        battle_image = Image.new("RGBA", bg.size)
        battle_image.paste(bg, (0, 0))
        battle_image.paste(bg_shadows, (0, 0), bg_shadows)
        
        attacker_cybermon = attacker_team.get_active_cybermon()
        target_cybermon = target_team.get_active_cybermon()
        
        a_sprite_path = f"cybermon/{attacker_cybermon.id.value}.png"
        t_sprite_path = f"cybermon/{target_cybermon.id.value}.png"

        if "glitched_sprite_path" in attacker_cybermon.states: a_sprite_path = attacker_cybermon.states["glitched_sprite_path"]
        if "glitched_sprite_path" in target_cybermon.states: sprite_path = attacker_cybermon.states["glitched_sprite_path"]

        attacker_sprite = Ui.get_sprite(a_sprite_path).copy().resize((212, 212), Image.BICUBIC).transpose(Image.FLIP_LEFT_RIGHT)
        target_sprite = Ui.get_sprite(t_sprite_path).copy().resize((160, 160), Image.BICUBIC)

        battle_image.paste(attacker_sprite, (52, 69, 52 + 212, 69 + 212), attacker_sprite)
        battle_image.paste(target_sprite, (284, 10, 284 + 160, 10 + 160), target_sprite)
    
        overlay = Ui.get_sprite("battle/overlay.png")

        battle_image.paste(overlay, (0, 0), overlay)
        
        font = Utils.get_path("assets/fonts/MS-UIGothic-02.ttf")
        font_cybermon = ImageFont.truetype(font, 20)
        font_team = ImageFont.truetype(font, 16)
        font_health = ImageFont.truetype(font, 12)

        draw = ImageDraw.Draw(battle_image)
        draw.fontmode = "1" # No aliasing, pixelated font
        text_colour = "#252422"

        def cut_name(name, max_w, font = font_cybermon):
            text_w, text_h = draw.textsize(name, font)
            out_name = name

            while (text_w >= max_w):
                name = name[:-1]
                out_name = name + "..."

                text_w, text_h = draw.textsize(out_name, font)
            
            return out_name

        def draw_status_circles(pivot, cybermon):
            for i in range(6):
                circle = Ui.get_sprite("battle/status_circles.png", (20, 0), (8, 8))
                
                if (i < len(cybermon)):
                    if (cybermon[i].hp > 0):
                        circle = Ui.get_sprite("battle/status_circles.png", (0, 0), (8, 8))
                    else:
                        circle = Ui.get_sprite("battle/status_circles.png", (10, 0), (8, 8))
                
                battle_image.paste(circle, (pivot[0] + 10 * i, pivot[1], pivot[0] + 10 * i + 8, pivot[1] + 8), circle)

        target_health_width = round(127 * (target_cybermon.hp / target_cybermon.stats.max_hp))
        draw.text((7, 38), cut_name(target_cybermon.name, 148), font=font_cybermon, fill=text_colour)
        draw.text((156, 38), "Lv." + str(target_cybermon.level), font=font_cybermon, fill=text_colour)
        draw.rectangle([(70, 66), (70 + target_health_width, 66 + 9)], fill="#E06552")
        draw.rectangle([(70, 66), (70 + target_health_width, 66 + 1)], fill="#A54B3D")

        draw.text((6, 14), cut_name(target_team.name, 210), font=font_team, fill=text_colour, stroke_width=2, stroke_fill="white")
        draw_status_circles((2, 91), target_team.cybermon)
        
        attacker_health_width = round(127 * (attacker_cybermon.hp / attacker_cybermon.stats.max_hp))
        draw.text((309, 199), cut_name(attacker_cybermon.name, 148), font=font_cybermon, fill=text_colour)
        draw.text((456, 199), "Lv." + str(attacker_cybermon.level), font=font_cybermon, fill=text_colour)
        draw.rectangle([(372, 226), (372 + attacker_health_width, 226 + 9)], fill="#E06552")
        draw.rectangle([(372, 226), (372 + attacker_health_width, 226 + 1)], fill="#A54B3D")

        attacker_xp_width = round(124 * (attacker_cybermon.xp / attacker_cybermon.xp_to_level_up))
        draw.rectangle([(321, 257), (321 + attacker_xp_width, 257 + 3)], fill="#44A1A0")
        draw.rectangle([(321, 257), (321 + attacker_xp_width, 257 + 1)], fill="#357A78")

        attacker_hp_text = f"{attacker_cybermon.hp}/{attacker_cybermon.stats.max_hp}"
        text_w, text_h = draw.textsize(attacker_hp_text, font_health)
        draw.text((504 - text_w, 239), attacker_hp_text, font=font_health, fill=text_colour)

        draw_status_circles((452, 257), attacker_team.cybermon)

        attacker_name = cut_name(attacker_team.name, 210, font_team)
        text_w, text_h = draw.textsize(attacker_name, font_team)

        draw.text((508 - text_w, 175), attacker_name, font=font_team, fill=text_colour, stroke_width=2, stroke_fill="white")
        
        if attacker_cybermon.status != CybermonStatus.NONE:
            status_sprite = Ui.get_sprite(f"battle/status/{attacker_cybermon.status.name}.png")
            battle_image.paste(status_sprite, (310, 223, 310 + 32, 223 + 16), status_sprite)

        if target_cybermon.status != CybermonStatus.NONE:
            status_sprite = Ui.get_sprite(f"battle/status/{target_cybermon.status.name}.png")
            battle_image.paste(status_sprite, (8, 63, 8 + 32, 63 + 16), status_sprite)

        image_file = Utils.get_path(f"served_images/battle/{attacker_team.id}.png")
        battle_image = battle_image.resize((battle_image.size[0] * 2, battle_image.size[1] * 2))
        battle_image.save(image_file)

        return (Ui.image_server + f"/battle/{attacker_team.id}.png?t=" + str(int(time.time()*1000)))

    @staticmethod
    def get_sprite(sprite_sheet, coords = None, size = None):
        if sprite_sheet not in Ui.sprite_cache:
            Ui.sprite_cache[sprite_sheet] = (
                Image.open(Utils.get_path("assets/" + sprite_sheet)), {}
            )  # Image file, cropped sprites cache
        
        if coords is not None:
            if coords not in Ui.sprite_cache[sprite_sheet][1]:
                # coords should come with size
                Ui.sprite_cache[sprite_sheet][1][coords] = Ui.sprite_cache[sprite_sheet][0].crop(
                    (coords[0], coords[1], coords[0] + size[0], coords[1] + size[1])
                )

            return Ui.sprite_cache[sprite_sheet][1][coords]
        else:
            return Ui.sprite_cache[sprite_sheet][0]

    class Button(disnake.ui.Button):
        def __init__(self, callback, author_id = None, label = ' ', emoji = None, style = disnake.ButtonStyle.secondary, disabled = False, row = None):
            super().__init__(label = label, emoji = emoji, style = style, disabled = disabled, row = row) # kwargs ?
            self.async_callback = callback
            self.author_id = author_id

        async def callback(self, ctx: disnake.MessageInteraction):
            if (self.author_id is not None and ctx.author.id != self.author_id): return
            await self.async_callback(ctx)

    class Select(disnake.ui.Select):
        def __init__(self, callback, author_id = None, options = [], placeholder = ' ', no_options_title = 'No options!', no_options_description = '', disabled = False, row = None):
            options = options[:25]
            ui_options = [None] * len(options)
            for i in range(len(options)):
                if "emoji" not in options[i]:
                    options[i]["emoji"] = None

                if "description" not in options[i]:
                    options[i]["description"] = None
                else:
                    options[i]["description"] = options[i]["description"][:100]

                ui_options[i] = disnake.SelectOption(label=f"#{i + 1}. " + options[i]["title"], description=options[i]["description"], emoji=options[i]["emoji"])

            self.external_options = options
            self.no_options_title = no_options_title
            
            if len(ui_options) == 0:
                ui_options.append(disnake.SelectOption(label = no_options_title, description = no_options_description, emoji = '\u274C'))

            super().__init__(placeholder = placeholder, min_values = 1, max_values = 1, options = ui_options, disabled = disabled, row = row) # kwargs ?
            self.async_callback = callback
            self.author_id = author_id

        async def callback(self, ctx: disnake.MessageInteraction):
            if (self.author_id is not None and ctx.author.id != self.author_id): return
            if (self.values[0] == self.no_options_title): return await ctx.response.send_message(embed = Ui.Embed("Error", "That's an invalid choice"), ephemeral = True)

            choice_index = int(self.values[0][1:self.values[0].find(".")])

            for i in range(len(self.external_options)):
                if (i + 1 == choice_index):
                    await self.async_callback(ctx, self.external_options[i])

    class Embed(disnake.Embed):
        def __init__(self, title, description, image = None):
            super().__init__(title = title, description = description, color = 14706002)
            if (image is not None):
                self.set_image(url = image)

    class GenericSelectView(disnake.ui.View):
        def __init__(self, author_id, options, callback, placeholder = "Pick option ..."):
            super().__init__(timeout = Ui.view_timeout)

            self.add_item(Ui.Select(author_id=author_id, callback=callback, options=options, placeholder=placeholder))
                                    
    class MapView(disnake.ui.View):
        def __init__(self, author_id, move_callback):
            super().__init__(timeout = Ui.view_timeout)

            async def nothing(ctx): pass
            async def move_up(ctx): await move_callback(ctx, 0, -1)
            async def move_left(ctx): await move_callback(ctx, -1, 0)
            async def move_down(ctx): await move_callback(ctx, 0, 1)
            async def move_right(ctx): await move_callback(ctx, 1, 0)

            async def fast_move_up(ctx): await move_callback(ctx, 0, -6)
            async def fast_move_left(ctx): await move_callback(ctx, -6, 0)
            async def fast_move_down(ctx): await move_callback(ctx, 0, 6)
            async def fast_move_right(ctx): await move_callback(ctx, 6, 0)

            self.add_item(Ui.Button(author_id = author_id, callback = fast_move_up, label = ' ', emoji = '\u23EB', row = 1))
            self.add_item(Ui.Button(author_id = author_id, callback = fast_move_down, label = ' ', emoji = '\u23EC', row = 1))
            self.add_item(Ui.Button(author_id = author_id, callback = move_up, label = ' ', emoji = '\U0001f53c', row = 1))
            self.add_item(Ui.Button(author_id = author_id, callback = move_down, label = ' ', emoji = '\U0001f53d', row = 1))

            self.add_item(Ui.Button(author_id = author_id, callback = fast_move_left, label = ' ', emoji = '\u23EA', row = 2))
            self.add_item(Ui.Button(author_id = author_id, callback = fast_move_right, label = ' ', emoji = '\u23E9', row = 2))
            self.add_item(Ui.Button(author_id = author_id, callback = move_left, label = ' ', emoji = '\u25c0', row = 2))
            self.add_item(Ui.Button(author_id = author_id, callback = move_right, label = ' ', emoji = '\u25b6', row = 2))
    
    class NewChallengeView(disnake.ui.View):
        def __init__(self, author_id, attacker_team_id, teams, callback):
            super().__init__(timeout = Ui.view_timeout)

            async def challenge_team(ctx, team_option):
                await callback(ctx, team_option["id"])
                
            team_options = []

            for team in teams:
                if team.can_battle():
                    if (attacker_team_id in team.blocked_team_ids):
                        team_options.append({"id": team.id, "title": team.name, "description": "This team has blocked you from challenging them.", 'emoji': '\U0001F6AB'})
                    else:
                        team_options.append({"id": team.id, "title": team.name, "emoji": '\u2694'})
                else:
                    description="Team is already in a battle, recovering, or is moving to another location"
                    team_options.append({"id": team.id, "title": team.name, "description": description, "emoji": '\u274C'})

            print(team_options)
            self.add_item(Ui.Select(author_id = author_id, callback = challenge_team, options = team_options, placeholder = "Challenge team ..."))

    class ChallengeView(disnake.ui.View):
        def __init__(self, author_id, accept_callback, reject_callback, block_callback, accepted):
            super().__init__(timeout = Ui.view_timeout)

            if not accepted: self.add_item(Ui.Button(author_id = author_id, callback = accept_callback, label = 'ACCEPT', style = disnake.ButtonStyle.primary, emoji = '\u2705', row = 1))
            self.add_item(Ui.Button(author_id = author_id, callback = reject_callback, label = 'CANCEL', emoji = '\u274C', row = 1))
            if not accepted: self.add_item(Ui.Button(author_id = author_id, callback = block_callback, label = 'BLOCK', style = disnake.ButtonStyle.red, emoji = '\U0001F480', row = 1))

    class BattleUi:
        def __init__(self, author_id, attacker_team, original_ctx):
            self.author_id = author_id
            self.attacker_team = attacker_team
            self.original_ctx = original_ctx

        async def refresh(self, ctx = None, new_message = False):
            if ctx is None:
                ctx = self.original_ctx

            function = ctx.response.edit_message

            if (new_message):
                function = ctx.response.send_message

            if (ctx.response.is_done()):
                function = ctx.edit_original_message
                print("response already done")
            
            try:
                await function(
                    embed = Ui.BattleUi.BattleEmbed(self.attacker_team),
                    view = Ui.BattleUi.BattleView(self, self.attacker_team)
                )
            except Exception as e:
                traceback.print_exc()

        async def set_message(self, ctx, message):
            if ctx is None:
                ctx = self.original_ctx

            function = ctx.response.edit_message

            if (ctx.response.is_done()):
                function = ctx.edit_original_message
                
            await function(embed = Ui.Embed("Info", message), view = None)


        class BattleView(disnake.ui.View):
            def __init__(self, main_ui, attacker_team):
                super().__init__(timeout = Ui.view_timeout)

                if (attacker_team.battle is None):
                    print("Attacker team's battle object is None")
                    return

                target_team = attacker_team.battle.get_opponent_team(attacker_team.id)
                if (type(target_team) != Team):
                    print("Type of target_team in UI battle is not Team!")
                    return

                attacker_cybermon = attacker_team.get_active_cybermon()
                target_cybermon = target_team.get_active_cybermon()
                if (attacker_cybermon == None or target_cybermon == None):
                    return
                
                attack_options = []
                
                if len(attacker_cybermon.moves) == 0:
                    attacker_cybermon.moves.append(MoveData.get_move(MoveId.NTURN_))

                for move in attacker_cybermon.moves:
                    if move["pp"] > 0:
                        attack_options.append({"move": move, "title": move["name"] + f" (PP: {move['pp']}/{move['max_pp']})", "description": move["description"], "emoji": move["emoji"]})

                if len(attack_options) == 0:
                    move = MoveData.get_move(MoveId.STRUGGLE)
                    attack_options.append({"move": move, "title": move["name"] + " (PP: INFINITE)", "description": move["description"], "emoji": move["emoji"]})

                async def use_attack(ctx, attack_option):
                    await attacker_team.battle.make_move(attacker_team, target_team, attack_option["move"], ctx)

                self.add_item(Ui.Select(author_id = main_ui.author_id, callback = use_attack, options = attack_options, placeholder = "Attack with ...", row = 1))

                async def load_bag(ctx):
                    await ctx.response.edit_message(view = Ui.BattleUi.BattleBagView(main_ui, attacker_team))

                self.add_item(Ui.Button(author_id = main_ui.author_id, callback = load_bag, label = 'BAG', emoji = '\U0001F392', row = 2, disabled = (len(attacker_team.items) == 0)))

                async def swap_cybermon(ctx):
                    await ctx.response.edit_message(view = Ui.BattleUi.BattleCybermonView(main_ui, attacker_team))

                alive_cybermon = []
                for cybermon in attacker_team.cybermon:
                    if cybermon.hp > 0:
                        alive_cybermon.append(cybermon)

                self.add_item(Ui.Button(author_id = main_ui.author_id, callback = swap_cybermon, label = 'CYBERMON', emoji = '\U0001F300', row = 2, disabled = (len(alive_cybermon) == 1)))

                async def run_away(ctx):
                    await attacker_team.battle.run_away(attacker_team, target_team, ctx)

                self.add_item(Ui.Button(author_id = main_ui.author_id, callback = run_away, label = 'RUN', emoji = '\U0001F4A8', style = disnake.ButtonStyle.red, disabled = (target_team.id is not None or len(target_team.name) > 0), row = 2))
        
        class BattleEmbed(disnake.Embed):
            def __init__(self, attacker_team):
                if (attacker_team.battle is None):
                    print("Attacker team's battle object is None")
                    return

                target_team = attacker_team.battle.get_opponent_team(attacker_team.id)

                if (type(target_team) != Team):
                    print("Type of target_team in UI battle is not Team!")
                    return

                attacker_cybermon = attacker_team.get_active_cybermon()
                target_cybermon = target_team.get_active_cybermon()

                description = ""

                battle_log = attacker_team.battle.battle_log
                if len(battle_log) > 0:
                    description = "\nBattle log:```" + '\n'.join(battle_log.split("\n")[-10:]) + "```"

                if (attacker_cybermon == None):
                    description += "\n**You lost!** You have been sent to the nearest Cyber center to heal your cybermon. You will need to wait for a while before you can do actions again."
                elif (target_cybermon == None):
                    description += "\n**You won!**"

                super().__init__(title = "Fight!", description = description, color = 14706002)

                if (attacker_cybermon is not None and target_cybermon is not None):
                    self.set_image(url = Ui.render_battle(attacker_team))

        class BattleBagView(disnake.ui.View):
            def __init__(self, main_ui, attacker_team):
                super().__init__(timeout = Ui.view_timeout)

                if (attacker_team.battle is None):
                    print("Attacker team's battle object is None")
                    return

                target_team = attacker_team.battle.get_opponent_team(attacker_team.id)

                if (type(target_team) != Team):
                    print("Type of target_team in UI battle is not Team!")
                    return

                item_options = []

                for item in attacker_team.items:
                    if ((target_team.id is None and ItemUsability.BATTLE_WILD in item["usability"])
                        or (target_team.id is not None and ItemUsability.BATTLE_TEAM in item["usability"])):
                        item_options.append({"item": item, "quantity": item["quantity"], "title": f"{item['name']} (x{item['quantity']})", "description": item["description"], "emoji": item['emoji']})

                async def use_item(ctx, item_option):
                    await attacker_team.battle.use_item(attacker_team, target_team, item_option["item"], ctx)

                self.add_item(Ui.Select(author_id = main_ui.author_id, callback = use_item, options = item_options, placeholder = "Use item ...", row = 1))
                async def btn_back(ctx): await main_ui.refresh(ctx = ctx)
                self.add_item(Ui.Button(author_id = main_ui.author_id, callback = btn_back, label = 'BACK', emoji = '\u21AA', row = 2))

        class BattleCybermonView(disnake.ui.View):
            def __init__(self, main_ui, attacker_team):
                super().__init__(timeout = Ui.view_timeout)

                if (attacker_team.battle is None):
                    print("Attacker team's battle object is None")
                    return

                target_team = attacker_team.battle.get_opponent_team(attacker_team.id)

                if (type(target_team) != Team):
                    print("Type of target_team in UI battle is not Team!")
                    return

                cybermon_options = []

                for i, cybermon in enumerate(attacker_team.cybermon):
                    if cybermon.hp > 0 and cybermon is not attacker_team.get_active_cybermon():
                        description = f"HP: {cybermon.hp}/{cybermon.stats.max_hp}, XP: {cybermon.xp}/{cybermon.xp_to_level_up}"
                        cybermon_options.append({"cybermon": cybermon, "cybermon_index": i, "title": cybermon.name + f" (LV. {cybermon.level})", "description": description})

                async def swap_cybermon(ctx, cybermon_option):
                    await attacker_team.battle.swap_cybermon(attacker_team, target_team, cybermon_option["cybermon_index"], ctx)

                self.add_item(Ui.Select(author_id = main_ui.author_id, callback = swap_cybermon, options = cybermon_options, placeholder = "Swap cybermon ...", row = 1))
                async def btn_back(ctx): await main_ui.refresh(ctx = ctx)
                self.add_item(Ui.Button(author_id = main_ui.author_id, callback = btn_back, label = 'BACK', emoji = '\u21AA', row = 2))