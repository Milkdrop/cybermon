import disnake
import os
import aiohttp
import time
import json
import asyncio
import traceback

from disnake.ext import commands
from utils import Utils
from game.game import Game
from game.cybermon import Cybermon
from game.enums import *
from game.map import MapData
from game.ui import Ui
from game.item import ItemData
from game.move import MoveData
from game.team import Team
from game.battle import Battle

bot = commands.Bot(command_prefix = "cyb!")#, test_guilds = [519974854485737483])
bot.remove_command('help')

MapData.load_map_data(Utils.get_path("assets/map"))
MapData.load_objects()

game = Game(Utils.get_path("game.db"))

ctfx_api_endpoint = "https://xmas.htsp.ro"
announcement_channel = 919590914853331004

@bot.event
async def on_ready():
    print(bot.user.name + " powered up!")
    await bot.change_presence(activity = disnake.Game(name = "The discord rate-limit is both a blessing and a curse. /help"))

async def notify_team(team_data, message):
    for member in team_data.members:
        user = bot.get_user(member)
        if user is None:
            user = await bot.fetch_user(member)
            print("User is not in cache: " + str(member))
        
        await user.send(embed = Ui.Embed("Info", message))

        if len(team_data.members) > 1:
            await asyncio.sleep(2)

async def notify(message):
    try:
        ch = await bot.fetch_channel(announcement_channel)
        await ch.send(embed = Ui.Embed("Info", message))
    except Exception as e:
        traceback.print_exc()

game.notify_team_callback = notify_team
game.notify_callback = notify

async def send_error(ctx, message):
    try:
        await ctx.response.send_message(embed = Ui.Embed("Error", message), ephemeral = True)
    except Exception as e:
        traceback.print_exc()

async def get_user_team_data(ctx):
    team_data = game.get_user_team_data(ctx.author.id)

    if (type(team_data) == GameError):
        error = "Unknown error"
        if (team_data == GameError.USER_NOT_IN_TEAM): error = "You are not in a team. Please login into your CTFx team by using `/login`."
        elif (team_data == GameError.TEAM_DATA_NULL): error = "There was a problem getting your team's data, please contact an organizer."

        await send_error(ctx, error)
        return None
    
    return team_data

# Smarter management of sessions is needed, making a session per request is bad
async def get_ctfx_api_data(query):
    output = None

    async with aiohttp.ClientSession() as session:
        async with session.get(ctfx_api_endpoint + '/api' + query) as response:
            output = json.loads(await response.text())
    
    return output

def get_team_embed(team_data, own_team = True):
    description = f"**Team ID:** {team_data.id}\n"
    description += "**Members:** " + ", ".join([f"<@{team_id}>" for team_id in team_data.members]) + "\n"
    description += f"**Coordinates:** X = {team_data.coords.x}, Y = {team_data.coords.y}\n"
    description += f"**Money:** **{team_data.money}** bits\n"
    description += f"**ELO:** **{team_data.elo}** ({team_data.games} matches played)\n"

    if own_team and len(team_data.blocked_team_ids) > 0:
        description += f"**Blocked team IDs:** " + ", ".join([str(team_id) for team_id in team_data.blocked_team_ids]) + "\n"

    pending_moves = 0
    for c in team_data.cybermon:
        pending_moves += len(c.pending_moves)
    
    if own_team:
        if team_data.is_in_state(TeamState.NO_CYBERMON):
            description += ":x: **No cybermon!** Pick a starter with `/pick-starter`!\n"
        if team_data.is_in_state(TeamState.DEAD):
            description += f":skull:  You recently lost a battle. Your cybermon will recover in: **{team_data.revival_timestamp - int(time.time())} seconds**.\n"
        if team_data.is_in_state(TeamState.BATTLE):
            description += f":warning:  You are currently in a battle! Use `/battle` to control what is going on\n"
        if team_data.is_in_state(TeamState.CHALLENGED):
            description += f":warning:  You have been challenged to a battle by another team! Use `/view-challenge` to respond to the challenge\n"
        if (pending_moves > 0):
            description += f":high_brightness:  Some of your cybermon have moves to learn! Replace some of their current moves with `/teach-move`\n"
        if team_data.is_in_state(TeamState.MOVING):
            description += f":person_walking:  You are currently moving to a new location\n"

        #description += "Your cybermon are below. The stats order is: HP/PATK/PDEF/SATK/SDEF/SPD\n"

    if (team_data.country_code == "wo"):
        flag_emoji = ":earth_americas:"
    else:
        flag_emoji = f":flag_{team_data.country_code}:"

    team_embed = disnake.Embed(title=f"{team_data.name} ({flag_emoji})", description=description, color=14706002)  # colour of embed as int (orange)
    team_embed.set_thumbnail(url = team_data.avatar_url)

    if own_team:
        for i in range(len(team_data.cybermon)):
            cybermon = team_data.cybermon[i]
            
            field_title = f"#{i}. {cybermon.name} (LV. {cybermon.level})"

            if cybermon.hp <= 0:
                field_title += " :skull:"

            field_text = f">>> HP: **{cybermon.hp}/{cybermon.stats.max_hp}**, XP: **{cybermon.xp}/{cybermon.xp_to_level_up}**\n"
            #field_text += f"\nStats:\n**{cybermon.stats.max_hp}**/**{cybermon.stats.patk}**/**{cybermon.stats.pdef}**/**{cybermon.stats.satk}**/**{cybermon.stats.sdef}**/**{cybermon.stats.spd}**\n"
            field_text += f"Moves:\n" + "\n".join([f"#{i}. **{move['name']}** (PP: {move['pp']}/{move['max_pp']})" for i, move in enumerate(cybermon.moves)]) + "\n"

            if len(cybermon.pending_moves) > 0:
                field_text += f"\n:high_brightness: **MOVES TO LEARN:** " + ", ".join([f"#{i}. {move['name']}" for i, move in enumerate(cybermon.pending_moves)]) + "\n"
            
            team_embed.add_field(field_title, field_text[:1024], inline = True)
            
            if i % 2 == 1:
                team_embed.add_field("\u200c", "\u200c", inline = True)

    if len(team_data.pc_cybermon) > 0 and own_team:
        team_embed.add_field("Stored cybermon", ", ".join([f"#{i + 6}. {cybermon.name}" for i, cybermon in enumerate(team_data.pc_cybermon)])[:1024], inline=False)

    if len(team_data.items) > 0 and own_team:
        team_embed.add_field("Items", ", ".join([f"{item['name']} (x{item['quantity']})" for item in team_data.items])[:1024], inline=False)

    return team_embed

@bot.slash_command(name="help")
async def help(ctx):
    description = "**To get started, first login via** `/login ctfx_email ctfx_password`.\n"
    description += "You can use `/myteam` to see your team\n"
    description += "There are various /view-* commands to see more details about what you have.\n"
    description += "\n"

    description += "First of all, you must **choose a starter** by using `/pick-starter`.\n"
    description += "_(The more warmups on CTFx you solve, the more starter options you will have)_\n"
    description += "\n"

    description += "Solving challenges in the CTF grants you **item rewards.** Use `/rewards` to see what you can get.\n"
    description += "\n"

    description += "You can **move around** using `/map` or `/move x_offset y_offset`.\n"
    description += "If you walk in tall grass there's a chance of a **wild encounter**!\n"
    description += "\n"

    description += "You can see and control what battle you are in by using `/battle`.\n"
    description += "You can **challenge another player** to a battle by being at the exact same coordinates with them, and then using `/new-challenge`. If they accept, you can battle!\n"
    description += "\n"
    description += "\n"
    description += "Credits:\n"
    description += "- https://phoenixdex.alteredorigin.net/ for all \"Cybermon\"\n"
    description += "- https://www.deviantart.com/aveontrainer for all overworld sprites\n"
    description += "- https://www.deviantart.com/magiscarf for the tileset and battle backgrounds\n"
    description += "- https://web.archive.org/web/20170726190716/http://whitecafe.sakura.ne.jp/info.html and http://neko.moo.jp/BS/ for item sprites\n"

    embed = Ui.Embed("Welcome to Cybermon!", description)
    embed.set_thumbnail(url = "https://cdn.discordapp.com/avatars/899306507802394655/c87e16aa6e3d950fdc8f4b01171604bc.webp?size=1024")

    await ctx.response.send_message(embed = embed)

@bot.slash_command(name="map", description = "Show where you are, and some controls to move")
async def map(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return

    async def move_callback(ctx, x, y):
        # There's a ton of copypasting here because you need to re-call the map render function inside every callback, otherwise you get outdated outputs
        async def finished_moving():
            try:
                if (not ctx.response.is_done()):
                   await ctx.response.edit_message(content = Ui.render_map_for_team(game.get_all_teams_data(), team_data.id), embed = None)
                else:
                    if timeout > 2:
                        await ctx.edit_original_message(content = Ui.render_map_for_team(game.get_all_teams_data(), team_data.id), embed = None)

            except Exception as e:
                traceback.print_exc()

        async def encounter_callback(custom_message = None):
            try:
                if custom_message is None:
                    message = f"A wild cybermon appeared!"
                else:
                    message = custom_message

                message += "\nUse `/battle` to fight!"

                if (not ctx.response.is_done()):
                    await ctx.response.edit_message(content = "", embed = Ui.Embed("Info", message), view = None)
                else:
                    await ctx.edit_original_message(content = "", embed = Ui.Embed("Info", message), view = None)

            except Exception as e:
                traceback.print_exc()
        
        moving = False
        max_iter = 6

        while not moving and max_iter > 0 and (x != 0 or y != 0):
            timeout = game.start_moving_team(team_data.id, x, y, finished_moving, encounter_callback)

            if type(timeout) != GameError:
                moving = True
            else:
                if x > 0: x -= 1
                if x < 0: x += 1

                if y > 0: y -= 1
                if y < 0: y += 1

            max_iter -= 1

        if type(timeout) == GameError:
            error = "Unknown error."
            if (timeout == GameError.TEAM_MOVE_OUT_OF_MAP): error = "You can't move there!"
            elif (timeout == GameError.TEAM_CANNOT_MOVE): error = "You can't move! Use `/myteam` to see what's happening."

            await ctx.response.edit_message(embed = Ui.Embed("Error", error))

        elif timeout > 2:
            try:
                await ctx.response.edit_message(embed = Ui.Embed("Map", f"You will arrive in {timeout} seconds.", image =  Ui.render_map_for_team(game.get_all_teams_data(), team_data.id)))
            except Exception as e:
                traceback.print_exc()

    await ctx.response.send_message(Ui.render_map_for_team(game.get_all_teams_data(), team_data.id), view = Ui.MapView(ctx.author.id, move_callback))
    

@bot.slash_command(name="myteam", description = "Show details about your team.")
async def myteam(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return
        
    await ctx.response.send_message(embed = get_team_embed(team_data, own_team = True))

@bot.slash_command(name="leaderboard", description = "See top teams ranked by ELO.")
async def leaderboard(ctx):
    teams = game.get_all_teams_data()

    msg = ""
    i = 0
    for t in sorted(teams, key=lambda x: -x.elo):
        if t.games <= 0:
            continue

        if (t.country_code == "wo"): flag_emoji = ":earth_americas:"
        else: flag_emoji = f":flag_{t.country_code}:"

        msg += f"#{i + 1}. **{t.elo}** ELO: **{t.name}** ({flag_emoji}) - ({t.games} matches)\n"

        i += 1
        if (i >= 16):
            break
    
    if len(msg) == 0:
        msg = "No teams."

    await ctx.response.send_message(embed = Ui.Embed("Leaderboard", msg))

@bot.slash_command(name="sell", description = "Sell items if you are at a shop's door")
async def sell(ctx,
    item_name: str = commands.Param(name="item_name", description="The exact name of the item you want to sell"),
    quantity: int = commands.Param(name="quantity", description="How much of the item do you want to sell"),
):
    team = await get_user_team_data(ctx)
    if team is None:
        return
    
    if not team.can_use_map_objects():
        await send_error(ctx, "You cannot use the shop! Use `/myteam` to see what's going on.")
    if not team.is_at_object(MapObject.SHOP):
        await send_error(ctx, "You're not next to a shop's door!")
    else:
        sell_items = []

        item = None
        for i in team.items:
            if i["name"].lower() == item_name.lower():
                item = i
        
        if item is not None:
            if item["quantity"] >= quantity and quantity > 0:
                team.add_item(item["id"], -quantity)

                money_add = max(item["shop_cost"], 0) * quantity
                team.money += money_add
                game.db_save_team_items(team.id)
                game.db_save_team_money(team.id)
                try:
                    await ctx.response.send_message(embed = Ui.Embed("Info", f"You sold {quantity} **{item['name']}** for **{money_add}** bits."))
                except Exception as e:
                    traceback.print_exc()
            else:
                await send_error(ctx, f"You cannot sell more than you have (x{item['quantity']}), and quantity needs to be > 0!")
        else:
            await send_error(ctx, f"You don't have a \"{item_name}\"!")


@bot.slash_command(name="shop", description = "Buy items if you are at a shop's door")
async def shop(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return
    
    if not team_data.can_use_map_objects():
        await send_error(ctx, "You cannot use the shop! Use `/myteam` to see what's going on.")
    elif not team_data.is_at_object(MapObject.SHOP):
        await send_error(ctx, "You're not next to a shop's door!")
    else:
        shop_items = []

        for item_id in ItemData.item:
            item = ItemData.get_item(item_id)
            if item["shop_cost"] > 0:
                shop_items.append({"item": item, "title": item["name"] + f" ({item['shop_cost']} bits)", "description": item["description"], "emoji": item["emoji"]})

        # TODO - Check if you are still near shop
        async def bought_item(ctx, item):
            item = item["item"]
            if item["shop_cost"] > team_data.money:
                await send_error(ctx, "You don't have enough money for that item!")
            else:
                team_data.money -= item["shop_cost"]
                team_data.add_item(item["id"], 1)
                game.db_save_team_items(team_data.id)
                game.db_save_team_money(team_data.id)
                try:
                    await ctx.response.send_message(embed = Ui.Embed("Info", f"You bought **{item['name']}**! You have **{team_data.money}** bits left."))
                except Exception as e:
                    traceback.print_exc()

        await ctx.response.send_message(embed = Ui.Embed("Shop", f"Buy items! You have **{team_data.money}** bits."), view = Ui.GenericSelectView(ctx.author.id, shop_items, bought_item))

@bot.slash_command(name="cyber-center", description = "Heal your cybermon if you are at a cyber center's door")
async def cyber_center(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return
    
    if not team_data.can_use_map_objects():
        await send_error(ctx, "You cannot use the cyber center! Use `/myteam` to see what's going on.")
    elif not team_data.is_at_object(MapObject.CYBER_CENTER) and False:
        await send_error(ctx, "You're not next to a cyber center's door!")
    else:
        for c in team_data.cybermon:
            c.revive()
        await ctx.response.send_message(embed = Ui.Embed("Info", f"[MACHINERY NOISES]\nYour cybermon are now fully healed!"))

@bot.slash_command(name="view-item", description = "View details about an item you have")
async def view_item(ctx,
    item_name: str = commands.Param(name="item_name", description="Name of the owned item you want to see the details of (fuzzy search)")
):
    team = await get_user_team_data(ctx)
    if team is None:
        return
    
    item = None
    for i in team.items:
        if item_name.lower() in i["name"].lower():
            item = i
            break
    
    if item is None:
        for i in ItemData.item:
            temp_item = ItemData.get_item(i)
            if item_name.lower() in temp_item["name"].lower() and temp_item["shop_cost"] > 0:
                item = temp_item
                break

    if item is None:
        await send_error(ctx, "You don't have that item, or that item cannot be bought from a shop!")
    else:
        item_image = Ui.get_served_image("items/" + item["name"].lower().replace(" ", "_") + ".png")

        item_embed = Ui.Embed(item["name"], f'Item ID: **{item["id"].value}**\n{item["description"]}')
        item_embed.set_thumbnail(url = item_image)

        await ctx.response.send_message(embed = item_embed)

@bot.slash_command(name="view-cybermon", description = "View details about a cybermon you have")
async def view_cybermon(ctx,
    cybermon_name: str = commands.Param(name="cybermon_name", description="Name of the owned cybermon you want to see the details of (fuzzy search)")
):
    team = await get_user_team_data(ctx)
    if team is None:
        return
    
    cybermon = None
    for i, c in enumerate(team.cybermon + team.pc_cybermon):
        if cybermon_name == str(i):
            cybermon = c

    if cybermon is None:
        for i, c in enumerate(team.cybermon + team.pc_cybermon):
            if cybermon_name.lower() in c.name.lower():
                cybermon = c
                break

    if cybermon is None:
        await send_error(ctx, "You don't have that cybermon!")
    else:
        cybermon_image = Ui.get_served_image(f"cybermon/{cybermon.id.value}.png")

        def get_stats_string(stats):
            s = stats
            if type(s) != list:
                s = [stats.max_hp, stats.patk, stats.pdef, stats.satk, stats.sdef, stats.spd]

            return f"HP: **{s[0]}**, PATK: **{s[1]}**, PDEF: **{s[2]}**, SATK: **{s[3]}**, SDEF: **{s[4]}**, SPD: **{s[5]}**"

        description = f"Cybermon ID: **{cybermon.id.value}**\n"
        description += "Type: **" + ", ".join([t.name.title() for t in cybermon.types]) + "**\n"
        description += f"Level: **{cybermon.level}**\n"

        if cybermon.evolve_into is not None:
            description += f"Evolves at: LV. **{cybermon.evolve_at}** into **{cybermon.evolve_into.name}**\n"
        else:
            description += "Does not evolve.\n"

        embed = Ui.Embed(cybermon.name, description)
        embed.set_thumbnail(url = cybermon_image)

        description = "Base stats:\n> " + get_stats_string(cybermon.base_stats) + "\n\n"
        description += "IVs:\n> " + get_stats_string(cybermon.ivs) + "\n\n"
        description += "EVs:\n> " + get_stats_string(cybermon.evs) + "\n\n"
        description += "Current stats:\n> " + get_stats_string(cybermon.stats) + "\n"

        embed.add_field("Stats", description, inline=False)

        moveset = Cybermon.movesets[cybermon.id]
        if len(moveset) > 0:
            description = "\n".join([f"LV. **{i}**: **{MoveData.move[moveset[i]]['name']}**" for i in moveset])
            embed.add_field("Moveset", description, inline=False)

        await ctx.response.send_message(embed = embed)

@bot.slash_command(name="view-move", description = "View details about a move of any of your cybermon")
async def view_move(ctx,
    move_name: str = commands.Param(name="move_name", description="Name of the move you want to see the details of (fuzzy search)")
):
    team = await get_user_team_data(ctx)
    if team is None:
        return
    
    move = None
    for i in (team.cybermon + team.pc_cybermon):
        for candidate_move in (i.moves + i.pending_moves):
            if move_name.lower() in candidate_move["name"].lower():
                move = candidate_move
                break
        
    if move is None:
        await send_error(ctx, "You don't have any cybermon with that move!")
    else:
        description = f"Move ID: **{move['id'].value}**\n"
        description += move["description"]
        description += "\n\n"
        description += f"Move type: **{move['type'].name}**\n"
        description += f"Damage type: **{move['dmg_type'].name}**\n"
        description += f"Power: **{move['power']}**\n"
        description += f"Accuracy: **{int(abs(move['accuracy']) * 100)}%**\n"
        description += f"Max PP: **{move['max_pp']}**\n"

        move_embed = Ui.Embed(move["emoji"] + " " + move["name"], description)

        await ctx.response.send_message(embed = move_embed)

@bot.slash_command(name="pick-starter", description = "Pick your starter!")
async def pick_starter(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return
    
    if not team_data.is_in_state(TeamState.NO_CYBERMON):
        return await send_error(ctx, "You already picked a starter!")

    async def pick_starter_callback(ctx, starter_option):
        starter = starter_option["cybermon"]

        # GLITCH: if guard removed, you can select multiple starters by switching teams
        if team_data.is_in_state(TeamState.NO_CYBERMON):
            game.set_team_starter(team_data.id, starter)
            try:
                await ctx.response.edit_message(embed = Ui.Embed("Info", f"You picked the starter: **{starter.name}**"), view = None)
            except Exception as e:
                traceback.print_exc()
        else:
            await send_error(ctx, "You already have a starter.")

    team_data_ctfx = await get_ctfx_api_data(f"?get=user&id={team_data.id}")

    starter_options = []
    for ch in team_data_ctfx["challenges_solved"]:
        if ch["id"] in game.starters:
            cybermon = Cybermon(game.starters[ch["id"]]["id"])
            starter_options.append({"cybermon": cybermon, "title": cybermon.name, "description": "Type: " + ", ".join([t.name.title() for t in cybermon.types])})

    print(starter_options)
    return await ctx.response.send_message(
        embed = Ui.Embed("Pick a starter", "You can pick your team's starter here. You cannot change your starter later.\nMost warmup challenges you solve on CTFx will **unlock a new starter**!"),
        view = Ui.GenericSelectView(ctx.author.id, starter_options, pick_starter_callback)
    )
    
@bot.slash_command(name="find-team", description = "Search a team")
async def get_team(ctx,
    query: str = commands.Param(name="query", description="Term to search a team by. It can be a Team ID or part of a team name.")
):
    team_data = game.get_team_data(query)

    if type(team_data) == GameError:
        team_data = game.get_team_data_from_name_search(query)
        if type(team_data) == GameError:
            return await send_error(ctx, "Team not found.")
        
    await ctx.response.send_message(embed = get_team_embed(team_data, own_team = False))

@bot.slash_command(name="find-team-of-user", description = "Find out what team a user is in")
async def get_user_team(ctx,
    user: disnake.User = commands.Param(name="user", description="Find the team of this user")
):
    team_data = game.get_user_team_data(user.id)

    if type(team_data) == GameError:
        await send_error(ctx, "User is not in any team.")
        return

    await ctx.response.send_message(embed = get_team_embed(team_data, own_team = False))

@bot.slash_command(name="move", description = "Move by some offset")
async def move(ctx,
    x: int = commands.Param(name="x", description="How much to move on the X axis"),
    y: int = commands.Param(name="y", description="How much to move on the Y axis")
):
    team_data = await get_user_team_data(ctx)
    if (team_data is None):
        return
    
    if (team_data.is_in_state(TeamState.DEAD)):
        return await send_error(ctx, f"You cannot move since you recently lost and your cybermon are recovering. "
        f"You can move again in: **{team_data.revival_timestamp - int(time.time())} seconds**.")

    # This will throw a runtime warning because we create the async functions and don't await them here. That is fine
    async def move_callback():
        try:
            # Needs to be here to avoid precalculation of the file before we move, this send should we called after we finish moving
            if (timeout > 0):
                await ctx.channel.send(f"<@{ctx.author.id}>\n", embed = Ui.Embed("Info", f"You have finished moving.", Ui.render_map_for_team(game.get_all_teams_data(), team_data.id)))
            else:
                await ctx.response.send_message(embed = Ui.Embed("Info", "You stopped moving."))
        except Exception as e:
            traceback.print_exc()

    # Using a fuction simply to be uniform with move_callback
    async def encounter_callback(custom_message = None):
        try:
            if custom_message is None:
                message = f"A wild cybermon appeared!"
            else:
                message = custom_message

            message += "\nUse `/battle` to fight!"

            await ctx.channel.send(f"<@{ctx.author.id}>", embed = Ui.Embed("Info", message))
        except Exception as e:
            traceback.print_exc()

    timeout = game.start_moving_team(team_data.id, x, y, move_callback, encounter_callback)

    if type(timeout) == GameError:
        error = "Unknown error"
        if (timeout == GameError.TEAM_MOVE_OUT_OF_MAP): error = "You can't move there!"
        elif (timeout == GameError.TEAM_CANNOT_MOVE): error = "You can't move! Use `/myteam` to see what's happening."

        await send_error(ctx, error)
        return
    
    if timeout > 0:
        await ctx.response.send_message(embed = Ui.Embed("Info", f"Moving in: {timeout} seconds."))


@bot.slash_command(name="rewards", description = "See what rewards you can collect")
async def rewards(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return

    team_data_ctfx = await get_ctfx_api_data(f"?get=user&id={team_data.id}")
    game.load_challenge_names(await get_ctfx_api_data("?get=challenges"))

    reward_options = []
    rewards_text = ""

    index = 0
            
    for ch in team_data_ctfx["challenges_solved"]:
        if ch["id"] in game.rewards:
            chall_name = game.challenge_names[ch["id"]]
            rewards = game.rewards[ch["id"]]

            index += 1
            description = ", ".join([(r["id"].name + f" (x{r['quantity']})") for r in rewards])
            rewards_text += f"#{index}. "

            if ch["id"] not in team_data.rewards_taken:
                reward_options.append({"id": ch["id"], "title": chall_name, "description": description, "emoji": "\U0001F506"})
                rewards_text += " :high_brightness: "
            else:
                rewards_text += " :white_check_mark: "
            
            rewards_text += f"**{chall_name}**: " + description + "\n"

    async def claim_reward(ctx, reward):
        # GLITCH - Removing this guard means you can claim rewards multiple times
        if (reward["id"] not in team_data.rewards_taken):
            game.team_claim_reward(team_data.id, reward["id"])

            for i, r in enumerate(reward_options):
                if r["id"] == reward["id"]:
                    del reward_options[i]

            rewards_view = Ui.GenericSelectView(ctx.author.id, reward_options, claim_reward)
            try:
                await ctx.response.edit_message(embed = Ui.Embed("Info", f"You picked the reward from: **{game.challenge_names[reward['id']]}**! You can choose another reward, if there are any left."), view = rewards_view)
            except Exception as e:
                traceback.print_exc()

        else:
            await send_error(ctx, "You already picked that reward.")

    return await ctx.response.send_message(embed = Ui.Embed("Rewards", rewards_text), view = Ui.GenericSelectView(ctx.author.id, reward_options, claim_reward))

@bot.slash_command(name="bag", description = "See what items you can use outside of battle.")
async def bag(ctx):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return

    if not team_data.can_use_bag():
        return await send_error(ctx, "You can't use items right now! Use `/myteam` to see what's going on")

    async def use_item(ctx, item_option):
        item = item_option["item"]

        if not team_data.can_use_bag():
            if team_data.is_in_state(TeamState.BATTLE) and ItemUsability.BATTLE_WILD not in item["usability"] and ItemUsability.BATTLE_TEAM not in item["usability"]:
                await team_data.notify_glitch(GlitchId.WRONG_PLACE_WRONG_TIME, "Did you use an OutsideBattle item inside a battle? huh.\n**X-MAS{WRONG_PLACE_WRONG_TIME_8913c913ncy983nfc81fcado1c}**")
                team_data.add_item(ItemId.NULL_BERRY, 1)

        if item["quantity"] == 0:
            await team_data.notify_glitch(GlitchId.ITEM_DONT_HAVE, "If the UI decides things... Then we have a problem. Check your inventory.\n**X-MAS{STALE_STATES_18cn983fc1f819fcy831yf9c1yn398y}**")
            team_data.add_item(ItemId((item["id"].value + 1) % 58), 58 - item["id"].value)

            return await send_error(ctx, "You can't use this item!")
            
        if not team_data.can_use_bag():
            if (team_data.is_in_state(TeamState.BATTLE) and team_data.battle is not None):
                target_team = team_data.battle.get_opponent_team(team_data.id)

                return await team_data.battle.use_item(team_data, target_team, item, ctx)
            else:
                return await send_error(ctx, "You can't use items right now! Use `/myteam` to see what's going on")

        user = team_data.get_active_cybermon()

        if item["type"] == ItemType.CM:
            if item["restriction"] not in user.types and item["restriction"] is not None:
                try:
                    return await send_error(ctx, f"You cannot teach a {item['restriction'].name} type move on a cybermon with types: " + ", ".join([t.name for t in user.types]))
                except Exception as e:
                    traceback.print_exc()
                    return

        if ItemUsability.OUTSIDE_BATTLE not in item["usability"]:
            try:
                return await send_error(ctx, f"You cannot use this item outside battle!")
            except Exception as e:
                traceback.print_exc()
                return

        item_use_text = f"You used **{item['name']}**! "
        item_use_text += item["use_function"](item, user, None) 
        team_data.add_item(item["id"], -1)
        game.db_save_team_items(team_data.id)
        
        team = user.team
        if GlitchId.EV_UNDERFLOW in user.glitch_states:
            await team.notify_glitch(GlitchId.EV_UNDERFLOW, "Weaker than weak apparently means strong!\n**X-MAS{BABYS_FIRST_UNDERFLOW_fe1ciu1fyc918f98y3c98y}**")

        if GlitchId.NOCLIP in user.glitch_states:
            await team.notify_glitch(GlitchId.NOCLIP, "Finally, free movement. Time to explore! I hope there aren't any Mew under the truck.\n**X-MAS{NOCLIP_12c918c139ybc9f19c13cf83}**")

        if GlitchId.TELEPORT in user.glitch_states:
            await team.notify_glitch(GlitchId.TELEPORT, "!Cruisin'\n**X-MAS{TELEPORT_1ucnf983f198cy98c19c3c1c}**")

        if GlitchId.MENU in user.glitch_states:
            if not team.is_in_state(TeamState.BATTLE):
                team.add_state(TeamState.BATTLE)

                encounter_team = Team(None, '', (team.coords.x, team.coords.y))

                cybermon = Cybermon(CybermonId.Q)
                cybermon.configure(encounter_team, level=79)
                encounter_team.cybermon.append(cybermon)

                battle = Battle(game, team, encounter_team)
                team.battle = battle
                encounter_team.battle = battle
            
                item_use_text = "A wild cybermon appeared! Use `/battle` to fight!"
                user.glitch_states.remove(GlitchId.MENU)

        try:
            return await ctx.response.edit_message(embed = Ui.Embed("Info", item_use_text))
        except Exception as e:
            traceback.print_exc()

    item_options = []

    for i, item in enumerate(team_data.items):
        emoji = item['emoji']

        if ItemUsability.OUTSIDE_BATTLE in item["usability"]:
            item_options.append({"item": item, "item_index": i, "title": f"{item['name']} (x{item['quantity']})",
                                "description": Utils.limit_string(item["description"], 80), "emoji": emoji})

    print(ctx.author.id)
    print(item_options)
    await ctx.response.send_message(
        embed=Ui.Embed("Bag", "Choose an item from this list to use it.\nThis is **NOT** your full bag. To view a full list, use `/myteam`.\n\n"
                              "For more detailed information about an item, use `/view-item`."),
        view=Ui.GenericSelectView(ctx.author.id, item_options, use_item)
    )

@bot.slash_command(name="swap-cybermon", description="Swap cybermon in your active team (indexed from 0)")
async def swap_cybermon(ctx,
    cybermon_1: int = commands.Param(name="cybermon_1", description="First cybermon to swap positions with"),
    cybermon_2: int = commands.Param(name="cybermon_2", description="Second cybermon to swap positions with")
):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return

    return_code = game.swap_cybermon(team_data.id, cybermon_1, cybermon_2)

    if type(return_code) == GameError:
        error = "Unknown error"
        if (return_code == GameError.CYBERMON_INVALID_INDEX): error = "Invalid cybermon index."
        elif (return_code == GameError.CYBERMON_CANT_EXECUTE): error = "You can't swap cybermon right now! Use `/myteam` to see what's happening."
        
        return await send_error(ctx, error)
    
    await ctx.response.send_message(embed = Ui.Embed("Info", "Swap successful!"))

@bot.slash_command(name="teach-move", description="Replace one of your cybermon's moves with a move that it can learn (indexed from 0 to 3 inclusive)")
async def teach_move(ctx,
    cybermon: int = commands.Param(name="cybermon", description="Position of your cybermon in your team (indexed from 0)"),
    move_index: int = commands.Param(name="move_index", description="Index of your cybermon's current move that you want to REPLACE (indexed from 0)"),
    learn_move_index: int = commands.Param(name="learn_move_index", description="Index of your cybermon's move that you want to teach (indexed from 0)")
):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return

    return_code = game.teach_move(team_data.id, cybermon, move_index, learn_move_index)

    if type(return_code) == GameError:
        error = "Unknown error"
        if (return_code == GameError.CYBERMON_INVALID_INDEX): error = "Invalid cybermon index."
        if (return_code == GameError.CYBERMON_CANT_EXECUTE): error = "You can't teach moves right now."
        elif (return_code == GameError.CYBERMON_INVALID_MOVE_INDEX): error = "That's an invalid move index!"
        
        return await send_error(ctx, error)
    
    cybermon = team_data.cybermon[cybermon]
    await ctx.response.send_message(embed = Ui.Embed("Info", f"Successfully changed moveset for {cybermon.name}!"))

@bot.slash_command(name="nick", description = "Nickname a cybermon in your team")
async def nick(ctx,
    cybermon: int = commands.Param(name="cybermon", description="Position of your cybermon in your team (indexed from 0)"),
    nickname: str = commands.Param(name="nickname", description="Nickname of your cybermon")
):
    team_data = await get_user_team_data(ctx)
    if team_data is None:
        return
    
    return_code = game.nickname_cybermon(team_data.id, cybermon, nickname)

    if type(return_code) == GameError:
        return await send_error(ctx, "Invalid cybermon index.")
    
    await ctx.response.send_message(embed = Ui.Embed("Info", f"Nicknamed cybermon #{cybermon} to **{nickname[:16]}**"))

@bot.slash_command(name="battle", description = "See information about the battle you are in")
async def battle(ctx):
    attacker_team = await get_user_team_data(ctx)
    if (attacker_team is None): return

    if (attacker_team.battle is None):
        await send_error(ctx, "You are not in any battle right now")
        return

    ui = Ui.BattleUi(ctx.author.id, attacker_team, ctx)
    attacker_team.battle.set_team_ui(attacker_team.id, ui)

    for c in attacker_team.cybermon:
        if GlitchId.IF_EVERYONES_DEAD in c.glitch_states:
            await attacker_team.notify_glitch(GlitchId.IF_EVERYONES_DEAD, "The pomeg glitch, but in Cybermon! (P.S. This was actually an unintended glitch, so we added a flag for it! Good job.\n**X-MAS{IF_EVERYONES_DEAD_31cn98813yfc1cnfy13013cf1}**")

    await ui.refresh(new_message = True)


@bot.slash_command(name="unblock", description = "Unblock a team")
async def view_challenge(ctx,
    team_id: int = commands.Param(name="team_id", description="Team ID to unblock")
):
    attacker_team = await get_user_team_data(ctx)
    if (attacker_team is None):
        return
        
    target_team = game.get_team_data(team_id)

    if type(target_team) == GameError:
        return await send_error(ctx, "Invalid team.")
    
    if team_id in attacker_team.blocked_team_ids:
        attacker_team.blocked_team_ids.remove(team_id)
        game.db_save_team_blocks(attacker_team.id)
        await ctx.response.send_message(embed = Ui.Embed("Info", f"You have unblocked **{target_team.name}**!"))
    else:
        await ctx.response.send_message(embed = Ui.Embed("Info", f"Team **{target_team.name}** is not currently blocked by you."))


"""
@bot.slash_command(name="set-notification-channel")
async def set_notifcation_channel(ctx,
    team_id: int = commands.Param(name="team_id", description="Team ID to unblock")
):
"""


@bot.slash_command(name="view-challenge", description = "View another team's challenge to a battle")
async def view_challenge(ctx):
    attacker_team = await get_user_team_data(ctx)
    if (attacker_team is None):
        return

    target_team = attacker_team.challenged_team

    if target_team == None:
        return await send_error(ctx, "You have not been challenged by anyone, or your opponent has cancelled the challenge.")
    elif target_team.challenged_team != attacker_team:
        return await send_error(ctx, "Your opponent is no longer challenging you.")

    async def accept(ctx):
        if target_team.challenged_team == attacker_team and attacker_team.challenged_team == target_team:
            attacker_team.accepted_challenge = True

            try:
                if not target_team.accepted_challenge:
                    await ctx.response.edit_message(embed = Ui.Embed("Info", f"Your opponent needs to accept the challenge themselves."), view = None)
                else:
                    game.start_team_battle(attacker_team.id, target_team.id)
                    await ctx.response.edit_message(embed = Ui.Embed("Info", f"The battle has started! Use `/battle` to fight. You have a timeout of 5 minutes per turn."), view = None)
                    await target_team.notify(f"Team **{attacker_team.name}** has accepted your challenge! Use `/battle` to fight. You have a timeout of 5 minutes per turn.")
            except Exception as e:
                traceback.print_exc()

    async def reject(ctx):
        if target_team.challenged_team == attacker_team and attacker_team.challenged_team == target_team:
            target_team.clear_challenged()
            attacker_team.clear_challenged()
            try:
                await ctx.response.edit_message(embed = Ui.Embed("Info", f"You have cancelled the challenge."), view = None)
            except Exception as e:
                traceback.print_exc()

            await target_team.notify(f"Team **{attacker_team.name}** has cancelled your challenge.")

    async def block(ctx):
        if target_team.challenged_team == attacker_team and attacker_team.challenged_team == target_team:
            attacker_team.blocked_team_ids.append(target_team.id)
            game.db_save_team_blocks(attacker_team.id)

            target_team.clear_challenged()
            attacker_team.clear_challenged()
            try:
                await ctx.response.edit_message(embed = Ui.Embed("Info", f"You have blocked **{target_team.name}** from challenging you again! You can use `/unblock their_id` if you wish to unblock them."), view = None)
            except Exception as e:
                traceback.print_exc()

            await target_team.notify(f"Team **{attacker_team.name}** has blocked you from challenging them anymore.")

    accepted = attacker_team.accepted_challenge
    view = Ui.ChallengeView(ctx.author.id, accept, reject, block, accepted)

    description = f"You have been challenged by **{target_team.name}**!"
    if accepted:
        description = f"You are challenging **{target_team.name}**."

    # TODO - Show elo
    await ctx.response.send_message(embed = Ui.Embed("Info", description = description), view = view)


@bot.slash_command(name="new-challenge", description="Challenge a team at your coordinates to a battle!")
async def new_challenge(ctx):
    attacker_team = await get_user_team_data(ctx)
    if (attacker_team is None):
        return

    if (attacker_team.is_in_state(TeamState.DEAD)):
        return await send_error(ctx, f"You cannot challenge someone since you recently lost and your cybermon are recovering. You can challenge again in: **{attacker_team.revival_timestamp - int(time.time())} seconds**.")

    teams = game.get_teams_at_coords(attacker_team.coords.x, attacker_team.coords.y)
    
    for team in teams:
        if team.id == attacker_team.id:
            teams.remove(team)

    teams = teams[:25] # Limit for discord

    if len(teams) == 0:
        await send_error(ctx, "There are no teams to challenge at your exact coordinates.")
    else:
        async def callback(ctx, target_team_id):
            target_team = game.get_team_data(target_team_id)
            code = game.challenge_team_to_battle(attacker_team.id, target_team.id)

            if (type(code) == GameError):
                error = "Unknown error"
                
                if code == GameError.TEAM_SELF_BATTLING: error = f"You cannot battle yourself."
                elif code == GameError.TEAM_BATTLE_INVALID_TARGET: error = f"Cannot find your opponent team"
                elif (code == GameError.TEAM_CANNOT_BATTLE): error = f"One of you cannot battle. You might already be in a battle, or moving to a different location."
                elif (code == GameError.TEAM_NOT_SAME_COORDS): error = "You are not at the same coordinates as that team."
                elif (code == GameError.TEAM_BLOCKED): error = "This team has blocked you from challenging them."

                await send_error(ctx, error)
                return

            try:
                description = f"You challenged **{target_team.name}** to a battle! You can use `/view-challenge` to view the pending challenge."
                await ctx.response.edit_message(embed = Ui.Embed("Info", description=description), view=None)
            except Exception as e:
                traceback.print_exc()

            await target_team.notify(f"You have been challenged by **{attacker_team.name}**! Use `/view-challenge` to accept, cancel, or block this team.")

        description = "There " + (f"are {len(teams)} teams" if len(teams) > 1 else "is 1 team") + " at your coordinates:"
        await ctx.response.send_message(embed = Ui.Embed("Info", description=description), view = Ui.NewChallengeView(ctx.author.id, attacker_team.id, teams, callback))


@bot.slash_command(name="login", description = "Login with your CTFx account")
async def login(ctx,
    team_email: str = commands.Param(name="team_email", description="Your CTFx team Email"),
    team_pass: str = commands.Param(name="team_pass", description="Your CTFx team password")
):
    # TODO - re-enable?
    #if (ctx.guild is not None):
    #    return await send_error(ctx, 'You should only use this command in DMs!')

    team_data = {}

    async with aiohttp.ClientSession() as session:
        print(session.cookie_jar.filter_cookies(ctfx_api_endpoint))
        async with session.get(ctfx_api_endpoint + '/api?get=xsrf_token') as response:
            xsrf_token = await response.text()

        post_data = {'action': 'login', 'xsrf_token': xsrf_token, 'email': team_email, 'password': team_pass}

        async with session.post(ctfx_api_endpoint + '/api', data=post_data) as response:
            pass

        async with session.get(ctfx_api_endpoint + '/api?get=my_user_id') as response:
            team_id = await response.text()
        
        if (len(team_id) > 0):
            team_id = int(team_id)
            async with session.get(ctfx_api_endpoint + f'/api?get=user&id={team_id}') as response:
                team_data = json.loads(await response.text())

    if team_data == {}:
        await send_error(ctx, 'Invalid email or password.')
    else:
        game.create_team_from_ctfx(team_id, team_data)
        game.assign_user_to_team(ctx.author.id, team_id)

        await ctx.response.send_message(embed = Ui.Embed("Info", f"You have successfully joined team: **{team_data['team_name']}**!"), ephemeral = True)

if not os.path.exists(Utils.get_path("token")):
    print("Please create a token file in the bot directory")
    exit(1)

bot.loop.create_task(game.periodic_db_commit())
bot.loop.create_task(game.periodic_do_timed_actions())
bot.loop.create_task(game.periodic_move_players())
bot.run(open(Utils.get_path("token")).read().strip())
