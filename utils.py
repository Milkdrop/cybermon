import os
from game.cybermon import Cybermon


class Utils:
    @staticmethod
    def get_path(path):
        return os.path.join(Utils.get_pwd(), path)

    @staticmethod
    def get_pwd():
        return os.path.dirname(os.path.realpath(__file__))

    @staticmethod
    def limit_string(string, length):
        return string if len(string) <= length else (string[:length - 3] + "...")


class FunctionUtils:
    # All these functions assume the same format (x, y, z)
    # This is used for simpler function composition when we come to making moves.
    def run_all_any(*f):
        return lambda x, y, z: any([ft(x, y, z) for ft in f])

    def run_all_all(*f):
        return lambda x, y, z: all([ft(x, y, z) for ft in f])
