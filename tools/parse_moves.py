import re

move_arr = []
with open("moves_import.txt", "r", encoding="utf-8") as f:
    for line in f:
        move_arr.append(line.replace("\n", "").split("\t"))

enum_vals = []
for index, move in enumerate(move_arr):
    enum_vals.append(
        [
            index, "".join(
            [i if ord(i) < 128 and i not in (" ", ".", "?", "@", "-") else "_" for i in move[0].upper()]
        )
        ]
    )

enum_vals[0][1] = "__b"
enum_vals[26][1] = "REMOVE_THIS1"
enum_vals[27][1] = "REMOVE_THIS2"
enum_vals[28][1] = "REMOVE_THIS3"

# for index in range(len(move_arr)):
#     print(enum_vals[index][1], "=", enum_vals[index][0])

# name, emoji (same for now), type, power, accuracy (0-1, so divide by 100), max_pp, attack_function
# remember to remove "~"
# for parseable specials (matches regex ".*=.*"), convert into a dict and pass into the "attack_options" dict.
format_str = 'MoveId.{}: П\n' \
             '\t"name": "{}", "emoji": "\\u2694", "type": CybermonType.{}, "dmg_type": MoveDamageType.{},\n' \
             '\t"power": {}, "accuracy": {}, "max_pp": {}, "description": "{}",\n' \
             '\t"attack_function": {}, "attack_options": {}\n' \
             'Л,\n'

attack_functions_used = {}
check_special_regexp = re.compile(".*=.*")
find_all_specials_regexp = re.compile("(\w+?)=(\d+|True|False|\(.+?\))")

for i in range(len(move_arr)):
    move = move_arr[i]

    if check_special_regexp.match(move[7]):
        attack_function_name = "generic_move_handler"
        replaced_options = find_all_specials_regexp.sub("\"\\1\": \\2", move[7])
        attack_options = "{" + replaced_options.rstrip(",") + "}"
    elif move[7] == "None":
        attack_function_name = "generic_move_handler"
        attack_options = "{}"
    else:
        if move[7] not in attack_functions_used:
            attack_functions_used[move[7]] = "special_moveid_{}".format(i)

        attack_function_name = attack_functions_used[move[7]]
        attack_options = "{}"

    print(format_str.format(
        enum_vals[i][1],
        move[0], move[1].upper(), move[2].upper(),
        move[3].replace("~", "") if move[3] != "--" else -1,
        round(int(move[4].replace("~", "")) / 100, 2) if move[4] != "--" else -1,
        move[5].replace("~", ""), move[6],
        attack_function_name, attack_options
    ).replace("П", "{").replace("Л", "}"))

dic = {

}
