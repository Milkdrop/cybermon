# Dict in which we first look up the type of the attacker, then the type of the defender.
from game.enums import CybermonType
from enum import Enum

with open("matchups_import.txt", "r", encoding="utf-8") as f:
    matchups = []
    for line in f.readlines():
        matchups.append(line.replace("\n", "").split("\t"))

type_list = matchups[0][1:]
matchups = matchups[1:]

txt = "matchups = {\n"
for item in matchups:
    txt += "\tCybermonType.{}: <\n".format(item[0].upper())
    txt += ",\n".join(
        "\t\tCybermonType.{}: MoveEffectiveness.{}".format(
            type_list[i].upper(), {
                "1×": "NORMAL",
                "2×": "SUPEREFFECTIVE",
                "½×": "NONEFFECTIVE",
                "0×": "ZERO",
            }[item[i + 1]]
        ) for i in range(len(item) - 1)
    )
    txt += "\n\t},\n\n"

txt += "}"

print(txt.replace(">", "}").replace("<", "{"))


class MoveEffectiveness(Enum):
    NORMAL = 1
    NONEFFECTIVE = 2
    SUPEREFFECTIVE = 3
    ZERO = 4
