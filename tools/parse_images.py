from PIL import Image, ImageOps, ImageFilter
import os

def makeShadow(image):
    im = Image.new("RGBA", image.size)
    im.paste((0, 0, 0, 255), (0, 0), image)

    #border = 4
    #im = im.resize((512 + border * 2, 512 + border * 2)).crop((border, border, border + 512, border + 512))
    for i in range(2):
        im = im.filter(ImageFilter.BLUR)
    im.paste(image, (0, 0), image)
    return im

for f in os.listdir("cybermon_original"):
    im = Image.open("cybermon_original/" + f)
    print(f, im.size)
    res_im = Image.new("RGBA", (512, 512))

    dist_x = 512 - im.size[0]
    dist_y = 512 - im.size[1]
    res_im.paste(im, (dist_x // 2, dist_y, dist_x // 2 + im.size[0], dist_y + im.size[1]))
    res_im = makeShadow(res_im)

    c_id = f[:-4]

    if (int(c_id) in [1, 2, 4, 6, 7, 10, 32, 37, 41, 44, 50, 51, 56, 58, 60]):
        res_im = res_im.transpose(Image.FLIP_LEFT_RIGHT)
        print("Flipping " + c_id)

    res_im.save("cybermon/" + f)