import disnake

from utils import Utils
from disnake.ext import commands

bot = commands.Bot(command_prefix="!")
bot.remove_command('help')

@bot.event
async def on_ready():
    print(bot.user.name + " powered up!")
    for guild in bot.guilds:
        print("\nGuild {} (id {}):".format(guild.name, guild.id))
        for emoji in guild.emojis:
            print(emoji)

bot.run(open(Utils.get_path("token")).read().strip())
