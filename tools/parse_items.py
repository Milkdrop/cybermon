item_arr = []
with open("items_import.txt", "r", encoding="utf-8") as f:
    for line in f:
        item_arr.append(line.replace("\n", "").split("\t"))

enum_vals = []
for index, move in enumerate(item_arr):
    enum_vals.append(
        [
            index, "".join(
                [i if ord(i) < 128 and i not in (" ", ".", "?", "@", "-") else "_" for i in move[1].upper()]
            )
        ]
    )

enum_vals[0][1] = "NULL_BERRY"
enum_vals[53][1] = "QUEQUEQUEQUEQUE"
enum_vals[60][1] = "DOKOKASHIRA"

# Parse emojis to find the  fucking
ids_to_emojis = {}
with open("emoji_names.txt", "r") as f:
    sprite_names_formatted = ["".join([i if ord(i) < 128 and i not in (" ", ".", "?", "@", "-") else "" for i in item[0]]) for item in item_arr]
    sprite_names_formatted[59] = "t_"
    sprite_names_formatted[60] = "dokokashira"

    for line in f:
        for i in range(len(sprite_names_formatted)):
            if sprite_names_formatted[i] == line.split(":")[1]:
                if line.replace("\n", "") in ids_to_emojis.values():
                    print("Tried to add duplicate emoji {} at index {}".format(line.replace("\n", ""), i))
                else:
                    ids_to_emojis[i] = line.replace("\n", "")
                    sprite_names_formatted[i] = "##################################"
                    break

# for i in range(64):
#     if i in ids_to_emojis:
#         print("{}: {}".format(i, ids_to_emojis[i]))
#     else:
#         print("{} NOT FOUND!!!!".format(i))


# for index in range(len(item_arr)):
#     print(enum_vals[index][1], "=", enum_vals[index][0])

# ItemId.APPA_BERRY: {
#     "name": "Appa Berry",
#     "description": "A hard berry with a sour taste. Used to cure burns, its medicinal properties also slightly heal the Cybermon that consumes it.",
#     "emoji": "\U0001F3D0", "use_function": debug_item,
#     "usability": [ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE]
# },

format_string = '# Effect: {}\n' \
                '# Obtain: {}\n' \
                'ItemId.{}: П\n' \
                '    "name": "{}", "type": ItemType.{},\n' \
                '    "description": "{}",\n' \
                '    "emoji": "{}", "use_function": itemfunction_id_{},\n' \
                '    "usability": [{}], "shop_cost": {}\n' \
                'Л,\n'

for i in range(len(item_arr)):
    item = item_arr[i]

    usability = ""
    if item[3] == "Nonbattle":
        usability = "ItemUsability.OUTSIDE_BATTLE"
    elif item[3] == "Battle":
        if "Cyberbox" in item[1]:
            usability = "ItemUsability.BATTLE_WILD"
        else:
            usability = "ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM"
    else:
        usability = "ItemUsability.BATTLE_WILD, ItemUsability.BATTLE_TEAM, ItemUsability.OUTSIDE_BATTLE"

    print(format_string.format(
        item[5],
        item[6],
        enum_vals[i][1],
        item[1], item[2].upper(),
        item[4].replace("\"", "\\\""),
        ids_to_emojis[i], i,
        usability, -1 if item[7] == "--" else item[7]
    ).replace("П", "{").replace("Л", "}"))
