from copy import copy

from game.enums import CybermonId

moveset_arr = []
with open("movesets_import.txt", "r", encoding="utf-8") as f:
    for line in f:
        moveset_arr.append(line.replace("\n", "").split("\t"))

move_arr = []
with open("moves_import.txt", "r", encoding="utf-8") as f:
    for line in f:
        move_arr.append(line.replace("\n", "").split("\t"))

enum_vals = []
for index, move in enumerate(move_arr):
    enum_vals.append(
        [
            index, "".join(
            [i if ord(i) < 128 and i not in (" ", ".", "?", "@", "-") else "_" for i in move[0].upper()]
        )
        ]
    )

enum_vals[0][1] = "B_"
enum_vals[26][1] = "REMOVE_THIS1"
enum_vals[27][1] = "REMOVE_THIS2"
enum_vals[28][1] = "REMOVE_THIS3"


forced_flushes = (0, 18, 19)
learnsets = []

buffer = []
for index, moveset in enumerate(moveset_arr):
    for move in moveset[1].split(","):
        move_index = -1
        if move.split(" ", 1)[1].startswith("REMOVE_THIS"):
            move_index = 25 + int(move.split("(", 1)[1][:1])
        else:
            for i in range(len(move_arr)):
                if move_arr[i][0] == move.split(" ", 1)[1]:
                    move_index = i
                    break

        buffer.append((move.split(" ", 1)[0], enum_vals[move_index][1]))

    learnsets.append(buffer.copy())
    if not moveset[0] or index in forced_flushes:
        buffer = []

print("movesets = {")
for index, l in enumerate(learnsets):
    print("\t{}: <\n{}\t\n\t>,\n".format(
        CybermonId(index + (1 if index >= 43 else 0)),
        ",\n".join(
            "\t\t{}: MoveId.{}".format(
                t[0], t[1]
            ) for t in l
        )
    ).replace("<", "{").replace(">", "}"))

print("}")
