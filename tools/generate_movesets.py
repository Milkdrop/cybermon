import random

moves = []
with open("moves_copy.txt", "r", encoding="utf-8") as f:
    for line in f.readlines():
        moves.append(line.replace("\n", "").split("	")[1:])

scores = []
print("Tackle has value 1 and frequency 950 (so it will often be picked)")
print("Power Hit has value 15 and frequency 850.\n")
with open("moves_ratings.txt", "r") as f:
    for index, line in enumerate(f):
        scores.append([moves[index], [int(t) for t in line.replace("\n", "").split(" ")]])

print("Loaded {} ratings".format(len(scores)))

for move in moves[len(scores):len(scores) + 12]:
    print("\n" + move[0])
    new_ratings = [
        int(input("level value? (0 for 'do not use')            >> ")),
        int(input("frequency? (0-1000; if -1, big chance bonus) >> "))
    ]

    scores.append([move, new_ratings])

#print("\n".join(
#    "{} {}".format(int(rating[1][0]), int(rating[1][1])) for rating in scores
#))

# sort by frequency then value
scores = sorted(scores, key=lambda t: t[1][1])
scores = sorted(scores, key=lambda t: t[1][0])

print("\n")

while True:
    moveset = []
    types = input("types? (Normal, Ground, ...) >> ").lower().split(",")
    possible_moves = list(filter(lambda t: t[0][1].lower() in types and t[1][0] != 0, scores))
    # after a move is picked, do not pick another for 2-4 levels
    grace_time = 0
    for level in range(1, 61):
        added = False
        for i in range(min(3, len(possible_moves))):
            if possible_moves[i][1][1] == -1 and abs(possible_moves[i][1][0] - level) <= 4 and random.randint(1, 2) == 2 and grace_time <= 1:
                added = True
                print(level, possible_moves[i][0][0])
                possible_moves.pop(i)
                grace_time = random.randint(3, 4) + int(level // 10)
                break

        if grace_time <= 0 and len(possible_moves) > 0 and not added:
            selected_move = possible_moves[random.randint(0, min(len(possible_moves) - 1, 2))]
            if random.randint(0, 999) < selected_move[1][1] and abs(selected_move[1][0] - level) <= 8:
                added = True
                print(level, selected_move[0][0])
                possible_moves.remove(selected_move)
                grace_time += random.randint(3, 4) + int(level // 10)

        while len(possible_moves) != 0 and level - possible_moves[0][1][0] > 8:
            possible_moves.pop(0)

        grace_time -= 1
