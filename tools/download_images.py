import os
from enum import Enum
import requests

class CybermonId(Enum):
    Q = 0
    GUILGILE = 1
    GROTUILLE = 2
    GUILLORENT = 3
    CROCOAL = 4
    FEUCROTA = 5
    BURUNGIN = 6
    LIZEROON = 7
    DRACOROON = 8
    MANDRAGOON = 9
    FINNIAL = 10
    TERMELC = 11
    DISTRIKE = 12
    GRAVENDOU = 13
    CRAGENDOU = 14
    QUARENDOU = 15
    MELANYAN = 16
    OCELUNA = 17
    PANUMBRA = 18
    PANUMBRATEST2 = 19
    MUDDLEBEE = 20
    DRILLINDER = 21
    DAUVESPA = 22
    POMPILLAR = 23
    GILTALIS = 24
    VANITARCH = 25
    GOWATU = 26
    TURATAL = 27
    JOSUCHE = 28
    FAINTRICK = 29
    FRAUDKILL = 30
    DRIBBINO = 31
    ACCELERET = 32
    VELOSWIFT = 33
    ACAFIA = 34
    CATALCIA = 35
    BOSSORNA = 36
    GUMBWAAL = 37
    HANDERWAAL = 38
    GASTREL = 39
    SKYRATE = 40
    ROBONBO = 41
    ROBUTANE = 42
    JACKRAVAGE = 44
    TINIMER = 45
    ALTAVAULT = 46
    TUNNOWL = 47
    GRYPHAULT = 48
    SEISMOGRYPH = 49
    SWIMMOLE = 50
    NEURAMOLE = 51
    NOSTRAMOLE = 52
    SHRYMPH = 53
    SKRIMPISH = 54
    DUELANTIS = 55
    WYRMAL = 56
    VENTORM = 57
    LARVILL = 58
    CAVERNOLM = 59
    STICKIBAT = 60
    TRICKIBAT = 61
    BANDIBAT = 62
    DASFIX = 63
    MALRAJA = 64

#os.mkdir("cybermon")

for c in CybermonId:
	print(c.name, c.value)

	img_data = requests.get(f"https://phoenixdex.alteredorigin.net/images/pokemon/{c.name.lower()}.png").content

	with open(f'cybermon/{c.value}.png', 'wb') as handler:
	    handler.write(img_data)
