import os

f = open("sheet.tsv").read().strip().split("\n")[1:]

cybermon = []
types_seen = []

id_to_enum = {}
type_to_enum = {}

for line in f:
	elems = line.split('\t')
	if len(elems) < 3 or len(elems[2]) == 0:
		break

	c_id = elems[1]
	c_name = elems[2]
	c_types = elems[3].split(", ")
	c_evolve = elems[6]
	c_evolve_lv = elems[7]

	c_hp = elems[8]
	c_atk = elems[9]
	c_def = elems[10]
	c_satk = elems[11]
	c_sdef = elems[12]
	c_spd = elems[13]

	c = {"id": c_id, "name": c_name, "types": c_types, "evolve_at": c_evolve_lv, "evolve_into": c_evolve,
		"hp": c_hp, "atk": c_atk, "def": c_def, "satk": c_satk, "sdef": c_sdef, "spd": c_spd}
	
	for t in c_types:
		if t not in types_seen:
			types_seen.append(t)

	cybermon.append(c)

print("class CybermonId(Enum):")

id_to_enum[''] = 'None'
for c in cybermon:
	filtered_name = c['name'].upper().replace("'",'')
	id_to_enum[c['id']] = f"CybermonId.{filtered_name}"

	print(f"    {filtered_name} = {c['id']}")

print("")
print("class CybermonType(Enum):")

index = 0
for t in types_seen:
	type_to_enum[t] = f"CybermonType.{t.upper()}"
	print(f"    {t.upper()} = {index}")

	index += 1

print("")
print("templates = {")

for c in cybermon:
	print(f"    {id_to_enum[c['id']]}: " + "{"
		+ f'"name": "{c["name"]}", '
		+ f'"types": [{", ".join([type_to_enum[t] for t in c["types"]])}], '
		+ f'"evolve_into": {id_to_enum[c["evolve_into"]]}, '
		+ f'"evolve_at": {c["evolve_at"] if c["evolve_at"] else 0}, '
		+ f'"hp": {c["hp"]}, '
		+ f'"atk": {c["atk"]}, '
		+ f'"def": {c["def"]}, '
		+ f'"satk": {c["satk"]}, '
		+ f'"sdef": {c["sdef"]}, '
		+ f'"spd": {c["spd"]}'
		+ "},")

print("}")