import os
from PIL import Image

path = "../assets/items/"
for item in os.listdir(path):
    if item != "big":
        print(item)
        img = Image.open(os.path.join(path, item))
        img2 = img.resize((480, 480), Image.NEAREST)
        img2.save(os.path.join(path, "big/", item))
