# +----+----------------------------------------------------------------+
# | id | title                                                          |
# +----+----------------------------------------------------------------+
# |  1 | Santa's Snowflake Sharing                                      |
# |  2 | Cryptographic Pairs                                            |
# |  3 | Santa's Secret Encoding Machine                                |
# |  4 | All three reindeers are fabulous-ish                           |
# |  5 | Still two bad reindeer                                         |
# |  6 | There can't be repetitions                                     |
# |  7 | Worst two reindeer                                             |
# |  8 | Santa's Bilingual Encryption System                            |
# |  9 | One Time Present                                               |
# | 10 | Gnome Oriented Programming                                     |
# | 11 | PinguVM 2 - Take the red pill                                  |
# | 12 | PinguVM 3 - Escaping the Matrix                                |
# | 13 | PinguVM 4 - Deafeating the Architect                           |
# | 14 | W=X                                                            |
# | 15 | Santa's Tobacco Shop                                           |
# | 16 | DNA Hack                                                       |
# | 17 | ROP Puzzle                                                     |
# | 18 | Elftek Secure Execution Engine                                 |
# | 19 | Mem-X                                                          |
# | 20 | Supply Troubles                                                |
# | 21 | P.A.I.N.T.: Professional Arctic Interactive NT drawing service |
# | 22 | QuoteHub                                                       |
# | 23 | ImageHub                                                       |
# | 24 | Santa's Shady Toy Manufacturing Business Part 1                |
# | 25 | Santa's Shady Toy Manufacturing Business Part 2                |
# | 26 | Santa's Shady Toy Manufacturing Business Part 3                |
# | 27 | Gnome Buttons v5                                               |
# | 28 | Public Ledger                                                  |
# | 29 | Glowy Password Manager                                         |
# | 30 | NALcromancer                                                   |
# | 31 | Underground forensics                                          |
# | 32 | Super Secret Communications                                    |
# | 33 | The Object                                                     |
# | 34 | PinguVM 1 - Waking up in the Matrix                            |
# | 35 | The grinch                                                     |
# | 36 | Intensive training                                             |
# | 37 | Lapland Crackme                                                |
# | 38 | Snake ransomware                                               |
# | 39 | 8-Bit Picasso                                                  |
# | 40 | Ahoy                                                           |
# | 41 | Logic Snowflakes                                               |
# | 43 | Super-(in)secure licensing system                              |
# | 44 | Champion gene                                                  |
# | 45 | P.A.I.N.T. Art Competition                                     |
# | 46 | Dox The Yak v4                                                 |
# | 47 | Santa's Flakpanzerkampfwagen                                   |
# | 48 | Scuffed Tetris                                                 |
# | 49 | Krampus' Inferno                                               |
# | 50 | Intergalactic Christmas                                        |
# | 51 | Cheating The Elf Exams                                         |
# | 52 | Santa's Personal Painter                                       |
# | 53 | Piano Carol                                                    |
# | 54 | wETHelcome!                                                    |
# | 55 | lvl 100 cookie boss                                            |
# | 56 | 残響                                                           |
# | 57 | CaramelPooler                                                  |
# | 58 | Having a BLAST                                                 |
# | 59 | A putative sequence                                            |
# | 60 | SantaVax reverse engineering                                   |
# | 61 | Bioengineering 101                                             |
# | 62 | Ho Ho Ho! Welcome!                                             |
# | 63 | The place where @everyone hangs out                            |
# +----+----------------------------------------------------------------+

# cm01 <- One Time Present (9)
# cm02 <- Santa's Tobacco Shop (15)
# cm03 <- ROP Puzzle (17)
# cm04 <- QuoteHub (22)
# cm05 <- Santa's Shady Toy Manufacturing Business Part 2 (25)
# cm06 <- Public Ledger (28)
# cm07 <- Intensive training (36)
# cm08 <- Lapland Crackme (37)
# cm09 <- The Object (33)
# cm10 <- NALcromancer (30)
# cm11 <- All thre reindeers are fabulous-ish (4)
# cm12 <- P.A.I.N.T.: Professional Arctic Interactive NT drawing service (21)
# cm13 <- 残響 (56)
# cm14 <- A putative sequence (59)
# cm15 <- Krampus' Inferno (49)
# cm16 <- Worst two reindeer (7)

cms = (
    9, 15, 17, 22, 25, 28, 36, 37, 33, 30, 4, 21, 56, 59, 49, 7
)

# self.rewards = {
#     2: [{"id": ItemId.CYBERBOX, "quantity": 1}, {"id": ItemId.APPA_BERRY, "quantity": 60}],
#     4: [{"id": ItemId.CYBERBOX, "quantity": 5}]
# }
import random

challenges = {
 1 : "Santa's Snowflake Sharing"                                      ,
 2 : "Cryptographic Pairs"                                            ,
 3 : "Santa's Secret Encoding Machine"                                ,
 4 : "All three reindeers are fabulous-ish"                           ,
 5 : "Still two bad reindeer"                                         ,
 6 : "There can't be repetitions"                                     ,
 7 : "Worst two reindeer"                                             ,
 8 : "Santa's Bilingual Encryption System"                            ,
 9 : "One Time Present"                                               ,
10 : "Gnome Oriented Programming"                                     ,
11 : "PinguVM 2 - Take the red pill"                                  ,
12 : "PinguVM 3 - Escaping the Matrix"                                ,
13 : "PinguVM 4 - Deafeating the Architect"                           ,
14 : "W=X"                                                            ,
15 : "Santa's Tobacco Shop"                                           ,
16 : "DNA Hack"                                                       ,
17 : "ROP Puzzle"                                                     ,
18 : "Elftek Secure Execution Engine"                                 ,
19 : "Mem-X"                                                          ,
20 : "Supply Troubles"                                                ,
21 : "P.A.I.N.T.: Professional Arctic Interactive NT drawing service" ,
22 : "QuoteHub"                                                       ,
23 : "ImageHub"                                                       ,
24 : "Santa's Shady Toy Manufacturing Business Part 1"                ,
25 : "Santa's Shady Toy Manufacturing Business Part 2"                ,
26 : "Santa's Shady Toy Manufacturing Business Part 3"                ,
27 : "Gnome Buttons v5"                                               ,
28 : "Public Ledger"                                                  ,
29 : "Glowy Password Manager"                                         ,
30 : "NALcromancer"                                                   ,
31 : "Underground forensics"                                          ,
32 : "Super Secret Communications"                                    ,
33 : "The Object"                                                     ,
34 : "PinguVM 1 - Waking up in the Matrix"                            ,
35 : "The grinch"                                                     ,
36 : "Intensive training"                                             ,
37 : "Lapland Crackme"                                                ,
38 : "Snake ransomware"                                               ,
39 : "8-Bit Picasso"                                                  ,
40 : "Ahoy"                                                           ,
41 : "Logic Snowflakes"                                               ,
42 : "Unknown",
43 : "Super-(in)secure licensing system"                              ,
44 : "Champion gene"                                                  ,
45 : "P.A.I.N.T. Art Competition"                                     ,
46 : "Dox The Yak v4"                                                 ,
47 : "Santa's Flakpanzerkampfwagen"                                   ,
48 : "Scuffed Tetris"                                                 ,
49 : "Krampus' Inferno"                                               ,
50 : "Intergalactic Christmas"                                        ,
51 : "Cheating The Elf Exams"                                         ,
52 : "Santa's Personal Painter"                                       ,
53 : "Piano Carol"                                                    ,
54 : "wETHelcome!"                                                    ,
55 : "lvl 100 cookie boss"                                            ,
56 : "残響"                                                             ,
57 : "CaramelPooler"                                                  ,
58 : "Having a BLAST"                                                 ,
59 : "A putative sequence"                                            ,
60 : "SantaVax reverse engineering"                                   ,
61 : "Bioengineering 101"                                             ,
62 : "Ho Ho Ho! Welcome!"                                             ,
63 : "The place where @everyone hangs out"                            ,
}

txt = "self.rewards = {\n"
for i in range(1, 64):
    reward_weights = [
        ("WAK_BERRY", 1, True, 3),
        ("ONON_BERRY", 2, True, 3),
        ("BLANC_BERRY", 2, True, 3),
        ("MED_BERRY", 2, True, 2),
        ("PULPA_BERRY", 4, True, 2),
        ("GREEN_CYBERDRUG", 3, True, 1),
        ("PURPLE_CYBERDRUG", 3, True, 1),
        ("BLUE_CYBERDRUG", 5, True, 1),
        ("WHITE_CYBERDRUG", 5, True, 1),
        ("HERBAL_MEDICINE", 1, True, 2),
        ("HERBAL_REMEDY", 1, True, 2),
        ("RED_POWDERBAG", 4, True, 1),
        ("GREEN_POWDERBAG", 7, True, 1),
        ("CURIOSITY_CHEST", 8, False, 1),
        ("BERRY_BASKET", 7, False, 1),
        ("RARITY_CHEST", 14, False, 1),
        ("SUPPLY_BACKPACK", 10, False, 1),
        ("RARITY_CHEST", 14, False, 1),
    ]

    value = (8, 14, 20, 0)[int(input("{:2} > {:64} -> easy (1), medium (2), hard (3), none (4) >> ".format(i, challenges[i]))) - 1]

    rewards = {}

    if i in cms:
        value = int(value / 2)
        rewards["CM{:02}".format(cms.index(i) + 1)] = 1

    while value > 0:
        for j in range(len(reward_weights) - 1, -1, -1):
            if random.randint(1, 3) == 1 and reward_weights[j][1] <= value:
                value -= reward_weights[j][1]
                if reward_weights[j][0] in rewards:
                    rewards[reward_weights[j][0]] += reward_weights[j][3]
                else:
                    rewards[reward_weights[j][0]] = reward_weights[j][3]

                if not reward_weights[j][2]:
                    reward_weights.pop(j)

                break

    txt += "\t{}: [{}],\n".format(
        i, ", ".join(
            "<\"id\": ItemId.{}, \"quantity\": {}>".format(
                t[0], t[1]
            ) for t in rewards.items()
        )
    ).replace("<", "{").replace(">", "}")

print(txt)
print("}")

"""
INPUTS:
1
2
3
3
2
3
2
2
1
4
2
3
2
1
1
1
2
1
4
2
1
2
2
1
2
3
4
2
1
3
1
4
1
2
4
1
2
2
2
2
2
4
3
2
4
4
2
2
2
1
3
2
1
4
1
2
3
1
2
3
3
4
4
"""