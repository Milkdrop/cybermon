127	1 ..b.,2 Bash,3 Bite,4 Power Hit,8 Water Jet,104 Scald,255 @
18	1 Tackle,2 Water Jet,4 Curl Up,7 Bite,9 Caress,11 Psych Up,15 Power Hit
34	18 Sonic Splash,22 Rock Throw,26 Earthshock,28 All-Out Attack,31 Erosion
	34 Deluge,36 Landslide,41 Powerblast,45 Erosion,51 Boulderfall,56 Tidal Wave
16	1 Tackle,2 Scald,5 Curl Up,8 Bite,11 Psych Up
32	16 Scorch,18 Shadow Strike,22 Sabotage,25 Ignite,27 Jumping Kick,30 Power Hit
	32 Volcanic Cutter,35 Darkcutter,42 Cauterize,48 Backstab,54 Immolate
17	1 Tackle,2 Spit Seed,5 Bite,9 Absorb,13 Photosynthesis
34	17 Seedcannon,21 Swoop,23 Lifedrain,26 Coalesce Aura,29 Powerblast
	34 Infestation,38 Wing Beat,41 Meteor,46 Divebomb,52 Arcanic Blast,55 Overgrowth
15	1 Tackle,4 Shock,7 Bash,10 Psych Up
32	15 Lightningbolt,23 Thunder,30 Alloy Strike
	32 Powerblast,39 All-Out Attack,44 Titanium Induction,52 Lethal Current
16	2 Bite,6 Tackle,9 Rock Throw,14 Slap
32	16 Power Hit,18 Landslide,23 Earthquake,29 Powerblast
	32 Fissure,40 All-Out Attack,51 Mountain Breaker,58 Eruption
14	1 Bite,6 Bash,11 Psych Up
28	14 Mind Tricks,19 Jumping Kick,24 Flay,26 Sabotage
101	28 Backstab,32 Flay,45 Mind Blast,47 Paradox Channeling
0	1 Tackle,47 REMOVE_THIS (1),57 REMOVE_THIS (2),67 REMOVE_THIS (3)
8	1 Spit Seed,3 Nip,5 Bite
20	11 Absorb,17 Armour Crush,19 Pupate
	21 Caress,28 Swarm,34 Infestation,42 All-Out Attack
10	1 Nip,4 Bite,7 Bash
18	10 Psych Up,12 Poison Spit,15 Caress
	20 Armour Crush,26 Swarm,32 Debilitate,39 Needler
20	1 Bite,5 Bash,11 Curl Up,14 Slap
38	23 Power Hit,26 Shock Palm,29 Wing Beat,35 All-Out Attack
	38 Powerblast,42 Divebomb,50 Lethal Fist
22	1 Trick,6 Bash,10 Curl Up,15 Slap,20 Caress
	26 Posess,31 Sabotage,38 All-Out Attack,45 Exorcism
18	1 Tackle,6 Bite,10 Curl Up,15 Swoop
34	19 Power Hit,24 Wing Beat,30 All-Out Attack
	32 Powerblast,36 Limber,41 All-Out Attack,46 Divebomb
19	1 Spit Seed,5 Absorb,9 Imbibe,13 Photosynthesis
35	19 Seedcannon,22 Lifedrain,29 Infestation
	35 Debilitate,42 Fatal Poison,51 Overgrowth
20	1 Bite,6 Bash,9 Curl Up,13 Psych Up
	21 Power Hit,28 Infestation,35 All-Out Attack,39 Lethal Fist
25	1 Feather Whip,4 Bite,7 Bash,13 Curl Up,19 Slap
	25 Imbibe,28 Wing Beat,32 Debilitate,38 Fatal Poison,47 Divebomb
28	2 Scald,4 Feather Whip,7 Bash,11 Slap,15 Ignite,20 Caress
	28 Wing Beat,33 Cauterize,39 All-Out Attack,46 Divebomb,53 Immolate
	1 Tackle,5 Curl Up,9 Bite,13 Slap,17 Caress,22 Shock Palm,28 All-Out Attack,33 Powerblast,39 Lethal Fist
20	1 Tackle,3 Nip,6 Bash,10 Curl Up,14 Slap,18 Infest
	20 Armour Crush,25 Caress,28 Tectonics,33 Powerblast,39 Needler,48 Mountain Breaker
25	1 Bite,5 Earthshock,9 Curl Up,13 Slap,18 Power Hit
40	25 Limber,29 Wing Beat,32 Earthquake,38 All-Out Attack
	40 Divebomb,43 Fissure,52 Eruption
23	1 Water Jet,4 Bite,7 Bash,11 Psych Up,15 Slap,20 Power Hit
38	23 Sonic Splash,27 Flay,31 Deluge,35 Telepathic Blast
	38 Mind Blast,46 Paradox Channeling,53 Tidal Wave
20	1 Water Jet,4 Tackle,7 Bash,10 Curl Up,14 Uppercut
34	22 Shock Palm,28 Deluge,31 Powerblast
	34 Glacier Slam,37 Deluge,40 Lethal Fist,47 Tidal Wave
28	1 Water Jet,3 Earthshock,8 Bite,11 Uppercut,18 Ignite,20 Shock Palm
	28 Burning Cold,29 Glacier Slam,35 Powerblast,42 All-Out Attack,50 Arctic Freeze,58 Eruption
20	1 Water Jet,4 Bite,7 Bash,11 Psych Up,15 Frostbite
	20 Jumping Kick,28 Burning Cold,31 Deluge,37 Powerblast,46 Tidal Wave
25	1 Feather Whip,5 Bite,12 Curl Up,15 Slap,20 Caress
37	25 Thunder,29 Wing Beat,35 All-Out Attack
	39 Backstab,45 Lethal Current,48 Divebomb
36	1 Trick,3 Riddle,6 Bite,9 Bash,13 Psych Up,16 Precision Shot,25 Shed Casing,29 Posess
	34 Super-Gyroscope,37 Polish,41 Exorcism,47 Titanium Induction